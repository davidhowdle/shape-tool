#include "CartesianArray.h"

CartesianArray::CartesianArray() {
  this->init();
}

CartesianArray::~CartesianArray() {
  free(this->origin);
  free(this->pitch);
}

void CartesianArray::init() {
  this->defaultName("CartesianArray");
  this->origin = (float *)calloc(3,sizeof(float));
  this->pitch = (float *)calloc(3,sizeof(float));

  // need to add CartesianArray to properties
  this->properties->addProperty("Pitch",
                                PropertyUtilities::arrayToString(this->pitch, 3),
                                SCALAR_ARRAY);
  
  this->properties->addProperty("Origin",
                                PropertyUtilities::arrayToString(this->origin, 3),
                                SCALAR_ARRAY_OPT);
}

void CartesianArray::updateProperties() {
//  LogUtility::info("APPLYING " + this->name);
}
