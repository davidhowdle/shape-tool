#include "Scaling.h"

Scaling::Scaling() {
  this->init();
}

Scaling::~Scaling() {
  free(this->origin);
}

void Scaling::init() {
  this->defaultName("Scaling");
  this->origin = (float *)calloc(3,sizeof(float));
  this->scale = 1.0;

  // need to add Scaling to properties
  this->properties->addProperty("Scale",
                                PropertyUtilities::toString(this->scale),
                                SCALAR);

  this->properties->addProperty("Origin",
                                PropertyUtilities::arrayToString(this->origin, 3),
                                SCALAR_ARRAY_OPT);
}

void Scaling::updateProperties() {
//  LogUtility::info("APPLYING " + this->name);
}
