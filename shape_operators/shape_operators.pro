! include ( ../common.pri) {
  error("Couldn't find config file!")
}

TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += ../shapes ../data_managers ../gui
HEADERS = CartesianArray.h  OperatorFactory.h Rotation.h    ShapeCopy.h   Translation.h Mirror.h    PolarArray.h    Scaling.h   ShapeOperator.h
SOURCES = CartesianArray.cpp  OperatorFactory.cpp Rotation.cpp    ShapeCopy.cpp   Translation.cpp Mirror.cpp    PolarArray.cpp    Scaling.cpp   ShapeOperator.cpp
