#ifndef SHAPE_OPERATORH
#define SHAPE_OPERATORH

#include "CADObject.h"
#include "Shape.h"
#include "PropertyManager.h"
#include <deque>
class ShapeOperator : public CADObject {

public:
  ShapeOperator();
  virtual ~ShapeOperator();
  void addShape(Shape *shape) {this->selected_shapes->push_back(shape);}
  virtual void updateProperties();
  virtual std::deque<Shape *> * updatePropertiesAndGenerateShapes();

protected:
  std::deque<Shape*> * selected_shapes;
  virtual void init();
};

#endif //SHAPE_OPERATORH

