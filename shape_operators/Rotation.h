#ifndef ROTATION_H
#define ROTATION_H

#include "ShapeOperator.h"

class Rotation : public ShapeOperator {

public: 
  Rotation();
  ~Rotation();

protected:

private:
  float * normal;
  float * origin;

  virtual void init();
  virtual void updateProperties();
};

#endif
