#ifndef OPERATOR_FACTORY_H
#define OPERATOR_FACTORY_H

#include "ShapeOperator.h"

#include <string>

class OperatorFactory
{
public:

  static ShapeOperator* create(const std::string& name);
};

#endif // OPERATOR_FACTORY_H
