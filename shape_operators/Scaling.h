#ifndef SCALING_H
#define SCALING_H

#include "ShapeOperator.h"

class Scaling : public ShapeOperator {

public: 
  Scaling();
  ~Scaling();

protected:

private:
  float   scale;
  float * origin;

  virtual void init();
  virtual void updateProperties();
};

#endif
