#include "Translation.h"

Translation::Translation() {
  this->init();
}

Translation::~Translation() {
  free(this->translation);
}

void Translation::init() {
  this->defaultName("Translation");
  this->translation = (float *)calloc(3,sizeof(float));

  // need to add translation to properties
  this->properties->addProperty("translation",
                                PropertyUtilities::arrayToString(this->translation, 3),
                                SCALAR_ARRAY);
}

void Translation::updateProperties() {
//  LogUtility::info("APPLYING " + this->name);

  PropertyUtilities::stringToFloats(this->properties->getValue("translation"),
  3, this->translation);
  float O[3];
  //while(!this->selected_shapes->empty() ) {
  for(auto shape : *(this->selected_shapes)) {
    //auto shape = this->selected_shapes->front();
    //this->selected_shapes->pop_front();

    auto mgr = shape->getPropMgmt();
    PropertyUtilities::stringToFloats(mgr->getValue("Origin"),
                    3, O);
    for( int i = 0; i < 3; i++ ) O[i] += this->translation[i];
    mgr->setValue("Origin", PropertyUtilities::arrayToString(O,3));
    shape->updateProperties();
  }
}
