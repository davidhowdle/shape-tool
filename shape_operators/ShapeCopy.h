#ifndef SHAPE_COPY_H
#define SHAPE_COPY_H

#include "ShapeOperator.h"

class ShapeCopy : public ShapeOperator {

public: 
  ShapeCopy();
  ~ShapeCopy();

protected:

private:
  virtual void init();
  virtual void updateProperties();
};

#endif
