#include "OperatorFactory.h"
#include "Translation.h"
#include "Rotation.h"
#include "Scaling.h"
#include "ShapeCopy.h"
#include "CartesianArray.h"
#include "PolarArray.h"
#include "Mirror.h"

ShapeOperator* OperatorFactory::create(const std::string& name) {
  ShapeOperator* shape_op = 0;
  if (name == "Translate")
    shape_op = new Translation();
  if (name == "Rotate")
    shape_op = new Rotation();
  if (name == "Scale")
    shape_op = new Scaling();
  if (name == "Copy")
    shape_op = new ShapeCopy();
  if (name == "Cartesian Array")
    shape_op = new CartesianArray();
  if (name == "Polar Array")
    shape_op = new PolarArray();
  if (name == "Mirror")
    shape_op = new Mirror();
  return shape_op;
}
