#include "ShapeOperator.h"

ShapeOperator::ShapeOperator() {
  this->init();
}
ShapeOperator::~ShapeOperator() {
}

void ShapeOperator::init() {
  // allocate memory
  this->selected_shapes = new std::deque<Shape *>;
  
  // now add properties
  this->properties->addProperty("Selected Shapes", this->selected_shapes, CAD_OBJECT_ARRAY);
}

void ShapeOperator::updateProperties() {
//  LogUtility::info("OPERATING WITH SHAPE OPERATOR");
}

std::deque<Shape *> * ShapeOperator::updatePropertiesAndGenerateShapes() {
//  LogUtility::info("OPERATING AND CREATING WITH SHAPE OPERATOR");
  return nullptr;
}
