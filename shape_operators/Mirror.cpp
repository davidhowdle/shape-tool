#include "Mirror.h"

Mirror::Mirror() {
  this->init();
}

Mirror::~Mirror() {
  free(this->origin);
  free(this->normal);
}

void Mirror::init() {
  this->defaultName("Mirror");
  this->origin = (float *)calloc(3,sizeof(float));
  this->normal = (float *)calloc(3,sizeof(float));
  this->normal[2] = 1.0;

  // need to add Mirror to properties
  this->properties->addProperty("Direction",
                                PropertyUtilities::arrayToString(this->normal, 3),
                                SCALAR_ARRAY);
  
  this->properties->addProperty("Origin",
                                PropertyUtilities::arrayToString(this->origin, 3),
                                SCALAR_ARRAY);
}

void Mirror::updateProperties() {
//  LogUtility::info("APPLYING " + this->name);
}
