#include "PolarArray.h"

PolarArray::PolarArray() {
  this->init();
}

PolarArray::~PolarArray() {
  free(this->origin);
  free(this->normal);
  free(this->pitch);
}

void PolarArray::init() {
  this->defaultName("PolarArray");
  this->origin = (float *)calloc(3,sizeof(float));
  this->normal = (float *)calloc(3,sizeof(float));
  this->normal[2] = 1.0;
  this->pitch = (float *)calloc(3,sizeof(float));

  // need to add PolarArray to properties
  this->properties->addProperty("Origin",
                                PropertyUtilities::arrayToString(this->origin, 3),
                                SCALAR_ARRAY);
  
  this->properties->addProperty("Direction",
                                PropertyUtilities::arrayToString(this->normal, 3),
                                SCALAR_ARRAY);
  
  this->properties->addProperty("Pitch",
                                PropertyUtilities::arrayToString(this->pitch, 3),
                                SCALAR_ARRAY);
}

void PolarArray::updateProperties() {
//  LogUtility::info("APPLYING " + this->name);
}
