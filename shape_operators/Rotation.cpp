#include "Rotation.h"

Rotation::Rotation() {
  this->init();
}

Rotation::~Rotation() {
  free(this->origin);
  free(this->normal);
}

void Rotation::init() {
  this->defaultName("Rotation");
  this->origin = (float *)calloc(3,sizeof(float));
  this->normal = (float *)calloc(3,sizeof(float));
  this->normal[2] = 1.0;

  // need to add Rotation to properties
  this->properties->addProperty("Direction",
                                PropertyUtilities::arrayToString(this->normal, 3),
                                SCALAR_ARRAY);
  
  this->properties->addProperty("Origin",
                                PropertyUtilities::arrayToString(this->origin, 3),
                                SCALAR_ARRAY_OPT);
}

void Rotation::updateProperties() {
//  LogUtility::info("APPLYING " + this->name);
}
