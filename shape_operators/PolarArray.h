#ifndef POLAR_ARRAY_H
#define POLAR_ARRAY_H

#include "ShapeOperator.h"

class PolarArray : public ShapeOperator {

public: 
  PolarArray();
  ~PolarArray();

protected:

private:
  float * pitch;
  float * origin;
  float * normal;

  virtual void init();
  virtual void updateProperties();
};

#endif
