#ifndef TRANSLATION_H
#define TRANSLATION_H

#include "ShapeOperator.h"

class Translation : public ShapeOperator {

public: 
  Translation();
  ~Translation();

protected:

private:
  float * translation;
  virtual void init();
  virtual void updateProperties();
};

#endif
