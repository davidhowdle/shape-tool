#ifndef MIRROR_H
#define MIRROR_H

#include "ShapeOperator.h"

class Mirror : public ShapeOperator {

public: 
  Mirror();
  ~Mirror();

protected:

private:
  float * normal;
  float * origin;

  virtual void init();
  virtual void updateProperties();
};

#endif
