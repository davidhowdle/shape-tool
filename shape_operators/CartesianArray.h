#ifndef CARTESIAN_ARRAY_H
#define CARTESIAN_ARRAY_H

#include "ShapeOperator.h"

class CartesianArray : public ShapeOperator {

public: 
  CartesianArray();
  ~CartesianArray();

protected:

private:
  float * pitch;
  float * origin;

  virtual void init();
  virtual void updateProperties();
};

#endif
