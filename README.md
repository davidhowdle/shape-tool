# Shape-Tool #

### What is the Shape-Tool ###

Shape-Tool is an independent development of software, including User-Interface, OpenGL and especially mathematical tools for geometric manipulation (Constructive Solid Geometry)
- Many ideas are attempts to make GUI elements streamlined
- Most efforts are to make a refined shape manipulation tool

### What is the intent? ###

To refine, condense and control several years of differing experience in software, geometries and mathematics 

### How do I get set up? ###

Currently very much a work in progress.  Requires qt5 and must be run on either linux or mac.  Requires some external deps such as cblas.