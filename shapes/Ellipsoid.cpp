#include "Ellipsoid.h"

#define NPHI 20
#define NTHETA 40

using namespace std;

Ellipsoid::Ellipsoid(GLfloat a, GLfloat b, GLfloat c)
: radiusA(a), radiusB(b), radiusC(c)
{
  this->defaultName("Ellipsoid");
  this->init();
  // prsize_t indices
}

Ellipsoid::~Ellipsoid()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Ellipsoid::init() {
  this->constructVertsAndNorms();
  this->constructMesh();
  if(!this->constructed) {
    this->constructed = true;
    this->initProperties();
  }
}

void Ellipsoid::initProperties() {
  // add properties
  this->properties->addProperty("Radius A",
                                PropertyUtilities::toString<GLfloat>(this->radiusA),
                                SCALAR);
  
  this->properties->addProperty("Radius B",
                                PropertyUtilities::toString<GLfloat>(this->radiusB),
                                SCALAR);
  
  this->properties->addProperty("Radius C",
                                PropertyUtilities::toString<GLfloat>(this->radiusC),
                                SCALAR);
}

void Ellipsoid::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat ra = PropertyUtilities::stringToFloat(this->properties->getValue("Radius A"));
  GLfloat rb = PropertyUtilities::stringToFloat(this->properties->getValue("Radius B"));
  GLfloat rc = PropertyUtilities::stringToFloat(this->properties->getValue("Radius C"));
  this->setRadii(ra,rb,rc);
  
  // update
  this->init();
}

void Ellipsoid::constructVertsAndNorms()
{
  // init the sphere!
  GLfloat delta_th = 2.0*M_PI/NTHETA;
  GLfloat delta_phi = M_PI/NPHI;
  size_t n_verts = (NPHI-1)*(NTHETA+1)+2;
  size_t v = 0;
  size_t n = 0;
  
  if (!this->constructed) {
    this->vertices = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
  }
  
  // North pole
  Ellipsoid::parametersToCoordinates(0.0, 0.0, this->vertices, v);
  Ellipsoid::normalForCoordinate(&this->vertices[n],this->normals,n);
  
  for(size_t i = 1; i < NPHI; i++) {
    for(size_t j = 0; j <= NTHETA; j++) {
      Ellipsoid::parametersToCoordinates(j*delta_th,i*delta_phi, this->vertices, v);
      Ellipsoid::normalForCoordinate(&this->vertices[n],this->normals,n);
    }
  }
  
  // South pole
  Ellipsoid::parametersToCoordinates(0.0, M_PI, this->vertices, v);
  Ellipsoid::normalForCoordinate(&this->vertices[n],this->normals,n);
  
  this->nVerts = v/3;
}


void Ellipsoid::constructMesh()
{
  
  // set facet count
  
  if (!this->constructed) {
    this->nFacets = 2*NTHETA + 2*NTHETA*(NPHI-2);
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  // north cap
  size_t f = 0;
  for(GLuint i = 0; i < NTHETA; i++)
  {
    this->indices[f++] = 0;
    this->indices[f++] = i+1;
    this->indices[f++] = i+2;
  }
  
  // body
  size_t a,b,c,d;
  for (size_t i = 0; i < NPHI-2; i++) {
    for (size_t j = 0; j < NTHETA; j++) {
      a = Shape::localToGlobal(i,j,NTHETA+1)+1;
      b = Shape::localToGlobal(i+1,j,NTHETA+1)+1;
      c = Shape::localToGlobal(i+1,j+1,NTHETA+1)+1;
      d = Shape::localToGlobal(i,j+1,NTHETA+1)+1;
      
      this->indices[f++] = a;
      this->indices[f++] = b;
      this->indices[f++] = c;
      
      this->indices[f++] = c;
      this->indices[f++] = d;
      this->indices[f++] = a;
    }
  }
  
  // south cap
  for(GLuint j = 0; j < NTHETA; j++)
  {
    a = Shape::localToGlobal(NPHI-2,j,NTHETA+1)+1;
    b = Shape::localToGlobal(NPHI-2,j+1,NTHETA+1)+1;
    this->indices[f++] = this->nVerts-1;
    this->indices[f++] = b;
    this->indices[f++] = a;
  }
}

void Ellipsoid::parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx)
{
  verts[idx++] = this->radiusA*cos(theta)*sin(phi) + this->origin[0];
  verts[idx++] = this->radiusB*sin(theta)*sin(phi) + this->origin[1];
  verts[idx++] = this->radiusC*cos(phi) + this->origin[2];
}

void Ellipsoid::normalForCoordinate(GLfloat * coordinate, GLfloat * norms, size_t &idx)
{
  GLfloat L = sqrt(pow(coordinate[0] - this->origin[0], 2) +
                   pow(coordinate[1] - this->origin[1], 2) +
                   pow(coordinate[2] - this->origin[2], 2));
  
  norms[idx++] = 2.0 * (coordinate[0] - this->origin[0]) / L / this->radiusA;
  norms[idx++] = 2.0 * (coordinate[1] - this->origin[1]) / L / this->radiusB;
  norms[idx++] = 2.0 * (coordinate[2] - this->origin[2]) / L / this->radiusC;
}


void Ellipsoid::draw() {
  this->Shape::draw();
}

void Ellipsoid::setRadiusA(GLfloat a) {
  if (a > 0.0 && (this->radiusA != a)) this->radiusA = a;
}

void Ellipsoid::setRadiusB(GLfloat b) {
  if (b > 0.0 && (this->radiusB != b)) this->radiusB = b;
}

void Ellipsoid::setRadiusC(GLfloat c) {
  if (c > 0.0 && (this->radiusC != c)) this->radiusC = c;
}

void Ellipsoid::setRadii(GLfloat a, GLfloat b, GLfloat c) {
  this->setRadiusA(a);
  this->setRadiusB(b);
  this->setRadiusC(c);
}
