! include(../common.pri){
error("Couldn't find common config file!")
}

TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += ../data_managers ../gui ../shape_operators
HEADERS = Annulus.h CADObject.h Circle.h Cone.h Cube.h Cuboid.h Cylinder.h Ellipse.h Ellipsoid.h NGon.h Rectangle.h Sphere.h Square.h Tetrahedron.h Torus.h Triangle.h TruncatedCone.h GeometricUtilities.h Shape.h ShapeFactory.h
SOURCES = Annulus.cpp CADObject.cpp Circle.cpp Cone.cpp Cube.cpp Cuboid.cpp Cylinder.cpp Ellipse.cpp Ellipsoid.cpp NGon.cpp Rectangle.cpp Sphere.cpp Square.cpp Tetrahedron.cpp Torus.cpp Triangle.cpp TruncatedCone.cpp GeometricUtilities.cpp Shape.cpp ShapeFactory.cpp
