#ifndef SQUARE_H
#define SQUARE_H

#include "Shape.h"

class Square : public Shape
{
public:
  Square(GLfloat W = 1.0);
  virtual ~Square();
  virtual void draw();
  virtual void init();

  GLfloat getWidth() const { return this->width;}
  void setWidth(GLfloat W);

private:

  GLfloat width;
  
  virtual void initProperties();
  virtual void updateProperties();

  void constructVerts();
  void constructMesh();
};

#endif // SQUARE_H
