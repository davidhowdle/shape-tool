#include "GeometricUtilities.h"


void GeometricUtilities::rotatePoint(const float * N0,
                          const float * O,
                          const float * Nr,
                                float * x) {
  
  float u[3] = {0.0, 0.0, 0.0};
  GeometricUtilities::crossProduct(N0,Nr,u);
  cblas_sscal(3,1.0/cblas_snrm2(3,u,1),u,1); // normalize

  // get angle between shapes normal and normal to rotate into
  float theta = acos(cblas_sdot(3,N0,1,Nr,1));
  
  // rotation quanternions
  float q[4] = {0.0, 0.0, 0.0, 0.0};
  q[0] = cos(0.5*theta);
  cblas_saxpy(3,sin(0.5*theta),u,1,&q[1],1);

  float qm[4] = {0.0, 0.0, 0.0, 0.0};
  cblas_scopy(4,q,1,qm,1);
  cblas_sscal(3,-1.0,&qm[1],1);

  // coordinate as a quanternion
  float p[4] = {0.0, 0.0, 0.0, 0.0};
  cblas_scopy(3,x,1,&p[1],1);
  cblas_saxpy(3,-1.0,O,1,p,1);

  // transformation
  float xTmp[4] = {0.0, 0.0, 0.0, 0.0};
  float xR[4] = {0.0, 0.0, 0.0, 0.0};
  GeometricUtilities::quanternionVectorProduct(q,p,xTmp);
  GeometricUtilities::quanternionVectorProduct(xTmp,qm,xR);
  
  cblas_saxpy(3,1.0,O,1,&xR[1],1);
  cblas_scopy(3,&xR[1],1,x,1);
}

void GeometricUtilities::quanternionVectorProduct(const float * r, const float *s, float *q) {
  
  q[0] = r[0]*s[0] - cblas_sdot(3,&r[1],1,&s[1],1);
  cblas_saxpy(3,r[0],&s[1],1,&q[1],1);
  cblas_saxpy(3,s[0],&r[1],1,&q[1],1);
  GeometricUtilities::crossProduct(&r[1],&s[1],&q[1],1.0); // the cross product part
}

void GeometricUtilities::crossProduct(const float *a, const float *b, float *c, float alpha)
{
  c[0] = a[1]*b[2]-a[2]*b[1] + alpha * c[0];
  c[1] = -(a[0]*b[2]-a[2]*b[0]) + alpha * c[1];
  c[2] = a[0]*b[1]-a[1]*b[0] + alpha * c[2];
}

