#include "Annulus.h"

#define NTHETA 40

using namespace std;

Annulus::Annulus(GLfloat rA, GLfloat rB)
: radiusA(rA), radiusB(rB)
{
  this->defaultName("Annulus");
  this->init();
}

Annulus::~Annulus()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Annulus::init() {
  this->constructVerts();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Annulus::initProperties() {
  // add properties
  this->properties->addProperty("Inner Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusA),
                                SCALAR);
  
  this->properties->addProperty("Outer Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusB),
                                SCALAR);
}

void Annulus::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat ra = PropertyUtilities::stringToFloat(this->properties->getValue("Inner Radius"));
  GLfloat rb = PropertyUtilities::stringToFloat(this->properties->getValue("Outer Radius"));
  
  this->setRadii(ra,rb);
  
  // update
  this->init();
}

void Annulus::constructVerts()
{
  this->nVerts = 2*NTHETA;
  if(!this->constructed) {
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }
  
  GLfloat dth = 2.0*M_PI/NTHETA;
  for(size_t i = 0; i < NTHETA; i++) {
    this->vertices[3*i] = this->radiusA*cos(i*dth) + this->origin[0];
    this->vertices[3*i+1] = this->radiusA*sin(i*dth) + this->origin[1];
    this->vertices[3*i+2] = this->origin[2];
    
    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
    
  }
  
  for(size_t i = NTHETA; i < 2*NTHETA; i++) {
    this->vertices[3*i] = this->radiusB*cos(i*dth) + this->origin[0];
    this->vertices[3*i+1] = this->radiusB*sin(i*dth) + this->origin[1];
    this->vertices[3*i+2] = this->origin[2];

    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
  }
  
}


void Annulus::constructMesh()
{
  // set facet count
  if(!this->constructed) {
    this->nFacets = 2*NTHETA;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  size_t a, b, c, d;
  size_t idx = 0;
  for(size_t i = 0; i < NTHETA; i++) {
    
    a = Shape::localToGlobal(0,i,NTHETA);
    b = Shape::localToGlobal(1,i,NTHETA);
    c = Shape::localToGlobal(1,(i+1)%NTHETA,NTHETA);
    d = Shape::localToGlobal(0,(i+1)%NTHETA,NTHETA);
    
    this->indices[idx++] = a;
    this->indices[idx++] = b;
    this->indices[idx++] = c;
    
    this->indices[idx++] = c;
    this->indices[idx++] = d;
    this->indices[idx++] = a;
  }
}


void Annulus::draw() {
  
  // draw fill
  this->Shape::draw();
  
  // draw outline
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  
  glLineWidth(3.0);
  glColor3f(0.0, 0.0, 0.0);
  glDrawArrays(GL_LINE_LOOP,0,NTHETA);
  glVertexPointer(3, GL_FLOAT, 0, &this->vertices[3*NTHETA]);
  glDrawArrays(GL_LINE_LOOP,0,NTHETA);
  
  // deactivate vertex arrays after drawing
  glDisableClientState(GL_VERTEX_ARRAY);
}

void Annulus::setRadiusA(GLfloat rA) {
  
  if (rA > 0.0
      && (this->radiusA != rA)
      && (rA < this->radiusB)) this->radiusA = rA;
}

void Annulus::setRadiusB(GLfloat rB) {
  if (rB > 0.0
      && (this->radiusB != rB)
      && rB > this->radiusA) this->radiusB = rB;
}

void Annulus::setRadii(GLfloat rA, GLfloat rB) {
  this->setRadiusA(rA);
  this->setRadiusB(rB);
}
