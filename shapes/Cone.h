#ifndef CONE_H
#define CONE_H

#include "Shape.h"

class Cone : public Shape
{
public:
  Cone(GLfloat R = 1.0, GLfloat H = 2.0);
  virtual ~Cone();
  virtual void draw();
  virtual void init();

  GLfloat getRadius() const { return this->radius; }
  GLfloat getHeight() const { return this->height; }

  void setRadius(GLfloat R);
  void setHeight(GLfloat H);

private:

  GLfloat radius;
  GLfloat height;
  
  virtual void initProperties();
  virtual void updateProperties();
  
  void constructVertsAndNorms();
  void constructMesh();
  void constructNormals();
  void parametersToCoordinates(GLfloat theta, GLfloat phi,GLfloat R, GLfloat * verts, size_t &idx);
  void normalForCoordinate(GLfloat theta, size_t phase, GLfloat * norms, size_t &idx);
};

#endif // CONE_H
