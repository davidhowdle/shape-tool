#include "Ellipse.h"

#define NTHETA 40

using namespace std;

Ellipse::Ellipse(GLfloat a, GLfloat b)
: radiusA(a), radiusB(b)
{
  this->defaultName("Ellipse");
  this->init();
}

Ellipse::~Ellipse()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Ellipse::init() {
  this->constructVerts();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Ellipse::initProperties() {
  // add properties
  this->properties->addProperty("Major Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusA),
                                SCALAR);
  
  this->properties->addProperty("Minor Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusB),
                                SCALAR);
}

void Ellipse::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat ra = PropertyUtilities::stringToFloat(this->properties->getValue("Major Radius"));
  GLfloat rb = PropertyUtilities::stringToFloat(this->properties->getValue("Minor Radius"));
  
  this->setRadii(ra, rb);
  
  // update
  this->init();
}

void Ellipse::constructVerts()
{
  if(!this->constructed) {
    this->nVerts = NTHETA+1;
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }
  
  GLfloat dth = 2.0*M_PI/NTHETA;
  
  for(size_t i = 0; i < NTHETA; i++) {
    this->vertices[3*i] = this->radiusA*cos(i*dth) + this->origin[0];
    this->vertices[3*i+1] = this->radiusB*sin(i*dth) + this->origin[1];
    this->vertices[3*i+2] = this->origin[2];
    
    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
  }
  
  for (size_t i = 0; i < 3; i++ ) this->vertices[3*NTHETA+i] = this->origin[i];
  
  cblas_scopy(3,this->normal,1,&this->normals[3*NTHETA],1);
}


void Ellipse::constructMesh()
{
  // set facet count
  if(!this->constructed) {
    this->nFacets = NTHETA;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  size_t idx = 0;
  for(size_t i = 0; i < NTHETA; i++) {
    this->indices[idx++] = this->nVerts-1;
    this->indices[idx++] = i;
    this->indices[idx++] = (i+1)%NTHETA;
  }
}


void Ellipse::draw() {
  
  // draw fill
  this->Shape::draw();
  
  // draw outline
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  
  // draw outline!
  glLineWidth(3.0);
  glColor3f(0.0, 0.0, 0.0);
  glDrawArrays(GL_LINE_LOOP,0,NTHETA);
  
  // deactivate vertex arrays after drawing
  glDisableClientState(GL_VERTEX_ARRAY);
  
}

void Ellipse::setRadiusA(GLfloat a) {
  if (a > 0.0 && (this->radiusA != a)) this->radiusA = a;
}

void Ellipse::setRadiusB(GLfloat b) {
  if (b > 0.0 && (this->radiusB != b)) this->radiusB = b;
}

void Ellipse::setRadii(GLfloat a, GLfloat b) {
  this->setRadiusA(a);
  this->setRadiusB(b);
}
