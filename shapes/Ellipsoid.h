#ifndef ELLIPSOID_H
#define ELLIPSOID_H

#include "Shape.h"

class Ellipsoid : public Shape
{
public:
  Ellipsoid(GLfloat a = 2.0, GLfloat b = 0.5, GLfloat c = 1.0);
  virtual ~Ellipsoid();
  virtual void draw();
  virtual void init();

  GLfloat getRadiusA() const { return this->radiusA; }
  GLfloat getRadiusB() const { return this->radiusB; }
  GLfloat getRadiusC() const { return this->radiusC; }

  void setRadiusA(GLfloat a);
  void setRadiusB(GLfloat b);
  void setRadiusC(GLfloat c);
  void setRadii(GLfloat a, GLfloat b, GLfloat c);

private:

  GLfloat radiusA, radiusB, radiusC;

  virtual void initProperties();
  virtual void updateProperties();

  void constructVertsAndNorms();
  void constructMesh();
  void parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx);
  void normalForCoordinate(GLfloat * coordinate, GLfloat * norms, size_t &idx);
};

#endif // ELLIPSOID_H
