#ifndef CUBE_H
#define CUBE_H

#include "Cuboid.h"

class Cube : public Cuboid
{
public:
  Cube(GLfloat W = 1.0);
  virtual ~Cube();

  void setWidth(GLfloat W);

private:

  GLfloat width;
  virtual void initProperties();
  virtual void updateProperties();

};

#endif // CUBE_H
