#include "Circle.h"

#define NTHETA 40

using namespace std;

Circle::Circle(GLfloat R) : radius(R)
{
  this->defaultName("Circle");
  this->init();
}

Circle::~Circle()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Circle::init() {
  this->constructVerts();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Circle::initProperties() {
  // add properties
  this->properties->addProperty("Radius",
                                PropertyUtilities::toString<GLfloat>(this->radius),
                                SCALAR);
}

void Circle::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat r = PropertyUtilities::stringToFloat(this->properties->getValue("Radius"));
  
  this->setRadius(r);
  
  // update
  this->init();
}

void Circle::constructVerts() {
  if(!this->constructed) {
    this->nVerts = NTHETA+1;
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }
  
  GLfloat dth = 2.0*M_PI/NTHETA;
  for(size_t i = 0; i < NTHETA; i++) {
    this->vertices[3*i] = this->radius*cos(i*dth) + this->origin[0];
    this->vertices[3*i+1] = this->radius*sin(i*dth) + this->origin[1];
    this->vertices[3*i+2] = this->origin[2];
    
    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
  }

  for (size_t i = 0; i < 3; i++ ) this->vertices[3*NTHETA+i] = this->origin[i];

  cblas_scopy(3,this->normal,1,&this->normals[3*NTHETA],1);
}


void Circle::constructMesh()
{
  // set facet count
  if(!this->constructed) {
    this->nFacets = NTHETA;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  size_t idx = 0;
  for(size_t i = 0; i < NTHETA; i++) {
    this->indices[idx++] = this->nVerts-1;
    this->indices[idx++] = i;
    this->indices[idx++] = (i+1)%(NTHETA);
  }
}


void Circle::draw() {
  
  // draw fill
  this->Shape::draw();
  
  // draw outline
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  
  glLineWidth(3.0);
  glColor3f(0.0, 0.0, 0.0);
  glDrawArrays(GL_LINE_LOOP,0,NTHETA);
  
  // deactivate vertex arrays after drawing
  glDisableClientState(GL_VERTEX_ARRAY);
}

void Circle::setRadius(GLfloat R)
{
  if(R > 0.0 && (this->radius != R)) this->radius = R;
}
