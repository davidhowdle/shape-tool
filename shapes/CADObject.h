#ifndef CAD_OBJECT_H
#define CAD_OBJECT_H
#include <string>
#include <sstream>
#include <deque>

#include "LogUtility.h"
class PropertyManager;
class Shape;

class CADObject {

public:
  CADObject();
  virtual ~CADObject();

  virtual void       	   setName(const std::string& nm);
  virtual void       	   defaultName(const std::string& nm);
  virtual std::string	   getName();
  virtual PropertyManager*  getPropMgmt() {return properties;}
  virtual void updateProperties() = 0;
  virtual std::deque<Shape *> * updatePropertiesAndGenerateShapes() {return nullptr;}
  std::string name;

protected:
  PropertyManager * properties;

private:

  static std::map<std::string, size_t> names;
};

Q_DECLARE_METATYPE(CADObject*);

#endif // CAD_OBJECT_H



