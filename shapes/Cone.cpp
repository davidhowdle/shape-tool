#include "Cone.h"

#define NPHI 2
#define NTHETA 80

using namespace std;

Cone::Cone(GLfloat R, GLfloat H) : radius(R), height(H)
{
  this->defaultName("Cone");
  this->init();
}

Cone::~Cone()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Cone::init() {
  
  this->constructVertsAndNorms();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Cone::initProperties() {
  // add properties
  this->properties->addProperty("Radius",
                                PropertyUtilities::toString<GLfloat>(this->radius),
                                SCALAR);
  
  this->properties->addProperty("Height",
                                PropertyUtilities::toString<GLfloat>(this->height),
                                SCALAR);
}

void Cone::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat r = PropertyUtilities::stringToFloat(this->properties->getValue("Radius"));
  GLfloat h = PropertyUtilities::stringToFloat(this->properties->getValue("Height"));

  this->setRadius(r);
  this->setHeight(h);
  
  // update
  this->init();
}

void Cone::constructVertsAndNorms()
{
  // init the sphere!
  GLfloat delta_th = 2.0*M_PI/NTHETA;
  size_t n_verts = 3*(NTHETA+1)+1;
  size_t v = 0;
  size_t n = 0;
  if(!this->constructed) {
    this->vertices = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
  }
  
  for (size_t i = 0; i <= 1 ; i++) {
    for(size_t j = 0; j <= NTHETA; j++) {
      Cone::parametersToCoordinates(j*delta_th,i*M_PI, i*this->radius,this->vertices, v);
      Cone::normalForCoordinate(j*delta_th,1,this->normals,n);
    }
  }
  
  for(size_t j = 0; j <= NTHETA; j++) {
    Cone::parametersToCoordinates(j*delta_th, M_PI, this->radius,this->vertices, v);
    Cone::normalForCoordinate(j*delta_th,2,this->normals,n);
  }
  // South pole
  Cone::parametersToCoordinates(0.0, M_PI, this->radius,this->vertices, v);
  Cone::normalForCoordinate(0,2,this->normals,n);
  this->nVerts = v/3;
}


void Cone::constructMesh()
{
  
  // set facet count
  size_t f = 0;
  if(!this->constructed) {
    this->nFacets = 3*NTHETA;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  // body
  size_t a,b,c,d;
  for (size_t i = 0; i < NPHI-1; i++) {
    for (size_t j = 0; j < NTHETA; j++) {
      a = Shape::localToGlobal(i,j,NTHETA+1);
      b = Shape::localToGlobal(i+1,j,NTHETA+1);
      c = Shape::localToGlobal(i+1,j+1,NTHETA+1);
      d = Shape::localToGlobal(i,j+1,NTHETA+1);
      
      this->indices[f++] = a;
      this->indices[f++] = b;
      this->indices[f++] = c;
      
      this->indices[f++] = c;
      this->indices[f++] = d;
      this->indices[f++] = a;
    }
  }
  
  // south cap
  for(GLuint j = 0; j < NTHETA; j++)
  {
    a = Shape::localToGlobal(NPHI,j,NTHETA+1);
    b = Shape::localToGlobal(NPHI,j+1,NTHETA+1);
    this->indices[f++] = this->nVerts-1;
    this->indices[f++] = b;
    this->indices[f++] = a;
  }
}

void Cone::parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat R, GLfloat * verts, size_t &idx)
{
  verts[idx++] = R*cos(theta) + this->origin[0];
  verts[idx++] = R*sin(theta) + this->origin[1];
  verts[idx++] = this->height/2.0 - this->height*phi/M_PI + this->origin[2];
}

void Cone::normalForCoordinate(GLfloat theta, size_t phase, GLfloat * norms, size_t &idx)
{
  if (phase == 0) {
    norms[idx++] = 0.0;
    norms[idx++] = 0.0;
    norms[idx++] = 1.0;
  } else if (phase == 1) {
    
    GLfloat nrm[3] = {cos(theta), sin(theta), 0.0};
    GLfloat Nr[3] = {static_cast<GLfloat>((0.0 - this->radius)*nrm[0]),
      static_cast<GLfloat>((0.0 - this->radius)*nrm[1]),
      this->height};
    
    cblas_sscal(3,1.0/cblas_snrm2(3,Nr,1),Nr,1); // normalize
    static GLfloat N0[3] = {0,0,1};
    static GLfloat O[3] = {0,0,0};
    
    GeometricUtilities::rotatePoint(N0,O,Nr,nrm);
    cblas_scopy(3,nrm,1,&norms[idx],1);
    idx += 3;
    
  } else {
    norms[idx++] = 0.0;
    norms[idx++] = 0.0;
    norms[idx++] = -1.0;
  }
}

void Cone::draw() {
  this->Shape::draw();
}

void Cone::setRadius(GLfloat R) {
  if (R > 0.0 && (this->radius != R)) this->radius = R;
}

void Cone::setHeight(GLfloat H) {
  if (H > 0.0 && (this->height != H)) this->height = H;
}

