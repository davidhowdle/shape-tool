#include "NGon.h"

using namespace std;

NGon::NGon(size_t N, GLfloat R)
: sides(N), radius(R)
{
  this->init();
  this->defaultName(NGon::getPolygonName(N));
}

std::string NGon::getPolygonName(size_t sides) {
  std::string name = "";
  switch(sides) {
    case 3: name = "Triangle"; break;
    case 4: name = "Square"; break;
    case 5: name = "Pentagon"; break;
    case 6: name = "Hexagon"; break;
    case 7: name = "Heptagon"; break;
    case 8: name = "Octogon"; break;
    default: name = "N-Gon"; break;
  }

  return name;
}

NGon::~NGon()
{
  this->tearDown();
}

void NGon::tearDown() {
  
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
  this->constructed = false;
}

void NGon::init() {
  this->constructVerts();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void NGon::initProperties() {
  // add properties
  this->properties->addProperty("Number of sides",
                                PropertyUtilities::toString<size_t>(this->sides),
                                INT);
  
  this->properties->addProperty("Radius",
                                PropertyUtilities::toString<GLfloat>(this->radius),
                                SCALAR);
}

void NGon::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat r = PropertyUtilities::stringToFloat(this->properties->getValue("Radius"));
  size_t N = PropertyUtilities::stringToUnsigned(this->properties->getValue("Number of sides"));
  
  this->setRadius(r);
  this->setSides(N);
  
  // update
  this->init();
}

void NGon::constructVerts()
{
  if(!this->constructed) {
    this->nVerts = this->sides+1;
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }
  
  GLfloat dth = 2.0*M_PI/this->sides;
  for(size_t i = 0; i < this->sides; i++) {
    this->vertices[3*i] = this->radius*cos(i*dth) + this->origin[0];
    this->vertices[3*i+1] = this->radius*sin(i*dth) + this->origin[1];
    this->vertices[3*i+2] = this->origin[2];
    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
  }
  
  for (size_t i = 0; i < 3; i++ ) this->vertices[3*this->sides+i] = this->origin[i];

  cblas_scopy(3,this->normal,1,&this->normals[3*this->sides],1);
  
}


void NGon::constructMesh()
{
  // set facet count
  if(!this->constructed) {
    this->nFacets = this->sides;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  size_t idx = 0;
  for(size_t i = 0; i < this->sides; i++) {
    this->indices[idx++] = this->nVerts-1;
    this->indices[idx++] = i;
    this->indices[idx++] = (i+1)%(this->sides);
  }
}


void NGon::draw() {
  
  // draw fill
  this->Shape::draw();
  
  // draw outline
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  
  glLineWidth(3.0);
  glColor3f(0.0, 0.0, 0.0);
  glDrawArrays(GL_LINE_LOOP,0,this->sides);
  
  // deactivate vertex arrays after drawing
  glDisableClientState(GL_VERTEX_ARRAY);
}

void NGon::setSides(size_t N) {
  if( N > 2 && (this->sides != N)) {
    this->sides = N;
    this->defaultName(NGon::getPolygonName(N));
    this->tearDown();
  }
}

void NGon::setRadius(GLfloat R) {
  if (R > 0.0 && (this->radius != R)) this->radius = R;
}
