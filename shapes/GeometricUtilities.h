#ifndef GEOMETRIC_UTILITIES_H
#define GEOMETRIC_UTILITIES_H

#include <cblas.h>
#include <math.h>
namespace GeometricUtilities {
  
  void rotatePoint(const float * N0,
                          const float * O,
                          const float * Nr,
                                float * x);

  void quanternionVectorProduct(const float * r, const float *s, float *q);
  void crossProduct(const float *a, const float *b, float *c, float alpha = 0.0);

}

#endif //GEOMETRIC_UTILITIES_H
