#ifndef SPHERE_H
#define SPHERE_H

#include "Shape.h"

class Sphere : public Shape
{
public:
  Sphere(GLfloat R = 1.0);
  virtual ~Sphere();
  virtual void draw();
  virtual void init();
  
  // setters and getters
  GLfloat getRadius() const { return this->radius; }
  void setRadius(GLfloat R);
  
private:
  
  GLfloat radius;
  
  virtual void initProperties();
  virtual void updateProperties();
  
  void constructVertsAndNorms();
  void constructMesh();
  void parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx);
  void normalForCoordinate(GLfloat * coordinate, GLfloat * norms, size_t &idx);
};

#endif // SPHERE_H
