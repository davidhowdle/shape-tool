#include "Cuboid.h"

using namespace std;

Cuboid::Cuboid(GLfloat L , GLfloat W, GLfloat D)
  : length(L), width(W), depth(D)
{
  this->defaultName("Cuboid");
  this->init();
}

Cuboid::~Cuboid()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Cuboid::init() {
  
    this->constructVerts();
    this->constructMesh();
    this->constructNormals();
  
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Cuboid::initProperties() {
  // add properties
  this->properties->addProperty("Length",
                                PropertyUtilities::toString<GLfloat>(this->length),
                                SCALAR);
  
  this->properties->addProperty("Width",
                                PropertyUtilities::toString<GLfloat>(this->width),
                                SCALAR);
  
  this->properties->addProperty("Depth",
                                PropertyUtilities::toString<GLfloat>(this->depth),
                                SCALAR);
}

void Cuboid::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat l = PropertyUtilities::stringToFloat(this->properties->getValue("Length"));
  GLfloat w = PropertyUtilities::stringToFloat(this->properties->getValue("Width"));
  GLfloat d = PropertyUtilities::stringToFloat(this->properties->getValue("Depth"));
  this->setLength(l);
  this->setWidth(w);
  this->setDepth(d);
  
  // update
  this->init();
}

void Cuboid::constructVerts()
{
  // init the sphere!
  if (!this->constructed) {
    this->nVerts = 24;
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }

  GLfloat v[24] = { static_cast<GLfloat>(-this->length/2.0),  static_cast<GLfloat>(-this->width/2.0), static_cast<GLfloat>(-this->depth/2.0),
                    static_cast<GLfloat>(this->length/2.0),   static_cast<GLfloat>(-this->width/2.0), static_cast<GLfloat>(-this->depth/2.0),
                    static_cast<GLfloat>(this->length/2.0),   static_cast<GLfloat>(this->width/2.0),  static_cast<GLfloat>(-this->depth/2.0),
                    static_cast<GLfloat>(-this->length/2.0),  static_cast<GLfloat>(this->width/2.0),  static_cast<GLfloat>(-this->depth/2.0),
                    static_cast<GLfloat>(-this->length/2.0),  static_cast<GLfloat>(-this->width/2.0), static_cast<GLfloat>(this->depth/2.0),
                    static_cast<GLfloat>(this->length/2.0),   static_cast<GLfloat>(-this->width/2.0), static_cast<GLfloat>(this->depth/2.0),
                    static_cast<GLfloat>(this->length/2.0),   static_cast<GLfloat>(this->width/2.0),  static_cast<GLfloat>(this->depth/2.0),
                    static_cast<GLfloat>(-this->length/2.0),  static_cast<GLfloat>(this->width/2.0),  static_cast<GLfloat>(this->depth/2.0)};

  for(size_t i = 0; i < 8; i++) {
    v[3*i] += this->origin[0];
    v[3*i+1] += this->origin[1];
    v[3*i+2] += this->origin[2];
  }

  for (size_t i = 0; i < 3; i++) cblas_scopy(this->nVerts,v,1,&this->vertices[i*24],1);

}


void Cuboid::constructMesh()
{

  // set facet count
  this->nFacets = 12;
  if (!this->constructed)
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  
  size_t idx = 0;
  //nz- face
  this->indices[idx++] = 0;
  this->indices[idx++] = 2;
  this->indices[idx++] = 1;
  
  this->indices[idx++] = 0;
  this->indices[idx++] = 3;
  this->indices[idx++] = 2;

  //nz+ face
  this->indices[idx++] = 4;
  this->indices[idx++] = 5;
  this->indices[idx++] = 6;
  
  this->indices[idx++] = 4;
  this->indices[idx++] = 6;
  this->indices[idx++] = 7;

  //nx- face
  this->indices[idx++] = 8;
  this->indices[idx++] = 12;
  this->indices[idx++] = 15;
  
  this->indices[idx++] = 8;
  this->indices[idx++] = 15;
  this->indices[idx++] = 11;

  //nx+ face
  this->indices[idx++] = 9;
  this->indices[idx++] = 10;
  this->indices[idx++] = 14;
  
  this->indices[idx++] = 9;
  this->indices[idx++] = 14;
  this->indices[idx++] = 13;

  //ny- face
  this->indices[idx++] = 16;
  this->indices[idx++] = 17;
  this->indices[idx++] = 21;
  
  this->indices[idx++] = 16;
  this->indices[idx++] = 21;
  this->indices[idx++] = 20;

  //ny+ face
  this->indices[idx++] = 18;
  this->indices[idx++] = 19;
  this->indices[idx++] = 23;
  
  this->indices[idx++] = 18;
  this->indices[idx++] = 23;
  this->indices[idx++] = 22;
}

void Cuboid::constructNormals() {

  if (!this->constructed)
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));

  GLuint a,b,c;
  GLfloat v1[3],v2[3], N[3];
  for (size_t i = 0; i < this->nFacets; i++) {
    a = this->indices[3*i];
    b = this->indices[3*i+1];
    c = this->indices[3*i+2];
    
    for (size_t j = 0; j < 3; j++) {
      v1[j] = this->vertices[3*b+j] - this->vertices[3*a+j];
      v2[j] = this->vertices[3*c+j] - this->vertices[3*a+j];
    }  

    GeometricUtilities::crossProduct(v1,v2,N);
    cblas_sscal(3,1.0/cblas_snrm2(3,N,1),N,1); // normalize

    for (size_t j = 0; j < 3; j++) {
      cblas_scopy(3,N,1,&this->normals[3*a], 1);
      cblas_scopy(3,N,1,&this->normals[3*b], 1);
      cblas_scopy(3,N,1,&this->normals[3*c], 1);
    }
  }

}

void Cuboid::draw() {
 this->Shape::draw();
}

void Cuboid::setLength(GLfloat L) {
  if (L > 0.0 && (this->length != L)) this->length = L;
}

void Cuboid::setWidth(GLfloat W) {
  if (W > 0.0 && (this->width != W)) this->width = W;
}

void Cuboid::setDepth(GLfloat D) {
  if (D > 0.0 && (this->depth != D)) this->depth = D;
}
