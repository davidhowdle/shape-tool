#include "Cylinder.h"

#define NPHI 2
#define NTHETA 40

using namespace std;

Cylinder::Cylinder(GLfloat R, GLfloat H)
: radius(R), height(H)
{
  this->defaultName("Cylinder");
  this->init();
}

Cylinder::~Cylinder()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Cylinder::init() {
  this->constructVertsAndNorms();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Cylinder::initProperties() {
  // add properties
  this->properties->addProperty("Radius",
                                PropertyUtilities::toString<GLfloat>(this->radius),
                                SCALAR);
  
  this->properties->addProperty("Height",
                                PropertyUtilities::toString<GLfloat>(this->height),
                                SCALAR);
}

void Cylinder::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat r = PropertyUtilities::stringToFloat(this->properties->getValue("Radius"));
  GLfloat h = PropertyUtilities::stringToFloat(this->properties->getValue("Height"));
  
  this->setRadius(r);
  this->setHeight(h);
  
  // update
  this->init();
}

void Cylinder::constructVertsAndNorms()
{
  // init the sphere!
  GLfloat delta_th = 2.0*M_PI/NTHETA;
  size_t n_verts = 4*(NTHETA+1)+2;
  size_t v = 0;
  size_t n = 0;
  size_t phase = 0;
  
  if(!this->constructed) {
    this->vertices = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
  }
  
  // North pole
  Cylinder::parametersToCoordinates(0.0, 0.0, this->vertices, v);
  Cylinder::normalForCoordinate(&this->vertices[n],phase,this->normals,n);
  
  for (size_t k = 0; k <= 1; k++) {
    for (size_t i = 0; i <= 1 ; i++) {
      if (k == 1 && i == 1) phase = 2;
      else if(k == 1 && i == 0) phase = 1;
      else phase = i;
      
      for(size_t j = 0; j <= NTHETA; j++) {
        Cylinder::parametersToCoordinates(j*delta_th,k*M_PI, this->vertices, v);
        Cylinder::normalForCoordinate(&this->vertices[n],phase,this->normals,n);
      }
    }
  }
  
  // South pole
  Cylinder::parametersToCoordinates(0.0, M_PI, this->vertices, v);
  Cylinder::normalForCoordinate(&this->vertices[n],phase,this->normals,n);
  
  this->nVerts = v/3;
}


void Cylinder::constructMesh()
{
  
  // set facet count
  if(!this->constructed) {
    this->nFacets = 2*NTHETA + 2*NTHETA;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  // north cap
  size_t f = 0;
  for(GLuint i = 0; i < NTHETA; i++)
  {
    this->indices[f++] = 0;
    this->indices[f++] = i+1;
    this->indices[f++] = i+2;
  }
  
  // body
  size_t a,b,c,d;
  for (size_t i = 1; i < NPHI; i++) {
    for (size_t j = 0; j < NTHETA; j++) {
      a = Shape::localToGlobal(i,j,NTHETA+1)+1;
      b = Shape::localToGlobal(i+1,j,NTHETA+1)+1;
      c = Shape::localToGlobal(i+1,j+1,NTHETA+1)+1;
      d = Shape::localToGlobal(i,j+1,NTHETA+1)+1;
      
      this->indices[f++] = a;
      this->indices[f++] = b;
      this->indices[f++] = c;
      
      this->indices[f++] = c;
      this->indices[f++] = d;
      this->indices[f++] = a;
    }
  }
  
  // south cap
  for(GLuint j = 0; j < NTHETA; j++)
  {
    a = Shape::localToGlobal(NPHI+1,j,NTHETA+1)+1;
    b = Shape::localToGlobal(NPHI+1,j+1,NTHETA+1)+1;
    this->indices[f++] = this->nVerts-1;
    this->indices[f++] = b;
    this->indices[f++] = a;
  }
}

void Cylinder::parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx)
{
  verts[idx++] = this->radius*cos(theta) + this->origin[0];
  verts[idx++] = this->radius*sin(theta) + this->origin[1];
  verts[idx++] = this->height/2.0 - this->height*phi/M_PI + this->origin[2];
}

void Cylinder::normalForCoordinate(GLfloat * coordinate, size_t phase, GLfloat * norms, size_t &idx)
{
  if (phase == 0) {
    norms[idx++] = 0.0;
    norms[idx++] = 0.0;
    norms[idx++] = 1.0;
  } else if (phase == 1) {
    norms[idx++] = (coordinate[0] - this->origin[0])/this->radius;
    norms[idx++] = (coordinate[1] - this->origin[1])/this->radius;
    norms[idx++] = 0.0;
  } else {
    norms[idx++] = 0.0;
    norms[idx++] = 0.0;
    norms[idx++] = -1.0;
  }
}

void Cylinder::draw() {
  this->Shape::draw();
}

void Cylinder::setRadius(GLfloat R) {
  if (R > 0.0 && (this->radius != R)) this->radius = R;
}

void Cylinder::setHeight(GLfloat H) {
  if (H > 0.0 && (this->height != H)) this->height = H;
}
