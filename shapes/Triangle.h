#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"

class Triangle : public Shape
{
public:
  Triangle(GLfloat B = 1.0, GLfloat H = 2.0);
  virtual ~Triangle();
  virtual void draw();
  virtual void init();
  
  GLfloat getBase() const { return this->base; }
  GLfloat getHeight() const { return this->height; }
  
  void setBase(GLfloat B);
  void setHeight(GLfloat H);
  
private:
  
  GLfloat base, height;
  
  virtual void initProperties();
  virtual void updateProperties();

  void constructVerts();
  void constructMesh();
};

#endif // TRIANGLE_H
