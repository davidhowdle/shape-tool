#ifndef TORUS_H
#define TORUS_H

#include "Shape.h"

class Torus : public Shape
{
public:
  Torus(GLfloat rA = 1.5, GLfloat rB = 0.6);
  virtual ~Torus();
  virtual void draw();
  virtual void init(); 
  GLfloat getRadiusA() const { return this->radiusA; }
  GLfloat getRadiusB() const { return this->radiusB; }

  void setRadiusA(GLfloat rA);
  void setRadiusB(GLfloat rB);
  void setRadii(GLfloat rA, GLfloat rB);

private:

  GLfloat radiusA, radiusB; // radiusA = major, radiusB = minor
    
  virtual void initProperties();
  virtual void updateProperties();

  void constructVertsAndNorms();
  void constructMesh();
  void parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx);
  void normalForCoordinate(GLfloat theta, GLfloat phi, GLfloat * norms, size_t &idx);
  void sweepPathNormal(GLfloat s, GLfloat * N);
  void sweepPath(GLfloat s, GLfloat * x);
};

#endif // TORUS_H
