#ifndef ELLIPSE_H
#define ELLIPSE_H

#include "Shape.h"

class Ellipse : public Shape
{
public:
  Ellipse(GLfloat a = 2.0, GLfloat b = 1.0);
  virtual ~Ellipse();
  virtual void draw();
  virtual void init();
  
  GLfloat getRadiusA() const { return this->radiusA; }
  GLfloat getRadiusB() const { return this->radiusB; }

  void setRadiusA(GLfloat a);
  void setRadiusB(GLfloat b);
  void setRadii(GLfloat a, GLfloat b);

private:
    
  GLfloat radiusA, radiusB;

  virtual void initProperties();
  virtual void updateProperties();

  void constructVerts();
  void constructMesh();
};

#endif // ELLIPSE_H
