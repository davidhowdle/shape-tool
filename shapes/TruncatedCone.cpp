#include "TruncatedCone.h"

#define NPHI 2
#define NTHETA 80

using namespace std;

TruncatedCone::TruncatedCone(GLfloat rA, GLfloat rB, GLfloat H)
: radiusA(rA), radiusB(rB), height(H)
{
  this->defaultName("Truncated Cone");
  this->init();
}

TruncatedCone::~TruncatedCone()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void TruncatedCone::init() {
  this->constructVertsAndNorms();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void TruncatedCone::initProperties() {
  // add properties
  this->properties->addProperty("Top Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusA),
                                SCALAR);
  
  this->properties->addProperty("Bottom Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusB),
                                SCALAR);
  
  this->properties->addProperty("Height",
                                PropertyUtilities::toString<GLfloat>(this->height),
                                SCALAR);
}

void TruncatedCone::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat ra = PropertyUtilities::stringToFloat(this->properties->getValue("Top Radius"));
  GLfloat rb = PropertyUtilities::stringToFloat(this->properties->getValue("Bottom Radius"));
  GLfloat h = PropertyUtilities::stringToFloat(this->properties->getValue("Height"));
  
  this->setRadiusA(ra);
  this->setRadiusB(rb);
  this->setHeight(h);
  
  // update
  this->init();
}

void TruncatedCone::constructVertsAndNorms()
{
  // init the sphere!
  GLfloat delta_th = 2.0*M_PI/NTHETA;
  size_t n_verts = 4*(NTHETA+1)+2;
  size_t v = 0;
  size_t n = 0;
  size_t phase = 0;
  
  if(!this->constructed) {
    this->vertices = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
  }
  
  // North pole
  TruncatedCone::parametersToCoordinates(0.0, 0.0, this->radiusA, this->vertices, v);
  TruncatedCone::normalForCoordinate(0,phase,this->normals,n);
  
  for (size_t k = 0; k <= 1; k++) {
    for (size_t i = 0; i <= 1 ; i++) {
      if (k == 1 && i == 1) phase = 2;
      else if(k == 1 && i == 0) phase = 1;
      else phase = i;
      
      for(size_t j = 0; j <= NTHETA; j++) {
        TruncatedCone::parametersToCoordinates(j*delta_th,k*M_PI,(1-k)*this->radiusA + k*this->radiusB,this->vertices, v);
        TruncatedCone::normalForCoordinate(j*delta_th,phase,this->normals,n);
      }
    }
  }
  
  // South pole
  TruncatedCone::parametersToCoordinates(0.0, M_PI, this->radiusB,this->vertices, v);
  TruncatedCone::normalForCoordinate(0,phase,this->normals,n);
  
  this->nVerts = v/3;
}


void TruncatedCone::constructMesh()
{
  
  // set facet count
  if(!this->constructed) {
    this->nFacets = 2*NTHETA + 2*NTHETA;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  // north cap
  size_t f = 0;
  for(GLuint i = 0; i < NTHETA; i++)
  {
    this->indices[f++] = 0;
    this->indices[f++] = i+1;
    this->indices[f++] = i+2;
  }
  
  // body
  size_t a,b,c,d;
  for (size_t i = 1; i < NPHI; i++) {
    for (size_t j = 0; j < NTHETA; j++) {
      a = Shape::localToGlobal(i,j,NTHETA+1)+1;
      b = Shape::localToGlobal(i+1,j,NTHETA+1)+1;
      c = Shape::localToGlobal(i+1,j+1,NTHETA+1)+1;
      d = Shape::localToGlobal(i,j+1,NTHETA+1)+1;
      
      this->indices[f++] = a;
      this->indices[f++] = b;
      this->indices[f++] = c;
      
      this->indices[f++] = c;
      this->indices[f++] = d;
      this->indices[f++] = a;
    }
  }
  
  // south cap
  for(GLuint j = 0; j < NTHETA; j++)
  {
    a = Shape::localToGlobal(NPHI+1,j,NTHETA+1)+1;
    b = Shape::localToGlobal(NPHI+1,j+1,NTHETA+1)+1;
    this->indices[f++] = this->nVerts-1;
    this->indices[f++] = b;
    this->indices[f++] = a;
  }
}

void TruncatedCone::parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat R, GLfloat * verts, size_t &idx)
{
  verts[idx++] = R*cos(theta) + this->origin[0];
  verts[idx++] = R*sin(theta) + this->origin[1];
  verts[idx++] = this->height/2.0 - this->height*phi/M_PI + this->origin[2];
}

void TruncatedCone::normalForCoordinate(GLfloat theta, size_t phase, GLfloat * norms, size_t &idx)
{
  if (phase == 0) {
    norms[idx++] = 0.0;
    norms[idx++] = 0.0;
    norms[idx++] = 1.0;
  } else if (phase == 1) {
    
    GLfloat nrm[3] = {cos(theta), sin(theta), 0.0};
    GLfloat Nr[3] = {(this->radiusA-this->radiusB)*nrm[0],(this->radiusA-this->radiusB)*nrm[1], this->height};
    cblas_sscal(3,1.0/cblas_snrm2(3,Nr,1),Nr,1); // normalize
    static GLfloat N0[3] = {0,0,1};
    static GLfloat O[3] = {0,0,0};
    
    GeometricUtilities::rotatePoint(N0,O,Nr,nrm);
    cblas_scopy(3,nrm,1,&norms[idx],1);
    idx += 3;
    
  } else {
    norms[idx++] = 0.0;
    norms[idx++] = 0.0;
    norms[idx++] = -1.0;
  }
}

void TruncatedCone::draw() {
  this->Shape::draw();
}

void TruncatedCone::setRadiusA(GLfloat rA) {
  if(rA > 0.0 && (this->radiusA != rA)) this->radiusA = rA;
}

void TruncatedCone::setRadiusB(GLfloat rB) {
  if(rB > 0.0 && (this->radiusB != rB)) this->radiusB = rB;
}

void TruncatedCone::setHeight(GLfloat H) {
  if(H > 0.0 && (this->height != H)) this->height = H;
}

