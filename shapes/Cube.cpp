#include "Cube.h"

using namespace std;

Cube::Cube(GLfloat W)
  : Cuboid(W, W, W)
{
  this->defaultName("Cube");
  this->initProperties();
}

void Cube::initProperties() {
  // remove properties from base class
  this->properties->removeProperty("Length");
  this->properties->removeProperty("Depth");
}

Cube::~Cube() {

}

void Cube::updateProperties() {

  this->Shape::updateProperties();

  // update self;
  GLfloat w = PropertyUtilities::stringToFloat(this->properties->getValue("Width"));
  this->setWidth(w);

  // update
  this->init();
}

void Cube::setWidth(GLfloat W) {
  if (W > 0.0 && (this->width != W)) {
    this->setLength(W);
    this->Cuboid::setWidth(W);
    this->setDepth(W);
  }
}

