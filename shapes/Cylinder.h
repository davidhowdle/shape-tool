#ifndef CYLINDER_H
#define CYLINDER_H

#include "Shape.h"

class Cylinder : public Shape
{
public:
  Cylinder(GLfloat R = 0.6, GLfloat H = 3.0);
  virtual ~Cylinder();
  virtual void draw();
  virtual void init();
  
  GLfloat getRadius() const { return this->radius; }
  GLfloat getHeight() const { return this->height; }

  void setRadius(GLfloat R);
  void setHeight(GLfloat H);

private:
  
  GLfloat radius;
  GLfloat height;
    
  virtual void initProperties();
  virtual void updateProperties();

  void constructVertsAndNorms();
  void constructMesh();
  void parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx);
  void normalForCoordinate(GLfloat * coordinate,size_t phase, GLfloat * norms, size_t &idx);
};

#endif // CYLINDER_H
