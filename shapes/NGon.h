#ifndef NGON_H
#define NGON_H

#include "Shape.h"

class NGon : public Shape
{
public:
  NGon(size_t N, GLfloat R = 1.0);
  virtual ~NGon();
  virtual void draw();
  virtual void init();
  
  size_t getSides() const { return this->sides; }
  GLfloat getRadius() const { return this->radius; }
  
  void setSides(size_t N);
  void setRadius(GLfloat R);

private:
  
  size_t sides;
  GLfloat radius;

  virtual void initProperties();
  virtual void updateProperties();
  void tearDown();

  void constructVerts();
  void constructMesh();
  
  static std::string getPolygonName(size_t sides);
};

#endif // NGON_H
