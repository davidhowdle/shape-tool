#ifndef CUBOID_H
#define CUBOID_H

#include "Shape.h"

class Cuboid : public Shape
{
public:
  Cuboid(GLfloat L = 2.0, GLfloat W = 1.0, GLfloat D = 0.5);
  virtual ~Cuboid();
  virtual void draw();
  virtual void init();

  GLfloat getLength() const { return this->length; }
  GLfloat getWidth() const { return this->width; }
  GLfloat getDepth() const { return this->depth; }

  void setLength(GLfloat L);
  virtual void setWidth(GLfloat W);
  void setDepth(GLfloat D);
  
protected:

  virtual void initProperties();
  virtual void updateProperties();

private:

  GLfloat length, width, depth;
    
  void constructVerts();
  void constructMesh();
  void constructNormals();
};

#endif // CUBOID_H
