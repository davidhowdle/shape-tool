#include "Shape.h"

Shape::Shape() : nVerts(0), nFacets(0), constructed(false) {
  this->init();
}

Shape::~Shape() {

  free(this->origin);
  free(this->normal);

}

void Shape::draw() {
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  
  glColor4f(0.3, 1.0, 0.3, 1.0);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  glNormalPointer(GL_FLOAT, 0, this->normals);
  // draw facets!
  glDrawElements(GL_TRIANGLES,this->nFacets*3,GL_UNSIGNED_INT,this->indices);
  
  // deactivate vertex arrays after drawing
  glEnableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
}

void Shape::init() {
  this->origin = (GLfloat *)calloc(3,sizeof(GLfloat));
  this->normal = (GLfloat *)calloc(3,sizeof(GLfloat));
  this->normal[2] = 1.0;
  // init the properties
  this->initProperties();
}

void Shape::initProperties() {
  this->properties = new PropertyManager();
  this->properties->addProperty("Origin",
                                PropertyUtilities::arrayToString(this->origin,3),
                                SCALAR_ARRAY);
}

void Shape::updateProperties() {
    PropertyUtilities::stringToFloats(this->properties->getValue("Origin"),
                          3, this->origin);
}

size_t Shape::localToGlobal(size_t i, size_t j, size_t cols)
{
  return i*cols + j;
}
