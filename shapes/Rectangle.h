#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.h"

class Rectangle : public Shape
{
public:
  Rectangle(GLfloat L = 2, GLfloat W = 1.0);
  virtual ~Rectangle();
  virtual void draw();
  virtual void init();

  GLfloat getLength() const { return this->length; }
  GLfloat getWidth() const { return this->width; }

  void setLength(GLfloat L);
  void setWidth(GLfloat W);

private:
 
  GLfloat length, width; 
  
  virtual void initProperties();
  virtual void updateProperties();

  void constructVerts();
  void constructMesh();
};

#endif // RECTANGLE_H
