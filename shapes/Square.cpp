#include "Square.h"

using namespace std;

Square::Square(GLfloat W) : width(W)
{
  this->defaultName("Square");
  this->init();
}

Square::~Square()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Square::init() {
  this->constructVerts();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Square::initProperties() {
  // add properties
  this->properties->addProperty("Width",
                                PropertyUtilities::toString<GLfloat>(this->width),
                                SCALAR);
}

void Square::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat w = PropertyUtilities::stringToFloat(this->properties->getValue("Width"));
  
  this->setWidth(w);
  
  // update
  this->init();
}

void Square::constructVerts()
{
  if(!this->constructed) {
    this->nVerts = 4;
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }
  
  GLfloat a[2] = {static_cast<GLfloat>(-this->width/2.0), static_cast<GLfloat>(-this->width/2.0)};
  GLfloat b[2] = {static_cast<GLfloat>(this->width/2.0),  static_cast<GLfloat>(-this->width/2.0)};
  GLfloat c[2] = {static_cast<GLfloat>(this->width/2.0),  static_cast<GLfloat>(this->width/2.0)};
  GLfloat d[2] = {static_cast<GLfloat>(-this->width/2.0), static_cast<GLfloat>(this->width/2.0)};
  
  cblas_scopy(2,a,1,&this->vertices[0],1);
  cblas_scopy(2,b,1,&this->vertices[3],1);
  cblas_scopy(2,c,1,&this->vertices[6],1);
  cblas_scopy(2,d,1,&this->vertices[9],1);
  
  for (size_t i = 0; i < 4; i++) {
    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
    this->vertices[3*i] += this->origin[0];
    this->vertices[3*i+1] += this->origin[1];
    this->vertices[3*i+1] += this->origin[2];
  }
}


void Square::constructMesh()
{
  // set facet count
  if(!this->constructed) {
    this->nFacets = 2;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  size_t idx = 0;
  this->indices[idx++] = 0;
  this->indices[idx++] = 1;
  this->indices[idx++] = 2;
  
  this->indices[idx++] = 0;
  this->indices[idx++] = 2;
  this->indices[idx++] = 3;
}


void Square::draw() {
  
  // draw fill
  this->Shape::draw();
  
  // draw outline
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  
  glLineWidth(3.0);
  glColor3f(0.0, 0.0, 0.0);
  glDrawArrays(GL_LINE_LOOP,0,4);
  
  // deactivate vertex arrays after drawing
  glDisableClientState(GL_VERTEX_ARRAY);
}

void Square::setWidth(GLfloat W) {
  if (W > 0.0 && (this->width != W)) this->width = W;
}
