#ifndef SHAPE_H
#define SHAPE_H

#include <QtOpenGL>

#include <map>
#include <cstdio>
#include <iostream>

#include "CADObject.h"
#include "PropertyManager.h"
#include "GeometricUtilities.h"

class Shape : public CADObject {

public:
                     	   Shape();
  virtual                 ~Shape();

  virtual void       	   draw();
  virtual void       	   init();

  virtual PropertyManager* getPropMgmt() {return this->properties;}
  virtual void        updateProperties();

protected:

  // important shit
  GLfloat * vertices;
  GLfloat * normals;
  GLuint * indices;
  size_t nVerts;
  size_t nFacets;
  bool constructed;

  GLfloat * origin;
  GLfloat * normal;

  virtual void        initProperties();

  static size_t localToGlobal(size_t i, size_t j, size_t cols);
};

Q_DECLARE_METATYPE(Shape*);

#endif // SHAPE_H
