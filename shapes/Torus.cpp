#include "Torus.h"

#define NPHI 40
#define NTHETA 40

using namespace std;

Torus::Torus(GLfloat rA, GLfloat rB)
: radiusA(rA), radiusB(rB)
{
  this->defaultName("Torus");
  this->init();
}

Torus::~Torus()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Torus::init() {
  this->constructVertsAndNorms();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Torus::initProperties() {
  // add properties
  this->properties->addProperty("Major Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusA),
                                SCALAR);
  
  this->properties->addProperty("Minor Radius",
                                PropertyUtilities::toString<GLfloat>(this->radiusB),
                                SCALAR);
}

void Torus::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat ra = PropertyUtilities::stringToFloat(this->properties->getValue("Major Radius"));
  GLfloat rb = PropertyUtilities::stringToFloat(this->properties->getValue("Minor Radius"));
  
  this->setRadii(ra, rb);
  
  // update
  this->init();
}

void Torus::constructVertsAndNorms()
{
  // init the sphere!
  GLfloat delta_th = 2.0*M_PI/NTHETA;
  GLfloat delta_phi = M_PI/NPHI;
  size_t n_verts = (NPHI+1)*(NTHETA+1);
  size_t v = 0;
  size_t n = 0;
  
  if(!this->constructed) {
    this->vertices = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
  }
  
  // body
  for(size_t i = 0; i <= NPHI; i++) {
    for(size_t j = 0; j <= NTHETA; j++) {
      Torus::parametersToCoordinates(j*delta_th,i*delta_phi, this->vertices, v);
      Torus::normalForCoordinate(j*delta_th,i*delta_phi,this->normals,n);
    }
  }
  
  this->nVerts = v/3;
}


void Torus::constructMesh()
{
  
  // set facet count
  if(!this->constructed) {
    this->nFacets = 2*NTHETA*NPHI;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  // body
  size_t f = 0;
  size_t a,b,c,d;
  
  for (size_t i = 0; i < NPHI; i++) {
    for (size_t j = 0; j < NTHETA; j++) {
      
      a = Shape::localToGlobal(i,j,NTHETA+1);
      b = Shape::localToGlobal(i+1,j,NTHETA+1);
      c = Shape::localToGlobal(i+1,j+1,NTHETA+1);
      d = Shape::localToGlobal(i,j+1,NTHETA+1);
      
      this->indices[f++] = a;
      this->indices[f++] = c;
      this->indices[f++] = b;
      
      this->indices[f++] = c;
      this->indices[f++] = a;
      this->indices[f++] = d;
    }
  }
}

void Torus::parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx)
{
  GLfloat vrt[3] = {this->radiusB*cos(theta), this->radiusB*sin(theta), 0.0};
  
  GLfloat Oc[3], N[3];
  Torus::sweepPath(phi/M_PI,Oc);
  Torus::sweepPathNormal(phi/M_PI,N);
  
  static  GLfloat O[3] = {0,0,0};
  static  GLfloat N0[3] = {0,0,1.0};
  
  GeometricUtilities::rotatePoint(N0,O,N,vrt);
  cblas_saxpy(3,1.0,Oc,1,vrt,1);
  
  // shift to origin
  for(size_t i = 0; i < 3; i++) vrt[i] += this->origin[i];
  cblas_scopy(3,vrt,1,&verts[idx],1);
  idx+=3;
}

void Torus::normalForCoordinate(GLfloat theta, GLfloat phi, GLfloat * norms, size_t &idx)
{
  GLfloat Nrm[3] = {cos(theta), sin(theta), 0.0};
  
  GLfloat Oc[3], N[3];
  Torus::sweepPath(phi/M_PI,Oc);
  Torus::sweepPathNormal(phi/M_PI,N);
  
  static  GLfloat O[3] = {0,0,0};
  static  GLfloat N0[3] = {0,0,1.0};
  
  GeometricUtilities::rotatePoint(N0,O,N,Nrm);
  cblas_scopy(3,Nrm,1,&norms[idx],1);
  idx+=3;
}

void Torus::sweepPath(GLfloat s, GLfloat * x) {
  x[0] = this->radiusA * cos(2.0*M_PI*s);
  x[1] = this->radiusA * sin(2.0*M_PI*s);
  x[2] = 0.0;
}

void Torus::sweepPathNormal(GLfloat s, GLfloat * N) {
  N[0] = -sin(2.0*M_PI*s);
  N[1] = cos(2.0*M_PI*s);
  N[2] = 0.0;
}

void Torus::draw() {
  this->Shape::draw();
}

void Torus::setRadiusA(GLfloat rA) {
  if (rA > 0.0
      && (this->radiusA != rA)
      && (rA > this->radiusB)) this->radiusA = rA;
}

void Torus::setRadiusB(GLfloat rB) {
  if (rB > 0.0
      && (this->radiusB != rB)
      && (rB < this->radiusA)) this->radiusB = rB;
}

void Torus::setRadii(GLfloat rA, GLfloat rB) {
  this->setRadiusA(rA);
  this->setRadiusB(rB);
}
