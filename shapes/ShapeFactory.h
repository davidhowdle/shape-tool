#ifndef SHAPEFACTORY_H
#define SHAPEFACTORY_H

#include "Shape.h"

#include <string>

class ShapeFactory
{
public:

  static Shape* create(const std::string& name);
};

#endif // SHAPEFACTORY_H
