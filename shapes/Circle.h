#ifndef CIRCLE_H
#define CIRCLE_H

#include "Shape.h"

class Circle : public Shape
{
public:
  Circle(GLfloat R = 1.0);
  virtual ~Circle();
  virtual void draw();
  virtual void init();
  
  // setters and getters
  GLfloat getRadius() const { return this->radius; }
  void setRadius(GLfloat R);

private:
  
  GLfloat radius; 
  
  virtual void initProperties();
  virtual void updateProperties();
  void constructVerts();
  void constructMesh();
};

#endif // CIRCLE_H
