#ifndef ANNULUS_H
#define ANNULUS_H

#include "Shape.h"

class Annulus : public Shape
{
public:
  Annulus(GLfloat rA = 0.5, GLfloat rB = 1.1);
  virtual ~Annulus();
  virtual void draw();
  virtual void init();
  
  GLfloat getRadiusA() const { return this->radiusA; }
  GLfloat getRadiusB() const { return this->radiusB; }

  void setRadiusA(GLfloat rA);
  void setRadiusB(GLfloat rB);
  void setRadii(GLfloat rA, GLfloat rB);

private:
  
  GLfloat radiusA, radiusB; // radiusA = outer , radiusB = ring
  
  virtual void initProperties();
  virtual void updateProperties();
  void constructVerts();
  void constructMesh();
};

#endif // ANNULUS_H
