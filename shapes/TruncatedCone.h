#ifndef TRUNCATED_CONE_H
#define TRUNCATED_CONE_H

#include "Shape.h"

class TruncatedCone : public Shape
{
public:
  TruncatedCone(GLfloat rA = 0.3, GLfloat rB = 0.6, GLfloat H = 1.0);
  virtual ~TruncatedCone();
  virtual void draw();
  virtual void init();

  GLfloat getRadiusA() const { return this->radiusA; }
  GLfloat getRadiusB() const { return this->radiusB; }
  GLfloat getHeight() const { return this->height; }

  void setRadiusA(GLfloat rA);
  void setRadiusB(GLfloat rB);
  void setHeight(GLfloat H);

private:

  GLfloat radiusA, radiusB;
  GLfloat height;
    
  virtual void initProperties();
  virtual void updateProperties();

  void constructVertsAndNorms();
  void constructMesh();
  void constructNormals();
  void parametersToCoordinates(GLfloat theta, GLfloat phi,GLfloat R, GLfloat * verts, size_t &idx);
  void normalForCoordinate(GLfloat theta, size_t phase, GLfloat * norms, size_t &idx);

};

#endif // TRUNCATED_CONE_H
