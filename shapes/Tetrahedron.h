#ifndef TETRAHEDRON_H
#define TETRAHEDRON_H

#include "Shape.h"

class Tetrahedron : public Shape
{
public:
  Tetrahedron(GLfloat L = 1.0, GLfloat W = 1.0, GLfloat D = 1.0);
  virtual ~Tetrahedron();
  virtual void draw();
  void init();
  
  GLfloat getLength() const { return this->length; }
  GLfloat getWidth() const { return this->width; }
  GLfloat getDepth() const { return this->depth; }

  void setLength(GLfloat L);
  void setWidth(GLfloat W);
  void setDepth(GLfloat D);

private:

  GLfloat length, width, depth;
    
  virtual void initProperties();
  virtual void updateProperties();

  void constructVerts();
  void constructMesh();
  void constructNormals();
};

#endif // TETRAHEDRON_H
