#include "ShapeFactory.h"

#include "Tetrahedron.h"
#include "Cuboid.h"
#include "Cube.h"
#include "Cone.h"
#include "Cylinder.h"
#include "Torus.h"
#include "Ellipsoid.h"
#include "Sphere.h"
#include "TruncatedCone.h"

#include "Square.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Ellipse.h"
#include "Triangle.h"
#include "NGon.h"
#include "Annulus.h"

Shape* ShapeFactory::create(const std::string& name) {
  Shape* shape = 0;
  if (name == "Tetrahedron")
    shape = new Tetrahedron();
  if (name == "Cube")
    shape = new Cube();
  if (name == "Cuboid")
    shape = new Cuboid();
  if (name == "Cone")
    shape = new Cone();
  if (name == "Cylinder")
    shape = new Cylinder();
  if (name == "Torus")
    shape = new Torus();
  if (name == "Ellipsoid")
    shape = new Ellipsoid();
  if (name == "Sphere")
    shape = new Sphere();
  if (name == "Truncated Cone")
    shape = new TruncatedCone();
  if (name == "Square")
    shape = new Square();
  if (name == "Rectangle")
    shape = new Rectangle();
  if (name == "Circle")
    shape = new Circle();
  if (name == "Ellipse")
    shape = new Ellipse();
  if (name == "Triangle")
    shape = new Triangle();
  if (name == "Pentagon")
    shape = new NGon(5);
  if (name == "Hexagon")
    shape = new NGon(6);
  if (name == "Annulus")
    shape = new Annulus();
  return shape;
}
