#include "Triangle.h"

using namespace std;

Triangle::Triangle(GLfloat B, GLfloat H)
: base(B), height(H)
{
  this->defaultName("Triangle");
  this->init();
}

Triangle::~Triangle()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Triangle::init() {
  this->constructVerts();
  this->constructMesh();
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Triangle::initProperties() {
  // add properties
  this->properties->addProperty("Base",
                                PropertyUtilities::toString<GLfloat>(this->base),
                                SCALAR);
  
  this->properties->addProperty("Height",
                                PropertyUtilities::toString<GLfloat>(this->height),
                                SCALAR);
}

void Triangle::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat b = PropertyUtilities::stringToFloat(this->properties->getValue("Base"));
  GLfloat h = PropertyUtilities::stringToFloat(this->properties->getValue("Height"));
  
  this->setBase(b);
  this->setHeight(h);
  
  // update
  this->init();
}

void Triangle::constructVerts()
{
  if(!this->constructed) {
    this->nVerts = 3;
    this->vertices = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(this->nVerts*3,sizeof(GLfloat));
  }
  
  GLfloat a[2] = {static_cast<GLfloat>(-this->base/3.0),    static_cast<GLfloat>(-this->height/3.0)};
  GLfloat b[2] = {static_cast<GLfloat>(2.0*this->base/3.0), static_cast<GLfloat>(-this->height/3.0)};
  GLfloat c[2] = {static_cast<GLfloat>(-this->base/3.0),    static_cast<GLfloat>(2.0*this->height/3.0)};
  
  cblas_scopy(2,a,1,&this->vertices[0],1);
  cblas_scopy(2,b,1,&this->vertices[3],1);
  cblas_scopy(2,c,1,&this->vertices[6],1);
  
  for (size_t i = 0; i < 3; i++) {
    cblas_scopy(3,this->normal,1,&this->normals[3*i],1);
    this->vertices[3*i] += this->origin[0];
    this->vertices[3*i+1] += this->origin[1];
    this->vertices[3*i+1] += this->origin[2];
  }
}


void Triangle::constructMesh()
{
  // set facet count
  if(!this->constructed) {
    this->nFacets = 1;
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  size_t idx = 0;
  this->indices[idx++] = 0;
  this->indices[idx++] = 1;
  this->indices[idx++] = 2;
}


void Triangle::draw() {
  
  // draw fill
  this->Shape::draw();
  
  // draw outline
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  
  glLineWidth(3.0);
  glColor3f(0.0, 0.0, 0.0);
  glDrawArrays(GL_LINE_LOOP,0,3);
  
  // deactivate vertex arrays after drawing
  glDisableClientState(GL_VERTEX_ARRAY);
}

void Triangle::setBase(GLfloat B) {
  if (B > 0.0 && (this->base != B)) this->base = B;
}

void Triangle::setHeight(GLfloat H) {
  if (H > 0.0 && (this->height != H)) this->height = H;
}
