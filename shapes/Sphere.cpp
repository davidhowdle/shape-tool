#include "Sphere.h"

#define NPHI 40
#define NTHETA 80

using namespace std;

Sphere::Sphere(GLfloat R) : radius(R)
{
  this->defaultName("Sphere");
  this->init();
}

Sphere::~Sphere()
{
  if (this->constructed)
  {
    free(this->vertices);
    free(this->normals);
    free(this->indices);
  }
}

void Sphere::init() {
  this->constructVertsAndNorms();
  this->constructMesh();
  
  if(!this->constructed) {
    this->initProperties();
    this->constructed = true;
  }
}

void Sphere::initProperties() {
  // add properties
  this->properties->addProperty("Radius",
                                PropertyUtilities::toString<GLfloat>(this->radius),
                                SCALAR);
}

void Sphere::updateProperties() {
  
  this->Shape::updateProperties();
  
  // update self;
  GLfloat r = PropertyUtilities::stringToFloat(this->properties->getValue("Radius"));
  this->setRadius(r);
  
  // update
  this->init();
}

void Sphere::constructVertsAndNorms()
{
  // init the sphere!
  GLfloat delta_th = 2.0*M_PI/NTHETA;
  GLfloat delta_phi = M_PI/NPHI;
  size_t n_verts = (NPHI-1)*(NTHETA+1)+2;
  size_t v = 0;
  size_t n = 0;
  
  if(!this->constructed) {
    this->vertices = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
    this->normals = (GLfloat *)calloc(n_verts*3,sizeof(GLfloat));
  }
  
  // North pole
  Sphere::parametersToCoordinates(0.0, 0.0, this->vertices, v);
  Sphere::normalForCoordinate(&this->vertices[n],this->normals,n);
  
  for(size_t i = 1; i < NPHI; i++) {
    for(size_t j = 0; j <= NTHETA; j++) {
      Sphere::parametersToCoordinates(j*delta_th,i*delta_phi, this->vertices, v);
      Sphere::normalForCoordinate(&this->vertices[n],this->normals,n);
    }
  }
  
  // South pole
  Sphere::parametersToCoordinates(0.0, M_PI, this->vertices, v);
  Sphere::normalForCoordinate(&this->vertices[n],this->normals,n);
  
  this->nVerts = v/3;
}


void Sphere::constructMesh()
{
  
  // set facet count
  if(!this->constructed) {
    this->nFacets = 2*NTHETA + 2*NTHETA*(NPHI-2);
    this->indices = (GLuint *)calloc(this->nFacets*3,sizeof(GLuint));
  }
  
  // north cap
  size_t f = 0;
  for(GLuint i = 0; i < NTHETA; i++)
  {
    this->indices[f++] = 0;
    this->indices[f++] = i+1;
    this->indices[f++] = i+2;
  }
  
  // body
  size_t a,b,c,d;
  for (size_t i = 0; i < NPHI-2; i++) {
    for (size_t j = 0; j < NTHETA; j++) {
      a = Shape::localToGlobal(i,j,NTHETA+1)+1;
      b = Shape::localToGlobal(i+1,j,NTHETA+1)+1;
      c = Shape::localToGlobal(i+1,j+1,NTHETA+1)+1;
      d = Shape::localToGlobal(i,j+1,NTHETA+1)+1;
      
      this->indices[f++] = a;
      this->indices[f++] = b;
      this->indices[f++] = c;
      
      this->indices[f++] = c;
      this->indices[f++] = d;
      this->indices[f++] = a;
    }
  }
  
  // south cap
  for(GLuint j = 0; j < NTHETA; j++)
  {
    a = Shape::localToGlobal(NPHI-2,j,NTHETA+1)+1;
    b = Shape::localToGlobal(NPHI-2,j+1,NTHETA+1)+1;
    this->indices[f++] = this->nVerts-1;
    this->indices[f++] = b;
    this->indices[f++] = a;
  }
}

void Sphere::parametersToCoordinates(GLfloat theta, GLfloat phi, GLfloat * verts, size_t &idx)
{
  verts[idx++] = this->radius*cos(theta)*sin(phi) + this->origin[0];
  verts[idx++] = this->radius*sin(theta)*sin(phi) + this->origin[1];
  verts[idx++] = this->radius*cos(phi) + this->origin[2];
}

void Sphere::normalForCoordinate(GLfloat * coordinate, GLfloat * norms, size_t &idx)
{
  for(int i = 0; i < 3; i++) norms[idx++] = (coordinate[i] - this->origin[i]) / this->radius;
}

void Sphere::draw() {
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  
  glColor3f(0.3, 1.0, 0.3);
  glVertexPointer(3, GL_FLOAT, 0, this->vertices);
  glNormalPointer(GL_FLOAT, 0, this->normals);
  // draw facets!
  glDrawElements(GL_TRIANGLES,this->nFacets*3,GL_UNSIGNED_INT,this->indices);
  
  // deactivate vertex arrays after drawing
  glEnableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
}

void Sphere::setRadius(GLfloat R)
{
  if(R > 0.0 && (this->radius != R)) this->radius = R;
}
