#include "CADObject.h"
#include "PropertyManager.h"

CADObject::CADObject() : name("CADObject") {
  this->properties = new PropertyManager;
}

CADObject::~CADObject() {
  delete this->properties;
}

void CADObject::defaultName(const std::string& nm) {
  std::map<std::string, size_t>::iterator itr = names.find(nm);
  if (itr == names.end()) {
    names.insert(std::make_pair(nm, 0));
    itr = names.find(nm);
  } else ++(itr->second);
  std::stringstream ss;
  ss << nm << itr->second;
  this->setName(ss.str());
}

std::string CADObject::getName() {return name;}

void CADObject::setName(const std::string& nm) {
  name = nm;
}

std::map<std::string, size_t> CADObject::names = std::map<std::string, size_t>();
