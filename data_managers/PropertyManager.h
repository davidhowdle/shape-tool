#ifndef PROPERTY_MANAGER_H
#define PROPERTY_MANAGER_H

#include "ObjectInterfaceManager.h"
#include "CADObject.h"
#include "PropertyUtilities.h"
#include <deque>

typedef std::deque<CADObject *> shape_queue;

class PropertyManager;
struct proxy_return_type {
  PropertyManager const * owner;
  const std::string &key;
  proxy_return_type(PropertyManager const * obj, const std::string &k) : owner(obj), key(k) {};

  operator std::string() const;
  operator shape_queue*() const;
  operator void*() const; 
};

class PropertyManager : public ObjectInterfaceManager
{

public:

  PropertyManager(){ };
  ~PropertyManager();

  using ObjectInterfaceManager::setValue;
  virtual void setValue(const std::string & key, const std::string &value);
  virtual void setValue(const std::string & key, shape_queue * value);

  proxy_return_type getValue(const std::string &key) const; 
  std::string getStringValue(const std::string & key) const;
  shape_queue * getShapeQueue(const std::string &key) const;

  using ObjectInterfaceManager::addProperty;
  virtual void addProperty(const std::string &key, const std::string &value, OBJECT_TYPE type);
  virtual void addProperty(const std::string &key, shape_queue * value, OBJECT_TYPE type);

private:
};
#endif // PROPERTY_MANAGER
