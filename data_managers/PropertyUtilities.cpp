#include "PropertyUtilities.h"

int PropertyUtilities::stringToInt(const std::string &s) {
  return std::stoi(s);
}

size_t PropertyUtilities::stringToUnsigned(const std::string &s) {
  return std::stoul(s);
}

float PropertyUtilities::stringToFloat(const std::string &s) {
  return std::stof(s);
}

void PropertyUtilities::stringToFloats(const std::string &s, size_t N, float * V) {
  size_t pos0 = 0;
  size_t pos = 0;
  size_t idx = 0;
  const std::string comma = ",";
  while(idx < N && pos != std::string::npos) {
    pos = s.find(comma,pos0);
    if (pos != std::string::npos) {
      V[idx++] = PropertyUtilities::stringToFloat(s.substr(pos0,pos-pos0));
      pos0 = pos + 1;
    }
  }
  V[idx++] = PropertyUtilities::stringToFloat(s.substr(pos0,s.length()-pos0));
}

