! include(../common.pri) {
error("Couldn't find config file!")
}

TEMPLATE=lib
CONFIG += staticlib
INCLUDEPATH += ../shapes ../gui
HEADERS=ObjectInterfaceManager.h  PropertyManager.h   PropertyUtilities.h
SOURCES=ObjectInterfaceManager.cpp  PropertyManager.cpp   PropertyUtilities.cpp
