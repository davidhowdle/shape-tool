#ifndef OBJECT_INTERFACE_MANAGER_H
#define OBJECT_INTERFACE_MANAGER_H

#include <map>
#include <vector>
#include <deque>
#include <string>

enum OBJECT_TYPE {
                    INT,
                    SCALAR,
                    INT_ARRAY,
                    SCALAR_ARRAY,
                    STRING,
                    CAD_OBJECT,
                    CAD_OBJECT_ARRAY,
                    INT_OPT,
                    SCALAR_OPT,
                    INT_ARRAY_OPT,
                    SCALAR_ARRAY_OPT,
                    STRING_OPT,
                    CAD_OBJECT_OPT,
                    CAD_OBJECT_ARRAY_OPT,
                    BOOLEAN
                  };

struct ObjectDetail {
  OBJECT_TYPE type;
  void* value;
};

typedef std::map<std::string, ObjectDetail> property_map;

class ObjectInterfaceManager {
public:
  ObjectInterfaceManager();
  virtual ~ObjectInterfaceManager();
  virtual property_map * getProperties() const { return this->properties;}
  
  virtual void setValue(const std::string & key, void * value);
  void * getValue(const std::string & key) const;
  virtual bool hasProperty(const std::string & key) const;
  virtual void addProperty(const std::string &key, void * value, OBJECT_TYPE type);
  virtual void addProperty(const std::string &key, const ObjectDetail &detail);
  virtual void removeProperty(const std::string &key);

protected:
  property_map * properties;
};
#endif //OBJECT_INTERFACE_MANAGER_H
