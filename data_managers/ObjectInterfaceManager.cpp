#include "ObjectInterfaceManager.h"
#include <iostream>

ObjectInterfaceManager::ObjectInterfaceManager() { 
  this->properties = new property_map;
}
ObjectInterfaceManager::~ObjectInterfaceManager() { delete this->properties; }

void ObjectInterfaceManager::setValue(const std::string & key, void * value) {
 if(!this->hasProperty(key)) return;
 this->properties->operator[](key).value = value;
}

void * ObjectInterfaceManager::getValue(const std::string & key) const { 
 if(!this->hasProperty(key)) return nullptr;
 return this->properties->operator[](key).value;
}

bool ObjectInterfaceManager::hasProperty(const std::string & key) const {
  return this->properties->find(key) != this->properties->end();
}

void ObjectInterfaceManager::addProperty(const std::string &key, void * value, OBJECT_TYPE type) {
  ObjectDetail detail = {type, value};
  if (this->hasProperty(key)) {
    this->properties->operator[](key) = detail;
  } else {
    this->properties->insert(std::make_pair(key, detail));
  }
}

void ObjectInterfaceManager::addProperty(const std::string &key, const ObjectDetail &detail) {
  if (this->hasProperty(key)) this->properties->operator[](key) = detail;
  else this->properties->insert(std::make_pair(key, detail));
}

void ObjectInterfaceManager::removeProperty(const std::string &key) {
  if (this->hasProperty(key)) this->properties->erase(key);
}

