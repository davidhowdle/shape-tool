#ifndef PROPERTY_UTILITIES_H
#define PROPERTY_UTILITIES_H
#include <sstream>
#include <string>

namespace PropertyUtilities {

  template<typename T>
  std::string toString(const T & value) {
    std::ostringstream oss;
    oss << value;
    return oss.str();
  }
  
  template<typename T>
  std::string arrayToString(const T & value, size_t N) {
    std::ostringstream oss;
    for (size_t i = 0; i < N-1; i++) oss << value[i] << ", ";
    oss << value[N-1];
    return oss.str();
  }

  int stringToInt(const std::string &s);
  size_t stringToUnsigned(const std::string &s);
  float stringToFloat(const std::string &s);
  void stringToFloats(const std::string &s,size_t N, float * V);
};
#endif //PROPERTY_UTILITIES_H
