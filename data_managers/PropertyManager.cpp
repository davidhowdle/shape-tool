#include "PropertyManager.h"

PropertyManager::~PropertyManager() {
  for (auto k : *this->properties) {
    switch (k.second.type) {
      case CAD_OBJECT:
      case CAD_OBJECT_ARRAY:break;
      default: {
        std::string * value_s = static_cast<std::string *>(k.second.value);
        delete value_s; } break;
    } 
  }
}

void PropertyManager::setValue(const std::string & key,
                               const std::string &value) {
  std::string * value_s;
  if(this->hasProperty(key)) {
    value_s = static_cast<std::string *>(this->ObjectInterfaceManager::getValue(key));
    *value_s = value;
  }else {
    value_s = new std::string(value);
  }
  this->ObjectInterfaceManager::setValue(key, (void *)value_s);
}

void PropertyManager::setValue(const std::string & key, shape_queue* value) {
  this->ObjectInterfaceManager::setValue(key, (void *)value);
}

proxy_return_type PropertyManager::getValue(const std::string &key) const {
  return proxy_return_type(this, key);
}

std::string PropertyManager::getStringValue(const std::string &key) const {
  void * val = this->ObjectInterfaceManager::getValue(key);
  if(val) return *static_cast<std::string *>(val);
  else return "";
}

shape_queue * PropertyManager::getShapeQueue(const std::string &key) const {
  void * val = this->ObjectInterfaceManager::getValue(key);
  if(val) return static_cast<shape_queue *>(val);
  else return nullptr;
}

void PropertyManager::addProperty(const std::string &key,
                                  const std::string &value,
                                  OBJECT_TYPE type) {
    std::string * value_s = new std::string(value);
    this->ObjectInterfaceManager::addProperty(key, (void *)value_s, type);
}

void PropertyManager::addProperty(const std::string &key,
                                  shape_queue * value,
                                  OBJECT_TYPE type) {
  this->ObjectInterfaceManager::addProperty(key, (void *)value, type);
}

proxy_return_type::operator std::string() const {
  return this->owner->getStringValue(this->key);
}

proxy_return_type::operator shape_queue*() const {
  return this->owner->getShapeQueue(this->key);
}

proxy_return_type::operator void*() const {
  return this->owner->ObjectInterfaceManager::getValue(this->key);
}
