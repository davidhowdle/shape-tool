#include "BSP2D.h"
#include <stdio.h>
#include <stack>
#include <sstream>
#include <cstdlib>

int testTree();
int testClipA();
int testClipB();
int testUninon();
int testDifference();
int testIntersection();

const std::string file_path = "/Users/davidson/shapes/shape-tool/unit_tests/graph_files/";
const std::string python_path = "/Users/davidson/shapes/shape-tool/unit_tests/python_scripts/";
const std::string python_script = "bsp_tree_plot.py";

int main(int argc, char ** argv) {
  printf("Testing 2D BSP Tree structure.\n\n");
  
  // test 1.
  printf("Test 1. testTree...\n");
  if(testTree() ) { 
    printf("Test1. testTree FAILED.\n");
    return 1;
  }

  printf("Test 2. tetstClipA...\n");
  if(testClipA() ) {
    printf("Test2. testClipA FAILED.\n");
    return 1;
  }
  
  printf("Test 3. testClipB...\n");
  if(testClipB() ) {
    printf("Test3. testClipB FAILED.\n");
    return 1;
  }
  
  printf("Test 4. testUnion...\n");
  if(testUninon() ) {
    printf("Test4. testUnion FAILED.\n");
    return 1;
  }
  
  printf("Test 5. testDifference...\n");
  if(testDifference() ) {
    printf("Test5. testDifference FAILED.\n");
    return 1;
  }
  
  printf("Test 6. testIntersection...\n");
  if(testIntersection() ) {
    printf("Test6. testIntersection FAILED.\n");
    return 1;
  }
  
  printf("ALL TESTS PASSED!\n\n");
  return 0;
}

void quadForTesting(float w, float h, float * O, struct CSG::bsp2d_data * data) {
  
  CSG::point p0{{ -w/2  + O[0],   -h/2  + O[1], O[2] }};
  CSG::point p1{{ w/2   + O[0],   -h/2  + O[1], O[2] }};
  CSG::point p2{{ w/2   + O[0],   h/2   + O[1], O[2] }};
  CSG::point p3{{ -w/2  + O[0],   h/2   + O[1], O[2] }};

  size_t off = data->verts.size();
  CSG::edge e0(0 + off, 1 + off);
  CSG::edge e1(1 + off, 2 + off);
  CSG::edge e2(2 + off, 3 + off);
  CSG::edge e3(3 + off, 0 + off);
  
  data->verts.insert(data->verts.end(), {p0, p1, p2, p3});
  
  auto tmpE = {e0, e1, e2, e3};
  data->element_map.insert(data->element_map.end(), tmpE);

  float N0[3] = {0.0, 0.0, 1.0};
  for(auto &e : tmpE) {
    data->normals.push_back(CSG::edge::planeForEdge(e, data->verts, N0) );
  }
}

void graph_vis(const std::vector<std::string> &files,
    const std::string &save_as) {
  std::ostringstream oss;
  oss << "python " << python_path + python_script;
  oss << " -f";
  for(auto f : files) oss << " "  << file_path + f;
  oss << " -o " << python_path + save_as;
  std::system(oss.str().c_str());
}

void open_graph(const std::string &file_name) {
  std::ostringstream oss;
  oss << "open " + python_path + file_name;
  std::system(oss.str().c_str());
}

int testTree() {
  
  CSG::bsp2d_data data;
  float w = 2.0;
  float h = 2.0;
  float O[3] = {0.0, 0.0, 0.0};
  quadForTesting(w, h, O, &data);

  CSG::bsp2d_node * T = nullptr;
  for(size_t e = 0; e < data.element_map.size(); e++) {
    CSG::insert(T, e, &data);
  }
    
  // count left, and right branches
  size_t l = 0; size_t r = 0;
  std::stack<CSG::bsp2d_node *> s; s.push(T);
  while (!s.empty()) {
    auto node = s.top();
    s.pop();
    if(node->left) {
      l++;
      s.push(node->left);
    }
    
    if(node->right) {
      r++;
      s.push(node->right);
    }
  }
  
  if (r > 0) {
    printf("FAILURE:: Found %lu right children. \
           Tree should be convex!!!\n",r);
    destroy(T);
    return 1;
  }
  
  if (l != data.element_map.size()-1) {
    printf("FAILURE:: Found %lu left children. \
           Tree should have been equal to number of edges - 1 !!!\n",l);
    destroy(T);
    return 1;
  }

  // print tree
  writeToFile(T, file_path, "quad_tree.txt");
  // generate graph
  graph_vis({"quad_tree.txt"}, "quad_tree.png");
  open_graph("quad_tree.png"); 

  destroy(T);
  printf("testTree: SUCCESS!\n");
  return 0;
}

int testClipA() {
  
  // Create data
  CSG::bsp2d_data data;
  float w = 2.0;
  float h = 2.0;
  float OA[3] = {0.0, 0.0, 0.0};
  quadForTesting(w, h, OA, &data);
  
  float OB[3] = {w/2, -h/2, 0.0};
  quadForTesting(w, h, OB, &data);
  
  // Make trees
  CSG::bsp2d_node * TA = nullptr;
  for(size_t e = 0; e < 4; e++) {
    CSG::insert(TA, e, &data);
  }
  
  CSG::bsp2d_node * TB = nullptr;
  for(size_t e = 4; e < 8; e++) {
    CSG::insert(TB, e, &data);
  }
  
  // clip shape B by shape A
  std::vector<size_t> a_b;
  std::vector<size_t> eB;
  print(TB, eB);
  for(auto &e : eB) {
    clip(TA, e, &data, CSG::NONE, CSG::A_AUB, a_b);
  }

  if (a_b.size() != 5) {
    printf("FAILURE: %lu segments after clipping shape B by shape A.\n", a_b.size());
    printf("Expected 5 segemnts.\n\n");
    destroy(TA);
    destroy(TB);
    return 1;
  }
 
  destroy(TA);
  destroy(TB);
  printf("testClipA: SUCCESS!\n");
  return 0;
}

int testClipB() {
  // Create data
  CSG::bsp2d_data data;
  float w = 2.0;
  float h = 2.0;
  float OA[3] = {0.0, 0.0, 0.0};
  quadForTesting(w, h, OA, &data);
  
  float OB[3] = {w/2, -h/2, 0.0};
  quadForTesting(w, h, OB, &data);
  
  // Make trees
  CSG::bsp2d_node * TA = nullptr;
  for(size_t e = 0; e < 4; e++) {
    CSG::insert(TA, e, &data);
  }
  
  CSG::bsp2d_node * TB = nullptr;
  for(size_t e = 4; e < 8; e++) {
    CSG::insert(TB, e, &data);
  }
  
  // clip shape B by shape A
  std::vector<size_t> b_a;
  std::vector<size_t> eA;
  print(TA, eA);
  for(auto &e : eA) {
    clip(TB, e, &data, CSG::NONE, CSG::B_AUB, b_a);
  }

  if (b_a.size() != 5) {
    printf("FAILURE: %lu segments after clipping shape A by shape B.\n", b_a.size());
    printf("Expected 5 segemnts.\n\n");
    destroy(TA);
    destroy(TB);
    return 1;
  }
 
  destroy(TA);
  destroy(TB);
  printf("testClipB: SUCCESS!\n");
  return 0;
}

int testUninon() {
  // Create data
  CSG::bsp2d_data data;
  float w = 2.0;
  float h = 2.0;
  float OA[3] = {0.0, 0.0, 0.0};
  quadForTesting(w, h, OA, &data);
  
  float OB[3] = {-w/2, 0, 0.0};
  w /= 2;
  h /= 2;
  quadForTesting(w, h, OB, &data);
  
  // Make trees
  // Make trees
  CSG::bsp2d_node * TA = nullptr;
  for(size_t e = 0; e < 4; e++) {
    CSG::insert(TA, e, &data);
  }
  
  CSG::bsp2d_node * TB = nullptr;
  for(size_t e = 4; e < 8; e++) {
    CSG::insert(TB, e, &data);
  }
  
  // clip shape B by shape A
  std::vector<size_t> a_b;
  std::vector<size_t> eB;
  print(TB, eB);
  for(auto &e : eB) {
    clip(TA, e, &data, CSG::NONE, CSG::A_AUB, a_b);
  }

  // clip shape A by shape B
  std::vector<size_t> b_a;
  std::vector<size_t> eA;
  print(TA, eA);
  for(auto &e : eA) {
    clip(TB, e, &data, CSG::NONE, CSG::B_AUB, b_a);
  }
 
 // merge shape
  b_a.insert(b_a.end(), a_b.begin(), a_b.end()); 

  // complete the procedure
  CSG::bsp2d_node * TC = nullptr;
  for(auto e : b_a) {
    CSG::insert(TC, e, &data);
  }
  CSG::merge(TC, &data);
  CSG::mergePoints(TC, &data);

  // print tree
  std::string save_txt_as = "union_tree.txt";
  std::string save_png_as = "union.png";
  writeToFile(TC, file_path, save_txt_as);
  // generate graph
  graph_vis({save_txt_as}, save_png_as);
  open_graph(save_png_as); 

  // write shape to file
  writeShapeToFile(TA, &data, file_path, "union_A");
  writeShapeToFile(TB, &data, file_path, "union_B");
  writeShapeToFile(TC, &data, file_path, "union_tree");

  destroy(TA);
  destroy(TB);
  destroy(TC);
  printf("testUnion: SUCCESS!\n");
  return 0;
}

int testDifference() {
  // Create data
  CSG::bsp2d_data data;
  float w = 2.0;
  float h = 2.0;
  float OA[3] = {0, 0.0, 0.0};
  quadForTesting(w, h, OA, &data);
  
  float OB[3] = {w/2, 0, 0.0};
  w /= 2;
  h /= 2;
  quadForTesting(w,h, OB, &data);
  
  // Make trees
  // Make trees
  CSG::bsp2d_node * TA = nullptr;
  for(size_t e = 0; e < 4; e++) {
    CSG::insert(TA, e, &data);
  }
  
  CSG::bsp2d_node * TB = nullptr;
  for(size_t e = 4; e < 8; e++) {
    CSG::insert(TB, e, &data);
  }

  // clip shape A by shape B
  std::vector<size_t> b_a;
  std::vector<size_t> eA;
  print(TA, eA);
  for(auto &e : eA) {
    clip(TB, e, &data, CSG::NONE, CSG::B_ADB, b_a);
  }
  
  // clip shape B by shape A
  std::vector<size_t> a_b;
  std::vector<size_t> eB;
  reverse(TB, &data);
  print(TB, eB);
  for(auto &e : eB) {
    clip(TA, e, &data, CSG::NONE, CSG::A_ADB, a_b);
  }
  //  reverse(TB, &data);
  
 // merge shape
  b_a.insert(b_a.end(), a_b.begin(), a_b.end()); 

  CSG::bsp2d_node * TC = nullptr;
  for(auto e : b_a) {
    CSG::insert(TC, e, &data);
  }
  CSG::merge(TC, &data);
  CSG::mergePoints(TC, &data);

  // print tree
  std::string save_txt_as = "difference_tree.txt";
  std::string save_png_as = "difference.png";
  writeToFile(TC, file_path, save_txt_as);
  // generate graph
  graph_vis({save_txt_as}, save_png_as);
  open_graph(save_png_as); 

  // write shape to file
  writeShapeToFile(TA, &data, file_path, "difference_A");
  writeShapeToFile(TB, &data, file_path, "difference_B");
  writeShapeToFile(TC, &data, file_path, "difference_tree");

  destroy(TA);
  destroy(TB);
  destroy(TC);
  printf("testDifference: SUCCESS!\n");
  return 0;
}

int testIntersection() {
  // Create data
  CSG::bsp2d_data data;
  float w = 2.0;
  float h = 2.0;
  float OA[3] = {0.0, 0.0, 0.0};
  quadForTesting(w, h, OA, &data);
  
  float OB[3] = {-w/2, 0, 0.0};
  w /= 2;
  h /= 2;
  quadForTesting(w, h, OB, &data);
  
  // Make trees
  // Make trees
  CSG::bsp2d_node * TA = nullptr;
  for(size_t e = 0; e < 4; e++) {
    CSG::insert(TA, e, &data);
  }
  
  CSG::bsp2d_node * TB = nullptr;
  for(size_t e = 4; e < 8; e++) {
    CSG::insert(TB, e, &data);
  }
  
  // clip shape B by shape A
  std::vector<size_t> a_b;
  std::vector<size_t> eB;

  print(TB, eB);
  for(auto &e : eB) {
    clip(TA, e, &data, CSG::NONE, CSG::A_AIB, a_b);
  }

  // clip shape A by shape B
  std::vector<size_t> b_a;
  std::vector<size_t> eA;
  print(TA, eA);
  for(auto &e : eA) {
    clip(TB, e, &data, CSG::NONE, CSG::B_AIB, b_a);
  }
 
 // merge shape
  b_a.insert(b_a.end(), a_b.begin(), a_b.end()); 

  CSG::bsp2d_node * TC = nullptr;
  for(auto e : b_a) {
    CSG::insert(TC, e, &data);
  }
  CSG::merge(TC, &data);
  CSG::mergePoints(TC, &data);

  // print treestd::string save_txt_as = "difference_tree.txt";
  std::string save_txt_as = "intersection_tree.txt";
  std::string save_png_as = "intersection.png";
  writeToFile(TC, file_path, save_txt_as);
  // generate graph
  graph_vis({save_txt_as}, save_png_as);
  open_graph(save_png_as); 

  // write shape to file
  writeShapeToFile(TA, &data, file_path, "intersection_A");
  writeShapeToFile(TB, &data, file_path, "intersection_B");
  writeShapeToFile(TC, &data, file_path, "intersection_tree");

  destroy(TA);
  destroy(TB);
  destroy(TC);
  printf("testDifference: SUCCESS!\n");
  return 0;
}
