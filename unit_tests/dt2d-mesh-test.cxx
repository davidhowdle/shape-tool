#include <iostream>
#include <sstream>
#include <chrono>
#include <stdlib.h>
#include <time.h>
#include "ConvexDelaunay2D.h"
#include "ConstrainedDelaunay2D.h"

const std::string file_path = "/Users/davidson/shapes/shape-tool/unit_tests/mesh_files/";

int testingConvexDelaunay2D();
int testingCDT2d();
int testingCDTWithPLC();
int testingCDTWithHole();


void getSquare(std::vector<float> & verts, size_t &nv) {
  verts.clear();
  float v[12] = {
    0.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 1.0, 0.0,
    0.0, 1.0, 0.0
  };
  
  nv = 4;
  for (size_t i = 0; i < nv*3; i++) verts.push_back(v[i]);
}

void getConvexShape(std::vector<float> & verts, size_t &nv) {
  verts.clear();
  float v[18] = {
    1.0, 7.5, 0.0,
    -1.0, 7.0, 0.0,
    0.0, 0.0, 0.0,
    3.0, 1.0, 0.0,
    4.8, 3.0, 0.0,
    5.0, 6.0, 0.0
  };
  
  nv = 6;
  for (size_t i = 0; i < nv*3; i++) verts.push_back(v[i]);
}

void getCircle(std::vector<float> & verts, size_t &nv,
              size_t N = 50, float R = 1.0, float x0 = 0.0, float y0 = 0.0) {
  verts.clear();
  nv = N;
  float dth = 2.0*M_PI/nv;
  for(size_t i = 0; i < nv; i++) {
    verts.push_back(x0 + R*cos(i*dth));
    verts.push_back(y0 + R*sin(i*dth));
    verts.push_back(0.0);
  }
}

void get2Circles(std::vector<float> & verts, size_t &nv) {
  verts.clear();
  nv = 40;
  float dth = 2.0*M_PI/nv;
  for(size_t i = 0; i < nv; i++) {
    verts.push_back(cos(i*dth));
    verts.push_back(sin(i*dth));
    verts.push_back(0.0);
  }
  
  for(size_t i = 0; i < nv; i++) {
    verts.push_back(2+cos(i*dth));
    verts.push_back(2+sin(i*dth));
    verts.push_back(0.0);
  }

  nv = 80;
}

void getConstrainedShape(std::vector<float> & verts, size_t &nv) {
  verts.clear();
  float v[33] = {
    15.0, 0.0, 0.0,
    11.0, 5.0, 0.0,
    12.0, 1.0, 0.0,
    10.0, 1.0, 0.0,
    11.0, 2.5, 0.0,
    6.0,  2.0, 0.0,
    5.5,  1.0, 0.0,
    6.0,  2.0, 0.0,
    3.5,  2.0, 0.0,
    4.0,  3.0, 0.0,
    0.0,  0.0, 0.0 };
  
  nv = 11;
  for (size_t i = 0; i < nv*3; i++) verts.push_back(v[i]);
}

void getE(std::vector<float> & verts, size_t &nv) {
  verts.clear();
  float v[36] = {
    0.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.1, 0.0,
    0.1, 0.1, 0.0,
    0.1, 0.45, 0.0,
    0.6, 0.45, 0.0,
    0.6, 0.55, 0.0,
    0.1, 0.55, 0.0,
    0.1,  0.9, 0.0,
    1.0,  0.9, 0.0,
    1.0,  1.0, 0.0,
    0.0,  1.0, 0.0,
  };
  
  nv = 12;
  for (size_t i = 0; i < nv*3; i++) verts.push_back(v[i]);
}

int main(int argc, char ** argv) {
  
  std::cout << "Begin 2-D delaunay triangulation tests..." << std::endl;
  std::cout << "Testing convex delaunay 2d..." << std::endl;
  if (testingConvexDelaunay2D() ) {
    std::cout << "Convex dealunay 2d failed!" << std::endl;
  }
  
  std::cout << "Testing CDT 2d..." << std::endl;
  if (testingCDT2d() ) {
    std::cout << "CDT 2d failed!" << std::endl;
  }
  
  std::cout << "Testing CDT 2d with plc insertion..." << std::endl;
  if (testingCDTWithPLC() ) {
    std::cout << "CDT 2d with plc insertion failed!" << std::endl;
  }
  
  std::cout << "Testing CDT 2d with hole insertion..." << std::endl;
  if (testingCDTWithHole() ) {
    std::cout << "CDT 2d with hole insertion failed!" << std::endl;
  }

  std::cout << "SUCCESS, All tests passed!" << std::endl;
  return 0;
}

//////////////////////////////////////////////////////////////////
// FOR CHEW's ALGORITHM
//////////////////////////////////////////////////////////////////

int testingConvexDelaunay2D() {
  
  // get oriented boundary to mesh
  std::vector<float> coordinates;
  size_t nc;
  getCircle(coordinates, nc);
  
  // begin timer 
  ConvexDelaunay2D mesh(&coordinates[0], nc);
  auto t1 = std::chrono::high_resolution_clock::now();
  mesh.setMesh(); 
  // finish timing 
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout << "ConvexDt contruction time: " << duration.count() << "s\n";
  
  // print to file
  std::string file_name = "ConvexDT";
  mesh.printToFile(file_path, file_name); 
  return 0;
}

//////////////////////////////////////////////////////////////////
// FOR CHEW's CDT ALGORITHM
//////////////////////////////////////////////////////////////////

int testingCDT2d() {
  // get oriented boundary to mesh
  std::vector<float> coordinates;
  size_t nc;
  getSquare(coordinates, nc);
//  getCircle(coordinates, nc);
  ConstrainedDelaunay2D mesh(&coordinates[0], nc);
  // begin timer 
  auto t1 = std::chrono::high_resolution_clock::now();
  mesh.setMesh();
  // finish timing 
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout << "CDT contruction time: " << duration.count() << "s\n";

  // print to file
  std::string file_name = "CDT";
  mesh.printToFile(file_path, file_name);
  return 0;
}

int testingCDTWithPLC() {
  // get oriented boundary to mesh
  std::vector<float> coordinates;
  size_t nc;
  getSquare(coordinates, nc);
  //  getCircle(coordinates, nc);
  ConstrainedDelaunay2D mesh(&coordinates[0], nc);
  // begin timer
  auto t1 = std::chrono::high_resolution_clock::now();
  mesh.setMesh();
  // finish timing
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout << "CDT contruction time: " << duration.count() << "s\n";

  // get a circle to insert
  coordinates.clear();
  getCircle(coordinates, nc, 6, 0.25, 0.5, 0.5);
  t1 = std::chrono::high_resolution_clock::now();
  mesh.insertPLC(&coordinates[0], nc);
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "CDT insert PLC time: " << duration.count() << "s\n";
  
  // print to file
  std::string file_name = "CDT_with_PLC";
  mesh.printToFile(file_path, file_name);
  return 0;
}

int testingCDTWithHole() {
  // get oriented boundary to mesh
  std::vector<float> coordinates;
  size_t nc;
//  getSquare(coordinates, nc);
  getCircle(coordinates, nc, 10, 2);
  ConstrainedDelaunay2D mesh(&coordinates[0], nc);
  // begin timer
  auto t1 = std::chrono::high_resolution_clock::now();
  mesh.setMesh();
  // finish timing
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout << "CDT contruction time: " << duration.count() << "s\n";

  // get a circle to insert
  coordinates.clear();
  getE(coordinates, nc);
  t1 = std::chrono::high_resolution_clock::now();
  mesh.insertPLC(&coordinates[0], nc);
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "CDT insert hole time: " << duration.count() << "s\n";

  
  // print to file
  std::string file_name = "CDT_with_hole";
  mesh.printToFile(file_path, file_name);
  return 0;
}
