import sys
import pydot

argc = len(sys.argv)
if argc == 1:
  print "\n\nProgram requires at least one input file name. Use -h or --help for usage.\n\n" 
  sys.exit()

if argc == 2 and sys.argv[1] == "-h" or sys.argv[1] == "--help":
  print "\n\npython bsp_tree_plot -f 'filepath1/filename1' ... 'filepathN/filenameN -o outName'\n\n"
  sys.exit()
elif argc == 2:
  print "\n\nUnknown flag or insufficent number of arguments. Use -h or --help for usage.\n\n"
  sys.exit()

if sys.argv[1] != "-f":
  print "\n\nUnknown flag or insufficent number of arguments. Use -h or --help for usage.\n\n"
  sys.exit()

## extract files and output name
save_as ="bsp_graph.png"
files = ()
k = 2;
for arg in sys.argv[2:]:
  if arg != "-o":
    files += (arg,)
  elif arg == "-o":
    save_as = sys.argv[k+1]
    break
  k += 1

no_files = len(files);

##print "Visualizing  %d files..." % no_files      
graph = pydot.Dot(graph_type='graph')
colors = ("red", "green", "blue")
for fl in files:
  f = open(fl,'r')
  for line in f:
    line = line.strip('\n')
    verts = line.split("\t")
    ## get adjacenies
    node = verts[0]
    c = 0
    for child in verts[1:3]:
      if child != "X":
        edge = pydot.Edge(node,child,color = colors[c])
        graph.add_edge(edge);
      c += 1
    if len(verts) > 3:
      edge = pydot.Edge(node,', '.join(verts[3:]), color=colors[c])
      graph.add_edge(edge);
    

#print "Saving file %s..." % save_as      
graph.write_png(save_as)
#print "Exiting bsp_tree_plot."
