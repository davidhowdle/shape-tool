#include <iostream>
#include <sstream>
#include "CRSMatrix.h"
#include "Algorithms.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <math.h>
#include <chrono>
#include <stdlib.h>
#include <time.h>

void testSorting(size_t n);

int main(int argc, char** argv)
{
  size_t N = 4;
  if(argc > 1) N = std::stoul(argv[1]);

  std::ostringstream oss;
  oss << "Benchmarking sorting algorithms for a random data set of size:" << N;
  oss << std::endl;
  std::cout << oss.str();

  testSorting(N);
  return 0;
}


bool compare(const SparseLA::triplet &a, const SparseLA::triplet &b) { return a < b; }

void testSorting(size_t n) {
  
  std::vector<SparseLA::triplet> data;
  SparseLA::triplet tmp{0,0,0};
  for(size_t i = 0; i < n; i++) {
    tmp.i = rand()%n;
    tmp.j = rand()%n;
    tmp.v = rand()%n;
    data.push_back(tmp);
  }
  std::vector<SparseLA::triplet> data2(data);
  std::vector<SparseLA::triplet> data3(data);
  std::vector<SparseLA::triplet> data4(data);
  std::vector<SparseLA::triplet> data5(data);
  
  // std::sort
  auto t1 = std::chrono::high_resolution_clock::now();
  std::sort(data.begin(), data.end(),compare);
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout<< "\nstd::sort time: " << duration.count() << "s\n";
  
  if(n < 50) {
    std::cout << "data: " << std::endl;
    for(auto &x : data) std::cout << x << " ";
    std::cout << std::endl;
  }
  
  // std::stable_sort
  t1 = std::chrono::high_resolution_clock::now();
  std::stable_sort(data2.begin(), data2.end());
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout<< "std::stable_sort time: " << duration.count() << "s\n";
  
  if(n < 50) {
    std::cout << "data: " << std::endl;
    for(auto &x : data2) std::cout << x << " ";
    std::cout << std::endl;
  }

  // Brad's merge sort
  t1 = std::chrono::high_resolution_clock::now();
  SparseLA::Algorithms::MergeSort::sort<SparseLA::triplet>(&data3[0], n);
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout<< "My merge sort time: " << duration.count() << "s\n";
  
  if(n < 50) {
    std::cout << "data: " << std::endl;
    for(auto &x : data3) std::cout << x << " ";
    std::cout << std::endl;
  }
  
  // Brad's heap sort
  t1 = std::chrono::high_resolution_clock::now();
  SparseLA::Algorithms::HeapSort::sort<SparseLA::triplet>(&data4[0], n);
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout<< "My heap sort time: " << duration.count() << "s\n";
  
  if(n < 50) {
    std::cout << "data: " << std::endl;
    for(auto &x : data4) std::cout << x << " ";
    std::cout << std::endl;
  }

  // sort with carry
  std::vector<size_t>order(data5.size(),0);
  for(size_t i = 0; i < data5.size(); i++) order[i] = i;
  if(n < 50) {
    std::cout << "unsorted: " << std::endl;
    for(auto &x : data5) std::cout << x << " ";
    std::cout << std::endl;
  }
  t1 = std::chrono::high_resolution_clock::now();
  SparseLA::Algorithms::MergeSort::sort<SparseLA::triplet, size_t>(&data5[0],&order[0],n);
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout<< "My merge sort with carry time: " << duration.count() << "s\n";
  if(n < 50) {
    std::cout << "sorted: " << std::endl;
    for(auto &x : data5) std::cout << x << " ";
    std::cout << std::endl;
    std::cout << "sorted order: " << std::endl;
    for(auto &x : order) std::cout << x << " ";
    std::cout << std::endl;
  }
}

