#include <Eigen/Sparse>
#include "CRSMatrix.h"
#include "Algorithms.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <math.h>
#include <chrono>
#include <stdlib.h>
#include <time.h>

typedef Eigen::SparseMatrix<int> cssMat; // declares a column-major sparse matrix type of double
typedef Eigen::SparseMatrix<int, Eigen::RowMajor> crsMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<int> T;


void testHeadToHead(size_t n, size_t fact);

std::vector<T> randomSparseMatrix(size_t n, size_t fact) {
  size_t m = n*n/fact; // fill some fraction of the matrix
  std::vector<T> x;
  for(size_t i = 0; i < m; i++) {
    auto tmp = T(rand()%n, rand()%n, rand()%10-5);
    x.push_back(tmp);
  }
  return x;
}

int main(int argc, char** argv)
{
  size_t N = 4;
  size_t fact = 2;
  if(argc > 1) N = std::stoul(argv[1]);
  if(argc > 2) fact = std::stoul(argv[2]);

  std::ostringstream oss;
  oss << "Testing sparce matrix operations for a " << N << "x" << N;
  oss << " randomly generated matrix with a fill factor of " << fact;
  oss << std::endl;
  std::cout << oss.str();
  testHeadToHead(N, fact);
  return 0;
}

void testHeadToHead(size_t n, size_t fact) {
  auto coefficients = randomSparseMatrix(n,fact);
  std::vector<SparseLA::triplet> data;
//  data = {{0,0,13},{1,1,2}, {1,1,3}, {1,1,5}, {3,3,4}, {2,3,4}, {2,3,4}, {2,3,4}};
  SparseLA::triplet tmp{0,0,0};
  for(auto &x : coefficients) {
    tmp.i = x.row();
    tmp.j = x.col();
    tmp.v = x.value();
    data.push_back(tmp);
  }
  // testing 3 different matrices
  cssMat css(n,n);
  crsMat crs(n,n);
  SparseLA::CRSMatrix my_crs(n,n);

  auto t1 = std::chrono::high_resolution_clock::now();
  css.setFromTriplets(coefficients.begin(), coefficients.end());
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout << "\n\nCSS contruction time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  crs.setFromTriplets(coefficients.begin(), coefficients.end());
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "CRS contruction time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  my_crs.setMatrix(data);
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "My CRS contruction time: " << duration.count() << "s\n";


 // transposes 
  t1 = std::chrono::high_resolution_clock::now();
  cssMat cssT = css.transpose();
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "\n\nCSS transpose time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  crsMat crsT = crs.transpose();
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "CRS transpose time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  SparseLA::CRSMatrix my_crsT =  my_crs.transpose();
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "My CRS transpose time: " << duration.count() << "s\n";
  
  // Matrix Matrix multiply
  t1 = std::chrono::high_resolution_clock::now();
  cssMat cssAAT = css * cssT;
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "\n\nCSS * CSST time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  crsMat crsAAT = crs * crsT;
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "CRS * CRST time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  SparseLA::CRSMatrix my_crsAAT =  my_crs * my_crsT;
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "My CRS * My CRST time: " << duration.count() << "s\n";

  // Matrix vector multiply
  Eigen::VectorXi b = Eigen::VectorXi::Ones(n);
  t1 = std::chrono::high_resolution_clock::now();
  Eigen::VectorXi cssC = css * b;
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "\n\nCSS * b time: " << duration.count() << "s\n";
  
  t1 = std::chrono::high_resolution_clock::now();
  Eigen::VectorXi crsC = crs * b;
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "CRS * b time: " << duration.count() << "s\n";

  Eigen::VectorXi c = Eigen::VectorXi::Zero(n);
  t1 = std::chrono::high_resolution_clock::now();
  my_crs.product(b.data(), c.data());
  t2 = std::chrono::high_resolution_clock::now();
  duration = t2 - t1;
  std::cout << "My CRS * b time: " << duration.count() << "s\n";

  // are solutions correct?
  if(cssC != crsC) {
    std::cout << "A*b failure: crs not equal css!!" << std::endl;
  }else {
    std::cout << "sum of css_c = " << cssC.sum() << std::endl;
    std::cout << "sum of crs_c = " << crsC.sum() << std::endl;
  }
  
  if(c != crsC) {
    std::cout << "A*b failure: my_crs not equal css or crs!!" << std::endl;
  } else {
    std::cout << "sum of my cc = " << c.sum() << std::endl;
  }
  
  // print the matrices
  if (n <= 10) {
    std::cout << "\n\nCSS Matrix:\n" << css << std::endl;
    std::cout << "\nCRS Matrix:\n" << crs << std::endl;
    std::cout << "\nMy CRS Matrix:\n" << my_crs << std::endl;
    
    std::cout << "\n\nCSST Matrix:\n" << cssT << std::endl;
    std::cout << "\nCRST Matrix:\n" << crsT << std::endl;
    std::cout << "\nMy CRS T Matrix:\n" << my_crsT << std::endl;
    
    std::cout << "\n\nCSSAAT Matrix:\n" << crsAAT << std::endl;
    std::cout << "\nCRSAAT Matrix:\n" << crsAAT << std::endl;
    std::cout << "\nMy CRSAAT Matrix:\n" << my_crsAAT << std::endl;
  }

}
