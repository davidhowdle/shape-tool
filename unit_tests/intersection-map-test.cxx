// This is a template for an mpi main.cpp file

#include <iostream>
#include <vector>
#include <stdio.h>
#include <random>
#include <chrono>
#include <stdlib.h>
#include <time.h>
//#include "BijectiveMap.h"
#include "IntersectionMap.h"

int mapTest();
void timingTest(int argc, char **argv);
void test1(size_t, size_t, int);
void test2(size_t, size_t, int);

int main(int argc, char ** argv)
{
  // check inclusions
  if (mapTest()) return 1;
  // run timing test
  timingTest(argc, argv);
  return 0;
}

int mapTest() {

  printf("\n\nBegin testing bijective map functionality...\n");

  IntersectionPair p0(0,1);
  IntersectionPair p1(1,2);
  IntersectionPair p2(5,13);
  IntersectionPair p3(1,1000);

  std::vector<IntersectionPair> pairs{p0, p1, p2, p3};
  std::vector<size_t> values{0, 1, 2, 3};
  IntersectionMap m;

  size_t vi = 0;
  for( auto p : pairs ) {
    if (m.hasValue(p)) {
      printf("FAILURE: map should not already contain pair = {%lu, %lu}\n", p.first, p.second);
      return 1;
    }else {
      m.insert(p,values[vi]); 
    }
    vi++;
  }

  //swap
  for(auto &p : pairs) p.swap();

  for( auto p : pairs ) {
    if (!m.hasValue(p)) {
      printf("FAILURE: map should not already contain pair = {%lu, %lu}\n", p.first, p.second);
      p.swap();
      printf("Because it contains pair = {%lu, %lu} => %lu\n", p.first, p.second, m.getValue(p));
      return 1;
    }
  }

  // add some new pairs
  IntersectionPair p4(1,1);
  IntersectionPair p5(2,2);
  IntersectionPair p6(6,13);
  IntersectionPair p7(7,1000);

  std::vector<IntersectionPair> new_pairs{p4, p5, p6, p7};
  
  vi = 0;
  for( auto p : new_pairs ) {
    if (m.hasValue(p)) {
      printf("FAILURE: map should not already contain pair = {%lu, %lu}\n", p.first, p.second);
      return 1;
    }else {
      m.insert(p, values[vi]); 
    }

    vi++;
  }

  m.insert(1,13,13);
  if (m.getValue(1,13) != 13) { printf("FAILURE: did not get expected value!\n"); return 1;}
  if (!m.hasValue(1,13)) { printf("FAILURE: map has pairing!!\n"); return 1;}
  if (!m.hasValue(13,1)) { printf("FAILURE: map has inverse pairing!!\n"); return 1;}
  if (m.getValue(13,1) != 13) { printf("FAILURE: did not get expected value for inverse!\n"); return 1;}

  printf("SUCCESS!\n\n");
  return 0;

}

void timingTest(int argc, char ** argv) {

  size_t N = 1000;
  size_t upper_bound;
  int R = 10;
  if (argc > 1) N = std::stoul(argv[1]);
  if (argc > 2) R = std::stoi(argv[2]);
  if (argc > 3) upper_bound = std::stoi(argv[3]);
  else upper_bound = N/20;

  std::cout << "Performing bijective-map test for " << N;
  std::cout << " random points, with " << R << " repetions,";
  std::cout << " and an upper bound of " << upper_bound <<"." << std::endl;
  
  test1(N,upper_bound,R);
  test2(N,upper_bound,R);
}

void test1(size_t N = 1000000, size_t upper_bound = 50, int R = 10) {
  // test on a map
  srand(time(NULL));
  std::map<size_t, size_t> m;

  size_t v;
  size_t idx;
  std::chrono::duration<double> total_time = std::chrono::duration<double>::zero();

  for(int r = 0; r < R ; r++) {
    auto t1 = std::chrono::high_resolution_clock::now();
    for(size_t i = 0; i < N; i++) {
      idx = rand() % upper_bound;
      if(m.find(idx) != m.end()) v = m[idx];
      else m.insert(std::make_pair(idx, rand() % upper_bound));
      }
    auto t2 = std::chrono::high_resolution_clock::now();
    total_time += std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
  }
  std::cout<< "(STANDARD MAP) Average time per test: " << total_time.count()/R << "s\n";
}

void test2(size_t N = 1000000, size_t upper_bound = 50, int R = 10) {
  // test on extended map
  srand(time(NULL));
  IntersectionMap m;

  size_t v;
  size_t a,b;
  std::chrono::duration<double> total_time = std::chrono::duration<double>::zero();

  for(int r = 0; r < R ; r++) {
    auto t1 = std::chrono::high_resolution_clock::now();
    for(size_t i = 0; i < N; i++) {
      a = rand() % upper_bound;
      b = rand() % upper_bound;
      if(m.hasValue(a, b)) v = m.getValue(a, b);
      else m.insert(a, b, rand() % upper_bound);
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    total_time += std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
  }
  std::cout<< "(BIJECTIVE MAP) Average time per test: " << total_time.count()/R << "s\n";
}
