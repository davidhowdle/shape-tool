#include <iostream>
#include "CSG_Utilities.h"
#include <chrono>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int testEdgeSort();
int testEndToEnd(size_t n);
int testSpacedEndToEnd(size_t n);
int testOverlapping(size_t n);
int testContains(size_t n);

int main(int argc, char ** argv) {
  std::cout << "Begin testing edge sorting..." << std::endl;
  std::cout << "Running: testEdgeSort()..." << std::endl;
  if (testEdgeSort()) {
    std::cout << "testEdgeSort() FAILED." << std::endl;
    return 1;
  }
  
  size_t N = 4;
  if (argc > 1) N = std::stoul(argv[1]);
  
  std::cout << "Running: testEndToEnd()..." << std::endl;
  if (testEndToEnd(N)) {
    std::cout << "testEndtoEnd() FAILED." << std::endl;
    return 1;
  }
  
  std::cout << "Running: testSpacedEndToEnd()..." << std::endl;
  if (testSpacedEndToEnd(N)) {
    std::cout << "testSpacedEndToEnd() FAILED." << std::endl;
    return 1;
  }
  
  std::cout << "Running: testOverlapping()..." << std::endl;
  if (testOverlapping(N)) {
    std::cout << "testOverlapping() FAILED." << std::endl;
    return 1;
  }
  
  std::cout << "Running: testContains()..." << std::endl;
  if (testContains(N)) {
    std::cout << "testContains() FAILED." << std::endl;
    return 1;
  }
  
  std::cout << "SUCCESS!" << std::endl;
  return 0;
}

int testEndToEnd(size_t n) {
  // make a random direction
  float x1  = static_cast<float>((rand()%100-50)/100.0);
  float x2  = static_cast<float>((rand()%100-50)/100.0);
  float x3  = static_cast<float>((rand()%100-50)/100.0);
  float L = sqrtf(powf(x1, 2.0) + powf(x2, 2.0) + powf(x3, 2.0));
  if (L < CSG::epsilon) {
    std::cout << "Random vector length == 0!!!" << std::endl;
    return 1;
  }
  
  float tau[3] = {x1/L, x2/L, x3/L};
  
  std::vector<CSG::point> verts;
  verts.push_back({{static_cast<float>((rand()%100-50)/100.0),
                    static_cast<float>((rand()%100-50)/100.0),
                    static_cast<float>((rand()%100-50)/100.0)}});
  
  float s;
  for(size_t i = 1; i <= n; i++) {
    s = static_cast<float>((rand()%100)/100.0);
    auto pi = verts[i-1];
    verts.push_back({{pi.x() + s * tau[0],
                      pi.y() + s * tau[1],
                      pi.z() + s * tau[2]}});
  }
  
  std::vector<CSG::edge> edge_map;
  std::vector<size_t> edges;
  for(size_t i = 1; i <= n; i++) {
    edge_map.push_back(CSG::edge(i-1, i));
    edges.push_back(i-1);
  }

  CSG::plane N0;  
  std::vector<CSG::plane> normals(2*n-1,N0);
  
  std::vector<size_t> result;
  CSG::mergeColinear(edges, verts, normals, edge_map, result);

  if(result.size() != 1) {
    std::cout << "FAILURE: merge unsuccessful!!" << std::endl;
    return 1;
  } else {
    std::vector<CSG::edge> solution{CSG::edge(0,n)};
    for(size_t i = 0; i < result.size(); i++) {
      if (edge_map[result[i]] != solution[i]) {
        std::cout << "FAILURE: merge produced incorrect output!" << std::endl;
        std::cout << edge_map[result[i]] << " != " << solution[i] << std::endl;
        return 1;
      }
    }
  }
  
  return 0;
}

int testSpacedEndToEnd(size_t n) {
  // make a random direction
  float x1  = static_cast<float>((rand()%100-50)/100.0);
  float x2  = static_cast<float>((rand()%100-50)/100.0);
  float x3  = static_cast<float>((rand()%100-50)/100.0);
  float L = sqrtf(powf(x1, 2.0) + powf(x2, 2.0) + powf(x3, 2.0));
  if (L < CSG::epsilon) {
    std::cout << "Random vector length == 0!!!" << std::endl;
    return 1;
  }
  
  float tau[3] = {x1/L, x2/L, x3/L};
  
  std::vector<CSG::point> verts;
  verts.push_back({{static_cast<float>((rand()%100-50)/100.0),
    static_cast<float>((rand()%100-50)/100.0),
    static_cast<float>((rand()%100-50)/100.0)}});
  
  float s;
  for(size_t i = 1; i < 2*n; i++) {
    s = static_cast<float>((rand()%100 + 50)/100.0);
    auto pi = verts[i-1];
    verts.push_back({{pi.x() + s * tau[0],
                      pi.y() + s * tau[1],
                      pi.z() + s * tau[2]}});
  }
  
  std::vector<CSG::edge> edge_map;
  std::vector<size_t> edges;
  size_t e = 0;
  for(size_t i = 0; i < n; i++) {
    edge_map.push_back(CSG::edge(2*i, 2*i+1));
    edges.push_back(e++);
  }
  
  CSG::plane N0;  
  std::vector<CSG::plane> normals(2*n-1,N0);
  
  // merge
  std::vector<size_t> result;
  CSG::mergeColinear(edges, verts, normals, edge_map, result);

  if(result.size() != n) {
    std::cout << "FAILURE: merge unsuccessful!!" << std::endl;
    return 1;
  } else {
    for(size_t i = 0; i < result.size(); i++) {
      if (edge_map[result[i]] != edge_map[i]) {
        std::cout << "FAILURE: merge produced incorrect output!" << std::endl;
        std::cout << edge_map[result[i]] << " != " << edge_map[i] << std::endl;
        return 1;
      }
    }
  }
  
  return 0;
}

int testOverlapping(size_t n) {
  // make a random direction
  float x1  = static_cast<float>((rand()%100-50)/100.0);
  float x2  = static_cast<float>((rand()%100-50)/100.0);
  float x3  = static_cast<float>((rand()%100-50)/100.0);
  float L = sqrtf(powf(x1, 2.0) + powf(x2, 2.0) + powf(x3, 2.0));
  if (L < CSG::epsilon) {
    std::cout << "Random vector length == 0!!!" << std::endl;
    return 1;
  }
  
  float tau[3] = {x1/L, x2/L, x3/L};

  std::vector<CSG::point> verts;
  verts.push_back({{static_cast<float>((rand()%100-50)/100.0),
    static_cast<float>((rand()%100-50)/100.0),
    static_cast<float>((rand()%100-50)/100.0)}});
  
  float s;
  for(size_t i = 1; i < n+2; i++) {
    s = static_cast<float>((rand()%100)/100.0);
    auto pi = verts[i-1];
    verts.push_back({{pi.x() + s * tau[0],
                      pi.y() + s * tau[1],
                      pi.z() + s * tau[2]}});
    
  }
  
  std::vector<CSG::edge> edge_map;
  std::vector<size_t> edges;
  size_t e = 0;
  for(size_t i = 0; i < n; i++) {
    edge_map.push_back(CSG::edge(i, i+2));
    edges.push_back(e++);
  }
  
  CSG::plane N0;  
  std::vector<CSG::plane> normals(2*n-1,N0);
  
  // merge
  std::vector<size_t> result;
  CSG::mergeColinear(edges, verts, normals, edge_map, result);

  if(result.size() != 1) {
    std::cout << "FAILURE: merge unsuccessful!!" << std::endl;
    return 1;
  } else {
    std::vector<CSG::edge> solution{CSG::edge(0,n+1)};
    for(size_t i = 0; i < result.size(); i++) {
      if (edge_map[result[i]] != solution[i]) {
        std::cout << "FAILURE: merge produced incorrect output!" << std::endl;
        std::cout << edge_map[result[i]] << " != " << solution[i] << std::endl;
        return 1;
      }
    }
  }
  
  return 0;
}

int testContains(size_t n) {
  // make a random direction
  float x1  = static_cast<float>((rand()%100-50)/100.0);
  float x2  = static_cast<float>((rand()%100-50)/100.0);
  float x3  = static_cast<float>((rand()%100-50)/100.0);
  float L = sqrtf(powf(x1, 2.0) + powf(x2, 2.0) + powf(x3, 2.0));
  if (L < CSG::epsilon) {
    std::cout << "Random vector length == 0!!!" << std::endl;
    return 1;
  }
  
  float tau[3] = {x1/L, x2/L, x3/L};

  std::vector<CSG::point> verts;
  verts.push_back({{static_cast<float>((rand()%100-50)/100.0),
    static_cast<float>((rand()%100-50)/100.0),
    static_cast<float>((rand()%100-50)/100.0)}});
  
  float s = 1.1;
  verts.push_back({{verts[0].x() + s * tau[0],
    verts[0].y() + s * tau[1],
    verts[0].z() + s * tau[2]}});
  
  float t;
  for(size_t i = 2; i < 2*n; i+=2) {
    s = static_cast<float>((rand()%50 + 10)/100.0);
    t = static_cast<float>((rand()%90 + 20)/100.0);
    if (s > t) std::swap(s,t);
    
    verts.push_back({{verts[0].x() + s * tau[0],
      verts[0].y() + s * tau[1],
      verts[0].z() + s * tau[2]}});
    
    verts.push_back({{verts[0].x() + t * tau[0],
      verts[0].y() + t * tau[1],
      verts[0].z() + t * tau[2]}});
  }
  
  std::vector<CSG::edge> edge_map;
  std::vector<size_t> edges;
  size_t e = 0;
  for(size_t i = 0; i < n; i++) {
    edge_map.push_back(CSG::edge(2*i, 2*i+1));
    edges.push_back(e++);
  }

  CSG::plane N0;  
  std::vector<CSG::plane> normals(2*n-1,N0);
  
  // merge
  std::vector<size_t> result;
  CSG::mergeColinear(edges, verts, normals, edge_map, result);

  if(result.size() != 1) {
    std::cout << "FAILURE: merge unsuccessful!!" << std::endl;
    return 1;
  } else {
    std::vector<CSG::edge> solution{CSG::edge(0,1)};
    for(size_t i = 0; i < result.size(); i++) {
      if (edge_map[result[i]] != solution[i]) {
        std::cout << "FAILURE: merge produced incorrect output!" << std::endl;
        std::cout << edge_map[result[i]] << " != " << solution[i] << std::endl;
        return 1;
      }
    }
  }
  
  return 0;
}

int testEdgeSort() {

  CSG::point p0{{0.0,       0.0,      0.0}};
  CSG::point p1{{1.0,       0.0,      0.0}};
  CSG::point p2{{0.5,       0.0,      0.0}};
  CSG::point p3{{1.5,       0.0,      0.0}};
  CSG::point p4{{2.5,       0.0,      0.0}};
  CSG::point p5{{2.75,      0.0,      0.0}};
  CSG::point p6{{2.75,      0.0,      0.0}};
  CSG::point p7{{3.0,       0.0,      0.0}};
  CSG::point p8{{2.8,       0.0,      0.0}};
  CSG::point p9{{2.9,       0.0,      0.0}};

// edges
  CSG::edge e0{0,1};
  CSG::edge e1{2,3};
  CSG::edge e2{4,5};
  CSG::edge e3{6,7};
  CSG::edge e4{8,9};
  
  std::vector<CSG::point> verts{p0, p1, p2, p3, p4, p5, p6, p7, p8, p9};

  std::vector<CSG::edge> edge_map{e0, e1, e2, e3, e4};
  size_t edge_count = edge_map.size();

  std::vector<size_t> edges(edge_count, 0);
  std::vector<float> distance;
  for(size_t i = 0; i < edge_count; i++) edges[i] = i;
  
  CSG::plane N0;  
  std::vector<CSG::plane> normals(10,N0);

  // try sorting the edges
  std::vector<size_t> sorted = CSG::edgeSort(edges, verts, edge_map, distance);

  // compare agains solutions
  std::vector<size_t> sordid_truth{0,1,0,1,2,2,3,4,4,3};
  if(sorted != sordid_truth) {
    std::cout << "FAILURE: sort unsuccessful!!" << std::endl;
    return 1;
  }
  
  std::vector<CSG::edge> merged = merge(sorted, distance, edges, edge_map);
  if(merged.size() != 2) {
    std::cout << "FAILURE: merge unsuccessful!!" << std::endl;
    return 1;
  } else {
    std::vector<CSG::edge> solution{CSG::edge(0,3), CSG::edge(4,7)};
    for(size_t i = 0; i < merged.size(); i++) {
      if (merged[i] != solution[i]) {
        std::cout << "FAILURE: merge produced incorrect output!" << std::endl;
        std::cout << merged[i] << " != " << solution[i] << std::endl;
        return 1;
      }
    }
  }

  return 0;
}
