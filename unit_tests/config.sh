#!/bin/bash

rm -rf CMakeFiles *Cache.txt *.cmake

cmake \
    -DCMAKE_CXX_COMPILER=clang++ \
    -DCMAKE_C_COMPILER=clang \
    -DCMAKE_CXX_FLAGS:STRING='-Wall -fPIC -pipe -std=c++11 -stdlib=libc++' \
    -DCMAKE_C_FLAGS:STRING='-fPIC -pipe -Wall' \

make -j8
