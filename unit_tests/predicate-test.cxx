#include <iostream>
#include "CSG_Utilities.h"

int distanceTest();
int edgePlanePredicateTest();
int edgeIntersectionTest();

int main(int argc, char ** argv) {

  std::cout << "\n\nTesting point-plane distances..." << std::endl;  
  if ( distanceTest() )         return 1;
  
  std::cout << "Testing edge-plane predicates..." << std::endl;  
  if (edgePlanePredicateTest() )  return 1;
  
  std::cout << "Testing edge-edge intersections..." << std::endl;  
  if (edgeIntersectionTest() )  return 1;

  std::cout << "ALL TESTS PASS!\n\n" << std::endl;  
  return 0;
}

int distanceTest() {

  // make some test points
  CSG::point a{{0.0,      0.0,      0.0}};
  CSG::point b{{0,      1.0,    0}};
  CSG::point c{{0,      -1.0,   0}};
  CSG::point d{{-1e-8,  1e-8,   0}};
  CSG::point e{{1e8,    -1e-8,  0}};

  //vert list
  std::vector< CSG::point> verts{a, b, c, d, e};

  // make a test plane
  CSG::plane p{{0, 1.0, 0.0}, 0.0};

  // begin tests
  if (distanceFromPlane(1,p,verts) <= 0.0) {
    std::cout << "Test 1. FAILURE! Point is in front of the test plane." << std::endl;
    return 1;
  }

  if (distanceFromPlane(2,p,verts) >= 0.0) {
    std::cout << "Test 2. FAILURE! Point is behind the test plane." << std::endl;
    return 1;
  }

  if (distanceFromPlane(3,p,verts) <= 0.0) {
    std::cout << "Test 3. FAILURE! Point is in front of the test plane." << std::endl;
    return 1;
  }
  
  if (distanceFromPlane(4,p,verts) >= 0.0) {
    std::cout << "Test 4. FAILURE! Point is behind the test plane." << std::endl;
    return 1;
  }

  if (!coincidentWithPlane(0,p,verts)) {
    std::cout << "Test 5. Distance from plane: " << distanceFromPlane(4,p,verts) << std::endl;
    std::cout << "Test 5. float epsilon: " << CSG::epsilon << std::endl;
    std::cout << "Test 5. FAILURE! Point is in the test plane." << std::endl;
    return 1;
  }

  std::cout << "SUCCESS!\n" << std::endl;  
  return 0;
}

int edgePlanePredicateTest() {

  CSG::point p0{{0.0,      0.0,      0.0}};
  CSG::point p1{{1.0,      1.0,      0.0}};
  CSG::point p2{{2.0,      1.0,      0.0}};
  CSG::point p3{{1.0,      2.0,      0.0}};
  CSG::point p4{{1.0,      -1.0,      0.0}};
  CSG::point p5{{2.0,      -1.0,      0.0}};
  CSG::point p6{{1.0,      -0.1,      0.0}};
  CSG::point p7{{100.0,     1e-3,      0.0}};
  CSG::point p8{{-100.0,    -1e-3,      0.0}};
  CSG::point p9{{1.0,    0.0,      0.0}};
  CSG::point p10{{1.0,    0.0,      -1.0}};
  CSG::point p11{{0.0,    1.0,      -1.0}};

// edges
  CSG::edge e0{1,2};
  CSG::edge e1{1,3};
  CSG::edge e2{4,5};
  CSG::edge e3{4,6};
  CSG::edge e4{7,8};
  CSG::edge e5{0,9};
  CSG::edge e6{0,10};
  CSG::edge e7{0,11};

// test plane
  CSG::plane p{{0, 1.0, 0.0}, 0.0};
  
  std::vector< CSG::point> verts{p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  std::vector< CSG::edge> edge_map{e0, e1, e2, e3, e4, e5, e6, e7};

  for (size_t i = 0; i < 4; i++) {
    if(planeEdgeIntersection(p, i, edge_map, verts)) {
      std::cout << "Test " << i <<"a: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " does not intersect the test plane." << std::endl;
      return 1;
    }

    if(coincidentWithPlane(i, p, edge_map, verts)) {
      std::cout << "Test " << i <<"b: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " is not coincident to the test plane." << std::endl;
      return 1;
    }
  }
  
  for (size_t i = 4; i < 5; i++) {
    if(!planeEdgeIntersection(p, i, edge_map, verts)) {
      std::cout << "Test " << i <<"a: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " intersects the test plane." << std::endl;
      return 1;
    }

    if(coincidentWithPlane(i, p, edge_map, verts)) {
      std::cout << "Test " << i <<"b: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " is not coincident to the test plane." << std::endl;
      return 1;
    }
  }
  
  for (size_t i = 5; i < 7; i++) {
    if(planeEdgeIntersection(p, i, edge_map, verts)) {
      std::cout << "Test " << i <<"a: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " is parallel to the test plane." << std::endl;
      return 1;
    }

    if(!coincidentWithPlane(i, p, edge_map, verts)) {
      std::cout << "Test " << i <<"b: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " is coincident to the test plane." << std::endl;
      return 1;
    }
  }

  for (size_t i = 7; i < 8; i++) {
    if(planeEdgeIntersection(p, i, edge_map, verts)) {
      std::cout << "Test " << i <<"a: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " does not intersect the test plane." << std::endl;
      return 1;
    }

    if(coincidentWithPlane(i, p, edge_map, verts)) {
      std::cout << "Test " << i <<"b: ";
      std::cout << "FAILUE!" << std::endl;
      std::cout << "Edge: " << i << " is not coincident to the test plane." << std::endl;
      return 1;
    }
  }
  
  std::cout << "SUCCESS!\n" << std::endl;  
  return 0;
}

int edgeIntersectionTest() {

  CSG::point p0{{0.0,      0.0,      0.0}};
  CSG::point p1{{1.0,      0.0,      0.0}};
  CSG::point p2{{1.0,      1.0,      0.0}};
  CSG::point p3{{0.5,      -1.0,      0.0}};
  CSG::point p4{{0.5,      1.0,      0.0}};

// edges
  CSG::edge e0{0,1};
  CSG::edge e1{2,0};
  CSG::edge e2{3,4};
  
  std::vector< CSG::point> verts{p0, p1, p2, p3, p4};
  size_t v_count = verts.size();

  std::vector< CSG::edge> edge_map{e0, e1, e2};
  size_t edges = edge_map.size();

  // normal for plane
  float N0[3] = {0.0, 0.0, 1.0};

  std::vector<struct CSG::plane> normals;
  
  for (size_t i = 0; i < edges; i++) {
    auto edge = edge_map[i];
    normals.push_back( CSG::edge::planeForEdge(edge,verts, N0) );
  }

  IntersectionMap m;
  size_t behind, infront;
  for(size_t i = 0; i < edges ; i++) {
    for(size_t j = 0; j < edges ; j++)
    {
      if (i != j) {
        if (planeEdgeIntersection(normals[i],j,edge_map,verts)) {
          computeEdgeEdgeIntersection(i, j, verts, normals, edge_map, m, behind, infront);
        }
      }
    }
  }
 
  if (verts.size() - v_count != 2) {
    std::cout << "FAILURE: should have only created 2 points." << std::endl;
    std::cout << "Instead of " << verts.size() - v_count;
    std::cout <<" points." << std::endl;
    return 1;
  }
  
  if (edge_map.size() - edges != 8) {
    std::cout << "FAILURE: should have created 8 new edges." << std::endl;
    std::cout << "Instead of " << edge_map.size() - edges;
    std::cout <<" edges." << std::endl;
    return 1;
  }

  printf("\n\nLIST INTERSECTIONS:\n");
  for (size_t i = v_count; i < verts.size(); i++) {
    auto pt = verts[i];
    printf("{%f, %f, %f}\n",pt[0], pt[1], pt[2]);
  }
  printf("\n\n");

  std::cout << "SUCCESS!\n" << std::endl;
  return 0;

}

