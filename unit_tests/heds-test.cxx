#include <iostream>
#include <sstream>
#include "heds.h"
#include <vector>
#include <math.h>
#include <chrono>
#include <stdlib.h>
#include <time.h>

int testHeds();
void makeShape(std::vector<size_t> &mesh, std::vector<float> & verts, size_t &ne, size_t &nv);

struct solid {
  
  std::vector<topology::he_vert *> verts;
  std::vector<topology::he_edge *> edges;
  std::vector<topology::he_face *> faces;
};

int main(int argc, char ** argv) {
  
  std::cout << "Testing heds..." << std::endl;
  if(testHeds()) {
    std::cout <<"testHeds FAILED!" << std::endl;
    return 1;
  }
  std::cout << "SUCCESS!" << std::endl;
}

void makeShape(std::vector<size_t> &mesh, std::vector<float> & verts, size_t &ne, size_t &nv) {

  float a[12] = {0.0, 0.0, 0.0,
                  1.0, 1.0, 0.0,
                  0.0, 1.0, 0.0,
                  1.0, 0.0, 0.0};

  for(size_t i = 0; i < 12; i++) verts.push_back(a[i]);
  
  size_t m[6] = {0, 1, 2, 0, 3, 1};
  for(size_t i = 0; i < 6; i++) mesh.push_back(m[i]);

  nv = 4;
  ne = 2;
}

void makeCirlce(size_t N, std::vector<size_t> &mesh, std::vector<float> & verts, size_t &ne, size_t &nv) {
  
  verts.push_back(0);
  verts.push_back(0);
  verts.push_back(0);
  
  float dth = 2*M_PI/N;
  for(size_t i = 0; i < N; i++)
  {
    verts.push_back(cos(i*dth));
    verts.push_back(sin(i*dth));
    verts.push_back(0);
  }
  
  for (size_t i = 1; i <= N; i++) {
    mesh.push_back(0);
    mesh.push_back(i);
    mesh.push_back((i)%(N)+1);
  }
  
  nv = N+1;
  ne = N;
}

solid * solidFromData(std::vector<size_t> &mesh, std::vector<float> & verts, size_t ne, size_t nv) {
  // make verts
  solid * S = new solid;
  for (size_t i = 0; i < nv; i++) {
    topology::he_vert * v = new topology::he_vert;
    v->id = i;
    v->x = verts[3*i];
    v->y = verts[3*i+1];
    v->z = verts[3*i+2];
    v->edge = nullptr;
    S->verts.push_back(v);
  }

 // make edges and faces
  for (size_t i = 0; i < ne; i++) {
    topology::he_face * f = new topology::he_face();
    f->id  = i;

    for(size_t j = 0; j < 3; j++) {
      topology::he_edge * e = new topology::he_edge();
      e->id = 3*i+j;
      e->target = S->verts[mesh[3*i + (j+1)%3]];
      f->insert(e);
      S->edges.push_back(e);
    }
    f->close();
    S->faces.push_back(f);
  }
  return S;
}

void listHalfEdges(const solid * S) {
  std::ostringstream oss;
  oss << "Listing all half-edges: ";
  for(auto &e : S->edges) {
    if (e->pair)
      oss << e->id << " ";
  }
  oss << std::endl;
  std::cout << oss.str() << std::endl;
}

void listFaceVertAdjacencies(const solid * S) {
  std::ostringstream oss;
  oss << "Listing all face-vert adjacencies:\n";
  
  for(auto &f : S->faces) {
    auto e = f->edge;
    oss << "face " << f->id << ": ";
    do {
      oss << e->target->id << " ";
      e = e->next;
    } while (e != f->edge);
    oss << std::endl;
  }
  std::cout << oss.str() << std::endl;
}

void listFaceEdgeAdjacencies(const solid * S) {
  std::ostringstream oss;
  oss << "Listing all face-edge adjacencies:\n";
  
  for(auto &f : S->faces) {
    auto e = f->edge;
    oss << "face " << f->id << ": ";
    do {
      oss << e->id << " ";
      e = e->next;
    } while (e != f->edge);
    oss << std::endl;
  }
  std::cout << oss.str() << std::endl;
}

void listVertFaceAdjacencies(const solid * S) {
  std::ostringstream oss;
  oss << "Listing all vert-face adjacencies:\n";
  for(auto &v : S->verts) {
    auto e = v->edge;
    oss << "vert " << v->id << ": ";
    do {
      oss << e->face->id << " ";
      if (e->pair) e = e->pair->next;
      else if (e->pred->pair) e = e->pred->pair;
      else e = nullptr;
    } while (e && e != v->edge);
    oss << std::endl;
  }
  std::cout << oss.str() << std::endl;
}

void destroySolid(solid * S) {
 for(size_t i = 0; i < S->edges.size(); i++) delete S->edges[i]; 
 for(size_t i = 0; i < S->verts.size(); i++) delete S->verts[i]; 
 for(size_t i = 0; i < S->faces.size(); i++) delete S->faces[i]; 
}

int testHeds() {
  size_t ne, nv;
  std::vector<size_t> mesh;
  std::vector<float> verts;
//  makeShape(mesh,verts, ne, nv);
  makeCirlce(5, mesh, verts, ne, nv);
  auto t1 = std::chrono::high_resolution_clock::now();
  solid * S = solidFromData(mesh,verts, ne, nv);
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> duration = t2 - t1;
  std::cout << "\nTopology contruction time: " << duration.count() << "s\n";
  // Test
  listHalfEdges(S);
  listFaceVertAdjacencies(S);
  listFaceEdgeAdjacencies(S);
  listVertFaceAdjacencies(S);
  destroySolid(S);
  return 0;
}

