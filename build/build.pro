TEMPLATE = app
#include(../common.pri)
QT += widgets
CONFIG += qt c++14
macx {
  QMAKE_MAC_SDK = macosx10.11
  QMAKE_CXXFLAGS += -stdlib=libc++
  INCLUDEPATH += /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/Headers/

  LIBS += -L/usr/lib/ -lblas
}

linux {
  INCLUDEPATH += /usr/include/openblas/
  LIBS += -lopenblas
}

INCLUDEPATH += ../gui ../shapes
LIBS += -L../gui/debug -lgui -L../shapes/debug -lshapes -L../shape_operators/debug -lshape_operators -L../data_managers/debug -ldata_managers
RESOURCES += resources.qrc
message("Building build, qt settings: $$QT")
message("Building build, inlcude settings: $$INCLUDEPATH")
message("Building build, lib settings: $$LIBS")

SOURCES += main.cpp

# Will build the final executable in the main project directory.
TARGET = shape-tool

release:DESTDIR = release
release:OBJECTS_DIR = release/.obj
release:MOC_DIR = release/.moc
release:RCC_DIR = release/.rcc
release:UI_DIR = release/.ui

debug:DESTDIR = debug
debug:OBJECTS_DIR = debug/.obj
debug:MOC_DIR = debug/.moc
debug:RCC_DIR = debug/.rcc
debug:UI_DIR = debug/.ui
