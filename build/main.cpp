#include "MainWindow.h"
#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.resize(w.sizeHint());
    int desktopArea = QApplication::desktop()->width()
      * QApplication::desktop()->height();
    int windowArea = w.width() * w.height();
    if (((float)windowArea / (float)desktopArea) < 0.75)
      w.show();
    else w.showMaximized();
    QObject::connect(&a, SIGNAL(focusChanged(QWidget*, QWidget*)),
		     &w, SLOT(focusChanged(QWidget*, QWidget*)));
    return a.exec();
}
