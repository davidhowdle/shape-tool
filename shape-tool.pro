#-------------------------------------------------
#
# Project created by dhowdle 2015-12-10T11:16:48
# http://stackoverflow.com/questions/1417776/how-to-use-qmakes-subdirs-template
#-------------------------------------------------
#message(Include path: $$INCLUDEPATH)
#message(Libs: $$LIBS)
#message(Config: $$CONFIG)
#message(Qt: $$QT)

TEMPLATE = subdirs
SUBDIRS = shapes shape_operators data_managers gui meshing matrix_utils csg

# build must be last:
CONFIG += ordered
SUBDIRS += build
