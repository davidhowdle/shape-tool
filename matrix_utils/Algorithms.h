#include <map>

namespace SparseLA {
  namespace Algorithms {
    

      class MergeSort {
        public:
          template <typename T>
          static void sort(T * x, size_t N) {
            T * bff = new T [N];
            MergeSort::sort<T>(x, N, bff);
            delete [] bff;
          }

          template <typename T, typename S>
          static void sort(T *x, S * y, size_t N) {
            T * bff = new T [N];
            S * bffy = new S [N];
            MergeSort::sort<T,S>(x, y, N, bff, bffy);
            delete [] bff;
            delete [] bffy;
          }

        protected:
          /// merge sorting
          template <typename T>
          static void swap(T &a, T &b) {
              if (a > b) {
                T tmp = a;
                a = b;
                b = tmp;
              }
          }

          template <typename T, typename S>
          static void swap(T &a, T &b, S &c, S &d) {
              if (a > b) {
                std::swap(a,b);
                std::swap(c,d);
              }
          }

          template <typename T>
          static void merge(T * a, size_t na,
                     T *b, size_t nb,
                     T * c) {
            
            size_t ia = 0;
            size_t ib = 0;
            size_t k = 0;
            while (ia < na && ib < nb) {
              if(a[ia] < b[ib]) {
                c[k++] = a[ia++];
              }else {
                c[k++] = b[ib++];
              }
            }
            
            if(ia == na) for(; ib < nb; ib++) c[k++] = b[ib];
            else for(; ia < na; ia++) c[k++] = a[ia];
            for(k = 0; k < na+nb; k++) a[k] = c[k];
          }
          
          template <typename T, typename S>
          static void merge(T * a, S * ya, size_t na,
                            T * b, S * yb, size_t nb,
                            T * c, S * d) {
            
            size_t ia = 0;
            size_t ib = 0;
            size_t k = 0;
            while (ia < na && ib < nb) {
              if(a[ia] < b[ib]) {
                c[k] = a[ia];
                d[k++] = ya[ia++];
              }else {
                c[k] = b[ib];
                d[k++] = yb[ib++];
              }
            }
            
            if(ia == na) for(; ib < nb; ib++) {
              c[k] = b[ib];
              d[k++] = yb[ib];
            } else for(; ia < na; ia++) {
              c[k] = a[ia];
              d[k++] = ya[ia];
            }
            
            for(k = 0; k < na+nb; k++) {
              a[k] = c[k];
              ya[k] = d[k];
            }
          }

          template <typename T>
          static void sort(T * x, size_t N, T * bff) {
            if(N == 2) { MergeSort::swap<T>(x[0], x[1]); }
            else if (N > 2) {
              MergeSort::sort<T>(&x[0],   N/2,    bff);
              MergeSort::sort<T>(&x[N/2], N-N/2,  bff);
              if(x[N/2-1] > x[N/2])
                MergeSort::merge<T>(x, N/2, &x[N/2], N-N/2, bff);
            }
          }
          
          template <typename T, typename S>
          static void sort(T * x, S * y, size_t N, T * bff, S * bffy) {
            if(N == 2) { MergeSort::swap<T, S>(x[0], x[1], y[0], y[1]); }
            else if (N > 2) {
              MergeSort::sort<T, S>(&x[0],  &y[0],    N/2,    bff, bffy);
              MergeSort::sort<T, S>(&x[N/2],&y[N/2],  N-N/2,  bff, bffy);
              if(x[N/2-1] > x[N/2])
                MergeSort::merge<T, S>(x, y,N/2, &x[N/2], &y[N/2], N-N/2, bff, bffy);
            }
          }
       };

      class HeapSort {
        public:
          template <typename T>
          static void sort(T * x, size_t N) {
            T * bff = new T [N];
            bool * is_set = new bool [N]();
            size_t * rep = new size_t [N]();
            std::map<size_t, size_t> bidx;
            HeapSort::sort<T>(x, rep, is_set, bidx, N, bff);
            delete [] rep;
            delete [] is_set;
            delete [] bff;
          }
      
        private:

          static inline size_t left(size_t idx) { return 2 * idx + 1;}
          static inline size_t right(size_t idx) { return 2 * idx + 2;}
        static inline bool has(const std::map<size_t, size_t> & map, size_t k) {
          return map.find(k) != map.end();
        }
          template <typename T>
          static void sort(T * x,
                           size_t * rep,
                           bool * is_set,
                           std::map<size_t, size_t> & bidx,
                           size_t N, T * bff) {
            bff[0] = x[0];
            is_set[0] = 1;
            bidx[0] = 0; // set root
            
            size_t k = 0;
            
            for(size_t i = 1; i < N; i++) {
              while(has(bidx, k) && x[i] < bff[bidx[k]]) {
                k = HeapSort::left(k);
              }

              while (has(bidx, k) && x[i] > bff[bidx[k]]) {
                k = HeapSort::right(k);
              }
             
              if (has(bidx, k))
                rep[bidx[k]]++;
              else {
                bff[i] = x[i];
                bidx[k] = i;
              }
              k = 0;
            }
            k = 0;
            HeapSort::inOrder<T>(x,rep,is_set,bidx,N,0,k,bff);
          }

          template <typename T>
          static void inOrder(T * x,
                              size_t * rep,
                              bool * is_set,
                              std::map<size_t, size_t> & bidx,
                              size_t N,
                              size_t idx,
                              size_t &k,
                              T * bff) {
            
            if(has(bidx,idx)) {
              inOrder(x,rep,is_set,bidx,N,left(idx),k,bff);
              x[k++] = bff[bidx[idx]];
              for(size_t i = 0; i < rep[bidx[idx]]; i++)
                x[k++] = bff[bidx[idx]];
              
              inOrder(x,rep,is_set, bidx,N,right(idx),k,bff);
            } 
          }
      };
  }
}
