// SPARSE LINEAR ALGEBRA
#include <stdlib.h>
#include <iostream>
namespace SparseLA {

struct triplet {
  size_t i;
  size_t j;
  int v;
  
  bool operator<(const triplet &b) const{
    if (this->i < b.i) return true;
    else if (this->i > b.i) return false;
    else return this->j < b.j;
  }
  
  bool operator>(const triplet &b) const{
    if (this->i > b.i) return true;
    else if (this->i < b.i) return false;
    else return this->j > b.j;
  }
  
  bool operator==(const triplet &b) const {
    return this->i == b.i && this->j == b.j;
  }
  
  friend std::ostream & operator<<(std::ostream &os, const triplet & x) {
    os << "(";
    os << x.i << ", ";
    os << x.j << ", ";
    os << x.v << ")";
    return os;
  }
  
};

}
