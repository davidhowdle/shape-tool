#include "CRSMatrix.h"
#include "Algorithms.h"
#include <stack>
#include <cassert>

using namespace SparseLA;

CRSMatrix::CRSMatrix(size_t r, size_t c)
  : num_values(0), num_rows(r), num_cols(c), is_mutable(true), constructed(false) {
}

CRSMatrix::~CRSMatrix() {
  if(this->constructed) {
    delete [] row_ptr;
    delete [] col_ptr;
    delete [] val_ptr;
  }
}

size_t CRSMatrix::uniqueValues(const std::vector<triplet> & data) {
  size_t k = data.size();

  auto it = std::make_pair(data.begin(), data.begin()+1);
  for (; it.first != data.end(); ++it.second)
  {
    while (it.second != data.end() && *(it.first) == *(it.second)) {
      k--;
      ++it.second;
    }
    it.first = it.second;
  }
  return k;
}

void CRSMatrix::setMatrix(std::vector<triplet> & data) {
  
  Algorithms::MergeSort::sort<triplet>(&data[0], data.size());
  this->num_values = CRSMatrix::uniqueValues(data);
  this->row_ptr = new size_t [this->num_rows+1]();
  this->row_ptr[this->num_rows] = this->num_values;
  
  this->col_ptr = new size_t [this->num_values]();
  this->val_ptr = new int [this->num_values]();
  
  size_t kidx = 0;
  size_t ridx = 1;
  size_t v;
  auto it = std::make_pair(data.begin(), data.begin()+1);
  for (; it.first != data.end(); ++it.second)
  {
    v = (it.first)->v;
    while (it.second != data.end() && *it.first == *it.second) {
      v += (it.second)->v;
      ++it.second;
    }
    
    this->val_ptr[kidx] = v;
    this->col_ptr[kidx] = (it.first)->j;
    if((it.first)->i != (it.second)->i) {
      this->row_ptr[ridx++] = kidx + 1;
    }
    
    it.first = it.second;
    kidx++;
  }

  this->constructed = true;
  this->is_mutable = false;
}

CRSMatrix CRSMatrix::transpose() const {

  CRSMatrix B(this->rows(), this->cols());
  std::vector<triplet> data; data.reserve(this->num_values);
  triplet tmp{0,0,0};
  for(size_t i = 0; i < this->rows(); i++) {
    for(size_t j = this->row_ptr[i]; j < this->row_ptr[i+1]; j++) {
      tmp.i = this->col_ptr[j];
      tmp.j = i;
      tmp.v = this->val_ptr[j];
      data.push_back(tmp);
    }
  }

  B.setMatrix(data);
  return B;
}

int CRSMatrix::operator()(size_t i, size_t j) const {
  //assert(i < this->rows());
  //assert(j < this->cols());
  for (size_t k = this->row_ptr[i]; k < this->row_ptr[i+1]; k++) {
    if (this->col_ptr[k] == j) return this->val_ptr[k];
  }
  return 0;
}

CRSMatrix CRSMatrix::operator*(const CRSMatrix &B) const {
  CRSMatrix C(this->rows(),B.cols());
  std::vector<triplet> data;
  triplet tmp{0,0,0};
  size_t A_idx_j, Aij, Bjk;
  for(size_t i = 0; i < this->rows(); i++) {
    for(size_t j = this->row_ptr[i]; j < this->row_ptr[i+1]; j++) {
      // j is a non-zero column for row i
      // want to do arithemetic only if row j
      // column k is non zero
      A_idx_j = this->col_ptr[j];
      Aij = this->val_ptr[j];

      for(size_t k = B.getRowPtr()[A_idx_j]; k < B.getRowPtr()[A_idx_j+1]; k++) {
        if(!(Bjk = B.getValuePtr()[k]))
          continue;
        tmp.i = i;
        tmp.j = B.getColPtr()[k];
        tmp.v = Aij* Bjk;
        data.push_back(tmp);
      }
    }
  }
  C.setMatrix(data);
  return C;
}

void CRSMatrix::product(int * x, int * y) const {
  for(size_t i = 0; i < this->rows(); i++) {
    for(size_t j = this->row_ptr[i]; j < this->row_ptr[i+1]; j++) {
      y[i] += this->val_ptr[j]*x[this->col_ptr[j]];
    }
  }
}
