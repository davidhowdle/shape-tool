#include "Triplet.h"
#include <iostream>
#include <vector>

namespace SparseLA {

  class CRSMatrix {
    public:
      CRSMatrix(size_t r, size_t c);
      ~CRSMatrix();

      const size_t  * getRowPtr()   const { return this->row_ptr; }
      const size_t  * getColPtr()   const { return this->col_ptr; }
      const int     * getValuePtr() const { return this->val_ptr; }
      size_t    nonZeros()        const {return this->num_values;}
      size_t    rows()        const {return this->num_rows;}
      size_t    cols()        const {return this->num_cols;}

      void            setMatrix(std::vector<triplet> & data);
      CRSMatrix       transpose() const;
      int operator()(size_t i, size_t j) const;
      CRSMatrix operator*(const CRSMatrix &B) const;
      void product(int * x, int * y) const;
    
      friend std::ostream &operator<<( std::ostream &output,
                                  const CRSMatrix &M) {
          output << "values:";
          auto row_ptr = M.getRowPtr();
          auto col_ptr = M.getColPtr();
          auto val_ptr = M.getValuePtr();
          for(size_t i = 0; i < M.nonZeros(); i++) output << " " << val_ptr[i];
          output << std::endl;
          
          output << "columns:";
          for(size_t i = 0; i < M.nonZeros(); i++) output << " " << col_ptr[i];
          output << std::endl;
          
          output << "compressed rows:";
          for(size_t i = 0; i <= M.rows(); i++) output << " " << row_ptr[i];
          output << std::endl;
        
        // print matrix
          output << std::endl;
        for(size_t i = 0; i < M.rows(); i++) {
          for (size_t j = 0; j < M.cols(); j++) {
            output << M(i,j) << " ";
          }
          output << std::endl;
        }
        return output;
        }
      
    private:
      size_t      * row_ptr;
      size_t      * col_ptr;
      int         * val_ptr;
      size_t        num_values;
      size_t        num_rows;
      size_t        num_cols;
      bool          is_mutable;
      bool          constructed;
    
      static size_t uniqueValues(const std::vector<triplet> & data);
  };
}
