#ifndef BOOL_EDITOR_H
#define BOOL_EDITOR_H

#include <QWidget>
#include <QHBoxLayout>
#include <QCheckBox>
#include <QLineEdit>
#include <QValidator>

class BoolEditor : public QWidget {

Q_OBJECT

public:

  BoolEditor(QWidget* parent = 0);
  ~BoolEditor();

  bool getState();
  QString getText();
  void setText(const QString& text);
  void setValidator(QValidator* v);

private:

  QHBoxLayout* layout;
  QCheckBox* checkBox;
  QLineEdit* editor;

};

#endif
