#include "PropertyView.h"

#include "BoolEditor.h"
#include "ListEditor.h"

#include <QLayout>
#include <QValidator>
#include <QLineEdit>

PropertyView::PropertyView(QWidget* parent)
  : QWidget(parent),
    currentObject(nullptr){

}

PropertyView::~PropertyView() {

}

void PropertyView::clear() {
  if (!this->layout()) return;
  while (this->layout()->itemAt(0) != 0) {
    QLayoutItem* item = this->layout()->takeAt(0);
    this->layout()->removeWidget(item->widget());
    this->layout()->removeItem(item);
    delete item->widget();
    delete item;
  }
  delete this->layout();
}

void PropertyView::set(CADObject* object) {
  currentObject = object;
  PropertyManager* pm = currentObject->getPropMgmt();
  property_map* properties = pm->getProperties();
  auto itr = properties->begin();
  QVBoxLayout* layout = new QVBoxLayout(this);
  for (; itr != properties->end(); ++itr) {
    std::string name = itr->first;
    OBJECT_TYPE type = itr->second.type;
    QWidget* editor = this->getEditor(type, name, pm);
    if (!editor) continue;
    QGroupBox* gb = new QGroupBox(QString::fromStdString(name), this);
    QVBoxLayout* gbl = new QVBoxLayout(gb);
    gbl->addWidget(editor);
    gb->setLayout(gbl);
    layout->addWidget(gb);
  }
  QPushButton* update = new QPushButton("Update", this);
  update->setDefault(true);
  connect(update, SIGNAL(clicked()), this, SIGNAL(update()));
  layout->addWidget(update);
  QSpacerItem* spacer
    = new QSpacerItem(1,1,QSizePolicy::Fixed, QSizePolicy::Expanding);
  layout->addSpacerItem(spacer);
  if (!this->layout()) this->setLayout(layout);
}

QWidget* PropertyView::getEditor(OBJECT_TYPE type,
				 const std::string& name,
				 PropertyManager* pm) {
  switch (type) {
    case INT: {
      std::string value = pm->getValue(name);
      QLineEdit* editor = new QLineEdit(this);
      QIntValidator* v = new QIntValidator(editor);
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case INT_OPT: {
      std::string value = pm->getValue(name);
      BoolEditor* editor = new BoolEditor(this);
      QIntValidator* v = new QIntValidator(editor);
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case SCALAR: {
      std::string value = pm->getValue(name);
      QLineEdit* editor = new QLineEdit(this);
      QDoubleValidator* v = new QDoubleValidator(editor);
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case SCALAR_OPT: {
      std::string value = pm->getValue(name);
      BoolEditor* editor = new BoolEditor(this);
      QDoubleValidator* v = new QDoubleValidator(editor);
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case INT_ARRAY: {
      std::string value = pm->getValue(name);
      QLineEdit* editor = new QLineEdit(this);
      QRegExpValidator* v = new QRegExpValidator(editor);
      QString rx = "^\\d+";
      int n = QString::fromStdString(value).split(",").size();
      for (int i = 0; i < (n-1); ++i) {
	rx = rx + ",\\d+";
      }
      rx = rx + "$";
      v->setRegExp(QRegExp(rx));
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case INT_ARRAY_OPT: {
      std::string value = pm->getValue(name);
      BoolEditor* editor = new BoolEditor(this);
      QRegExpValidator* v = new QRegExpValidator(editor);
      QString rx = "^\\d+";
      int n = QString::fromStdString(value).split(",").size();
      for (int i = 0; i < (n-1); ++i) {
	rx = rx + ",\\d+";
      }
      rx = rx + "$";
      v->setRegExp(QRegExp(rx));
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case SCALAR_ARRAY: {
      std::string value = pm->getValue(name);
      QLineEdit* editor = new QLineEdit(this);
      QRegExpValidator* v = new QRegExpValidator(editor);
      QString rx = "^\\d+(\\.\\d+)?";
      int n = QString::fromStdString(value).split(",").size();
      for (int i = 0; i < (n-1); ++i) {
	rx = rx + ",\\d+(\\.\\d+)?";
      }
      rx = rx + "$";
      v->setRegExp(QRegExp(rx));
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case SCALAR_ARRAY_OPT: {
      std::string value = pm->getValue(name);
      BoolEditor* editor = new BoolEditor(this);
      QRegExpValidator* v = new QRegExpValidator(editor);
      QString rx = "^\\d+(\\.\\d+)?";
      int n = QString::fromStdString(value).split(",").size();
      for (int i = 0; i < (n-1); ++i) {
	rx = rx + ",\\d+(\\.\\d+)?";
      }
      rx = rx + "$";
      v->setRegExp(QRegExp(rx));
      editor->setValidator(v);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case STRING: {
      std::string value = pm->getValue(name);
      QLineEdit* editor = new QLineEdit(this);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case STRING_OPT: {
      std::string value = pm->getValue(name);
      BoolEditor* editor = new BoolEditor(this);
      editor->setText(QString::fromStdString(value));
      return editor;
    }
    case CAD_OBJECT_ARRAY: {
      ListEditor* editor = new ListEditor(this);
      QString accName = QString::fromStdString(name) + "CADObjectArray";
      editor->setAccessibleName(accName);
      editor->setShapes(pm->getValue(name));
      return editor;
    }
    default: return nullptr;
  }
}

void PropertyView::updateProperties() {
  PropertyManager* pm = currentObject->getPropMgmt();
  if (!pm) return;
  QVBoxLayout* layout = dynamic_cast<QVBoxLayout*>(this->layout());
  if (!layout) return;
  int count = layout->count();
  for (int i = 0; i < (count-4); ++i) {
    QGroupBox* gb = dynamic_cast<QGroupBox*>(layout->itemAt(i)->widget());
    if (!gb) continue;
    QVBoxLayout* gbl = dynamic_cast<QVBoxLayout*>(gb->layout());
    if (!gbl) continue;
    for (int j = 0; j < gbl->count(); ++j) {
      QLayoutItem* editorItem = gbl->itemAt(j);
      QWidget* editor = dynamic_cast<QWidget*>(editorItem->widget());
      std::string name = gb->title().toStdString();
      if (dynamic_cast<QLineEdit*>(editor)) {
	QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
	std::string value = lineEdit->text().toStdString();
	pm->setValue(name, value);
      }
      else if (dynamic_cast<ListEditor*>(editor)) {
	ListEditor* lw = dynamic_cast<ListEditor*>(editor);
	pm->setValue(name, lw->getShapes());
      }
      else if (dynamic_cast<BoolEditor*>(editor)) {
	BoolEditor* oe = dynamic_cast<BoolEditor*>(editor);
	pm->setValue(name, oe->getText().toStdString());
	std::string optName = name+"Option";
	if (oe->getState()) pm->addProperty(optName, "True", BOOLEAN);
	else pm->addProperty(optName, "False", BOOLEAN);
      }
    }
  }
  currentObject->updateProperties();
  emit updateScene();
}

void PropertyView::addItem(const QModelIndex& index, const QString& internalName, bool& success) {
  CADObject* object = index.data(Qt::UserRole+1).value<CADObject*>();
  if (!object) {
    success = false;
    return;
  }
  if (!dynamic_cast<Shape*>(object)) {
    success = false;
    return;
  }
  QVBoxLayout* layout = dynamic_cast<QVBoxLayout*>(this->layout());
  if (!layout) {
    success = false;
    return;
  }
  QWidget* widget = nullptr;
  for (int i = 0; i < layout->count(); ++i) {
    bool gotItem = false;
    QLayoutItem* item = layout->itemAt(i);
    if (!item->widget()) continue;
    QGroupBox* gb = dynamic_cast<QGroupBox*>(item->widget());
    if (!gb) continue;
    QVBoxLayout* gbl = dynamic_cast<QVBoxLayout*>(gb->layout());
    if (!gbl) continue;
    for (int j = 0; j < gbl->count(); ++j) {
      QLayoutItem* gbli = gbl->itemAt(j);
      if (gbli->widget()->accessibleName() != internalName) continue;
      widget = gbli->widget();
      gotItem = true;
      break;
    }
    if(gotItem) break;
  }
  if (!widget) {
    success = false;
    return;
  }
  ListEditor* list = dynamic_cast<ListEditor*>(widget);
  if (!list) {
    success = false;
    return;
  }
  list->pushShape(object);
  list->addItem(QString::fromStdString(object->getName()));
  list->setFocus(Qt::OtherFocusReason);
  success = true;
  return;
}
