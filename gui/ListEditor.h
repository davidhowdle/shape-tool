#ifndef LIST_EDITOR_H
#define LIST_EDITOR_H

#include <QListWidget>

#include "CADObject.h"
#include "Shape.h"

class ListEditor : public QListWidget {
  Q_OBJECT;

public:

  ListEditor(QWidget* parent = 0);
  ~ListEditor();

  void pushShape(CADObject* shape);
  CADObject* popShape(CADObject* shape);
  std::deque<CADObject*>* getShapes();
  void setShapes(std::deque<CADObject*>* q);

public slots:

  void contextClick(const QPoint& pt);

private:

  std::deque<CADObject*>* shapes;
};

#endif //LIST_EDITOR_H
