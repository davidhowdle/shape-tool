#ifndef OBJECT_VIEW_H
#define OBJECT_VIEW_H

#include "CADObject.h"

class TreeModel;

#include <QTreeView>
class QAction;

class ObjectView : public QTreeView {
  Q_OBJECT;

public:

  ObjectView(QWidget* parent = 0);
  virtual ~ObjectView();

  TreeModel* getModel() {return model;}

  enum SelectionMode {
    NORMAL = 0,
    SELECTION
  };

  void setRefName(const QString& name) {
    internal = name;
  }

  QModelIndexList getSelection();
  QModelIndex getCurrent();
  CADObject* getObject(const QModelIndex& index);
  void removeRow(int row);
  void setMode(SelectionMode m) {mode = m;}
  void setSelect(const QModelIndex& index);
  void addObject(CADObject* object);

public slots:

  void processPrimitive(QAction* action);
  void processOperator(QAction* action);
  void processBoolOp(QAction* action);
  void selectionChanged(const QItemSelection& previous,
			const QItemSelection& current);
  void contextClick(const QPoint& pt);
  void setInteractive(bool intr) {interactive = intr;}

signals:

  void selection(const QModelIndex& index,
		 const QString& name,
		 bool& success);
  void set(CADObject* object);
  void clear();
  void none();

private: // Members

  TreeModel* model;
  SelectionMode mode;
  QString internal;
  bool interactive;
};

#endif
