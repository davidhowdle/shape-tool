#ifndef PROPERTY_VIEW_H
#define PROPERTY_VIEW_H

#include <QWidget>
#include "PropertyManager.h"

class PropertyView : public QWidget {
  Q_OBJECT
public:
  PropertyView(QWidget* parent = 0);
  ~PropertyView();

public slots:

  void clear();
  void set(CADObject* object);
  void addItem(const QModelIndex& item,
	       const QString& internalName,
	       bool& success);
  void updateProperties();

signals:

  void update();
  void updateScene();

private:

  QWidget* getEditor(OBJECT_TYPE type,
		     const std::string& name,
		     PropertyManager* pm);
  CADObject* currentObject;

};

#endif
