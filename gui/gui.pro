! include( ../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += ../shapes ../data_managers ../shape_operators
FORMS += MainWindow.ui
HEADERS += BoolEditor.h GLView.h  ListEditor.h  LogUtility.h  LogView.h MainWindow.h  ObjectView.h TreeModel.h PropertyView.h
SOURCES += BoolEditor.cpp GLView.cpp  ListEditor.cpp  LogUtility.cpp  LogView.cpp MainWindow.cpp  ObjectView.cpp TreeModel.cpp PropertyView.cpp
