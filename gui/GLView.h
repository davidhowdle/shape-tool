/**
 * @file   GLView.h
 *
 * @author David Howdle <dhowdle@asgard.Home>
 *
 * @brief  OpenGL canvas for displaying shapes
 */

#ifndef GL_VIEW_H
#define GL_VIEW_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>

/**
 * @class GLView
 *
 * @brief Derived from QOpenGLWidget, creates a resizable
 *        canvas on which shapes made using openGL can be
 *        displayed.  Provides support for mouse actions
 *        relating to camera control
 */
class GLView : public QOpenGLWidget {

  Q_OBJECT

public:

  /**
   * Constructor
   *
   * @param parent Pointer to the widget parent
   */
  GLView(QWidget* parent = 0);

  /**
   * Destructor
   */
  virtual ~GLView();

public slots:

    /**
   * Overloaded from QWidget
   * Called when the GLView receives a mouse click
   * Handles different events based on a series of factors,
   * such as button pressed, press or release or hold
   *
   * @param event the event of the mouse click
   */
  virtual void mousePressEvent(QMouseEvent* event);

  /**
   * Overloaded from QWidget
   * Called when GLView recieves a mouse move
   * Handles different gestures on the view,
   * such as rotations of the camera, or scaling the scene
   *
   * @param event the event of the mouse movement
   */
  virtual void mouseMoveEvent(QMouseEvent* event);

  /**
   * Overloaded from QWidget
   * Called when GLView recieves a key press
   * Handles different keys on the view,
   * such as scaling the scene in tandem with the mouse
   *
   * @param event the event form the keyboard
   */
  virtual void keyPressEvent(QKeyEvent* event);

signals:

  /**
   * Emitted during the render cycle to notify
   * the model to draw shapes
   */
  void draw();


protected:

  /**
   * Overloaded from QOpenGLWidget
   * Initializes the openGL canvas
   */
  virtual void initializeGL();

  /**
   * Overloaded from QOpenGLWidget
   * Paints the canvas, called each render cycle
   */
  virtual void paintGL();

  /**
   * Overloaded from QOpenGLWidget
   * Handles the canvas sizing when there is a resize event
   */
  virtual void resizeGL(int width, int height);

  /**
   * Overloaded from QWidget
   * Gives the recommended initial size for this widget
   *
   * @return the recommended size
   */
  virtual QSize sizeHint() const;

private: // Methods

  /**
   * Update the scene for a dimension change
   *
   * @param dim true for 3D, false for 2D
   */
  void setDimensions(bool dim);

  /**
   * Set the axis rotation for the camera
   *
   * @param angle normalized angle of rotation
   */
  void setXRotation(int angle);

  /**
   * Set the axis rotation for the camera
   *
   * @param angle normalized angle of rotation
   */
  void setYRotation(int angle);

  /**
   * Set the axis rotation for the camera
   *
   * @param angle normalized angle of rotation
   */
  void setZRotation(int angle);

  /**
   * Set the axis translation for the camera
   *
   * @param t the translation factor
   */
  void setXTranslation(float t);

  /**
   * Set the axis translation for the camera
   *
   * @param t the translation factor
   */
  void setYTranslation(float t);

  /**
   * Set the scale for the camera
   *
   * @param s the scale factor
   */
  void setScale(float s);

  /**
   * Reset the camera to the default,
   * depending on which dimesion is selected
   */
  void resetView();

  /**
   * Convienience function to normalize angles in the scene
   *
   * @param angle reference to the angle to be normalized
   */
  void normalizeAngle(int& angle);

private: // Members

  /// x rotation
  int xRot;
  /// y rotation
  int yRot;
  /// z rotation
  int zRot;

  /// x translation
  float xTrans;
  /// y translation
  float yTrans;

  /// scale factor
  float scale;

  /// aspectRatio
  float aspectRatio;

  /// last position of mouse
  QPoint lastPos;

  /// dimension boolean
  bool dimension;

};

#endif
