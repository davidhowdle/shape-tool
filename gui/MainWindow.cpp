#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "TreeModel.h"

#include <QFileInfo>
#include <QMessageBox>

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
  ui->setupUi(this);
  this->setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
  this->setDefaultTools();
  connect (ui->primitiveBar, SIGNAL(actionTriggered(QAction*)),
	   ui->objectView, SLOT(processPrimitive(QAction*)));
  connect (ui->transformBar, SIGNAL(actionTriggered(QAction*)),
	   ui->objectView, SLOT(processOperator(QAction*)));
  connect (ui->csgBar, SIGNAL(actionTriggered(QAction*)),
	   ui->objectView, SLOT(processBoolOp(QAction*)));
  connect (ui->miscBar, SIGNAL(actionTriggered(QAction*)),
	   this, SLOT(processMisc(QAction*)));
  connect (ui->glView, SIGNAL(draw()),
	   ui->objectView->getModel(), SLOT(drawShapes()));
  connect (ui->objectView->getModel(), SIGNAL(updated()),
	   ui->glView, SLOT(update()));
  connect (ui->objectView, SIGNAL(clear()), ui->propertyView, SLOT(clear()));
  connect (ui->objectView, SIGNAL(set(CADObject*)), ui->propertyView, SLOT(set(CADObject*)));
  connect (ui->objectView, SIGNAL(selection(const QModelIndex&, const QString&, bool&)),
	   ui->propertyView, SLOT(addItem(const QModelIndex&, const QString&, bool&)));
  connect (ui->propertyView, SIGNAL(updateScene()), ui->glView, SLOT(update()));
  connect (ui->propertyView, SIGNAL(update()), ui->propertyView, SLOT(updateProperties()));
//  connect (ui->actionInteractiveRm, SIGNAL(toggled(bool)), ui->objectView, SLOT(setInteractive(bool)));
}

MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::processMisc(QAction* action) {
  if (action->data().toString() == "3D") {
    ui->objectView->blockSignals(true);
    bool check = action->isChecked();
    ui->primitiveBar->clear();
    if (check) {
      for (QAction* action : actions3D) {
	ui->primitiveBar->addAction(action);
      }
    } else {
      for (QAction* action : actions2D) {
	ui->primitiveBar->addAction(action);
      }
    }
    ui->objectView->blockSignals(false);
  }
}

void MainWindow::setDefaultTools() {
  // By default, 3D prims are used
  QAction* action = new QAction(QIcon(":/cube_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Cube");
  action->setData("Cube");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/cuboid_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Cuboid");
  action->setData("Cuboid");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/cone_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Cone");
  action->setData("Cone");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/cylinder_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Cylinder");
  action->setData("Cylinder");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/sphere_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Sphere");
  action->setData("Sphere");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/ellipsoid_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Ellipsoid");
  action->setData("Ellipsoid");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/tetra_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Tetrahedron");
  action->setData("Tetrahedron");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/trunkcone_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Truncated Cone");
  action->setData("Truncated Cone");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);
  action = new QAction(QIcon(":/torus_icon.png"), "", ui->primitiveBar);
  action->setToolTip("Torus");
  action->setData("Torus");
  actions3D.push_back(action);
  ui->primitiveBar->addAction(action);

  // Operations
  action = new QAction(QIcon(":/mirror_icon.png"), "", ui->transformBar);
  action->setToolTip("Mirror");
  action->setData("Mirror");
  ui->transformBar->addAction(action);
  action = new QAction(QIcon(":/rotate_icon.png"), "", ui->transformBar);
  action->setToolTip("Rotate");
  action->setData("Rotate");
  ui->transformBar->addAction(action);
  action = new QAction(QIcon(":/scale_icon.png"), "", ui->transformBar);
  action->setToolTip("Scale");
  action->setData("Scale");
  ui->transformBar->addAction(action);
  action = new QAction(QIcon(":/translate_icon.png"), "", ui->transformBar);
  action->setToolTip("Translate");
  action->setData("Translate");
  ui->transformBar->addAction(action);
  action = new QAction(QIcon(":/copy_icon.png"), "", ui->transformBar);
  action->setToolTip("Copy");
  action->setData("Copy");
  ui->transformBar->addAction(action);
  action = new QAction(QIcon(":/cartarray_icon.png"), "", ui->transformBar);
  action->setToolTip("Cartesian Array");
  action->setData("Cartesian Array");
  ui->transformBar->addAction(action);
  action = new QAction(QIcon(":/polararray_icon.png"), "", ui->transformBar);
  action->setToolTip("Polar Array");
  action->setData("Polar Array");
  ui->transformBar->addAction(action);

  // CSG Operations
  QString sub = QString("%1").arg(QChar(0x208B));
  action = new QAction(sub, ui->csgBar);
  action->setToolTip("Subtract");
  action->setData("Subtract");
  ui->csgBar->addAction(action);
  QString inter = QString("%1").arg(QChar(0x2229));
  action = new QAction(inter, ui->csgBar);
  action->setToolTip("Intersection");
  action->setData("Intersection");
  ui->csgBar->addAction(action);
  QString uni = QString("%1").arg(QChar(0x222A));
  action = new QAction(uni, ui->csgBar);
  action->setToolTip("Union");
  action->setData("Union");
  ui->csgBar->addAction(action);

  // Misc
  action = new QAction("3D", ui->miscBar);
  action->setToolTip("Change Dimensions");
  action->setData("3D");
  action->setCheckable(true);
  action->setChecked(true);
  ui->miscBar->addAction(action);

  // Finally make and store the 2D primitives
  action = new QAction(QIcon(":/square_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("Sqaure");
  action->setData("Sqaure");
  actions2D.push_back(action);
  action = new QAction(QIcon(":/rectangle_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("Rectangle");
  action->setData("Rectangle");
  actions2D.push_back(action);
  action = new QAction(QIcon(":/circle_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("Circle");
  action->setData("Circle");
  actions2D.push_back(action);
  action = new QAction(QIcon(":/ellipse_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("Ellipse");
  action->setData("Ellipse");
  actions2D.push_back(action);
  action = new QAction(QIcon(":/triangle_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("Triangle");
  action->setData("Tringle");
  actions2D.push_back(action);
  action = new QAction(QIcon(":/ngon_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("N-Sided Polygon");
  action->setData("Hexagon");
  actions2D.push_back(action);
  action = new QAction(QIcon(":/annulus_icon.png"), "",  ui->primitiveBar);
  action->setToolTip("Annulus");
  action->setData("Annulus");
  actions2D.push_back(action);
}

void MainWindow::focusChanged(QWidget* previous, QWidget* current) {
  if (!previous || !current) return;
  if (previous->accessibleName().contains("CADObjectArray")
      && current->accessibleName() == "objectTree") {
    ui->objectView->setMode(ObjectView::SELECTION);
    ui->objectView->setRefName(previous->accessibleName());
  } else {
    ui->objectView->setMode(ObjectView::NORMAL);
  }
}

void MainWindow::on_actionHideLog_toggled(bool checked) {
  if (checked) ui->logDock->hide();
  else ui->logDock->show();
}

void MainWindow::on_actionAbout_triggered() {
  QMessageBox::about(this, "About shape-tool", "Up Ye!");
}

void MainWindow::on_actionQuit_triggered() {
  qApp->quit();
}
