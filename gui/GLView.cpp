#include "GLView.h"

#include <QtWidgets>
#include <QtOpenGL>

#include <GL/glu.h>
#include <iostream>

GLView::GLView(QWidget* parent)
  : QOpenGLWidget(parent),
    xRot(45*16),
    yRot(45*16),
    zRot(0),
    xTrans(0),
    yTrans(0),
    scale(1.0),
    aspectRatio(0),
    dimension(true) {
  this->setFocusPolicy(Qt::StrongFocus);
}

GLView::~GLView() {

}

QSize GLView::sizeHint() const {
  return QSize(800, 400);
}

void GLView::normalizeAngle(int& angle) {
  while (angle < 0) angle += 360 * 16;
  while (angle > 360) angle -= 360 * 16;
}

void GLView::setXRotation(int angle) {
    this->normalizeAngle(angle);
    if (angle == xRot) return;
    xRot = angle;
}

void GLView::setYRotation(int angle) {
    this->normalizeAngle(angle);
    if (angle == yRot) return;
    yRot = angle;
}

void GLView::setZRotation(int angle) {
    this->normalizeAngle(angle);
    if (angle == zRot) return;
    zRot = angle;
}

void GLView::setXTranslation(float t) {
    if (t == xTrans) return;
    xTrans = t;
}

void GLView::setYTranslation(float t) {
    if (t == yTrans) return;
    yTrans = t;
}

void GLView::setScale(float s) {
    if (s == scale) return;
    scale = s;
}

void GLView::setDimensions(bool dim) {
  dimension = dim;
  if(!dimension) {
    this->setXRotation(0);
    this->setYRotation(0);
    this->setZRotation(0);
    this->update();
  }
}

void GLView::keyPressEvent(QKeyEvent* event) {
  switch (event->key()) {
    case Qt::Key_Space: this->resetView(); break;
    default: break;
  }
}

void GLView::mousePressEvent(QMouseEvent* event) {
  lastPos = event->pos();
}

void GLView::mouseMoveEvent(QMouseEvent* event) {
  int dx = event->x() - lastPos.x();
  int dy = event->y() - lastPos.y();
  lastPos = event->pos();
  if ((QApplication::keyboardModifiers() & Qt::ShiftModifier)) {
    if (event->buttons() & Qt::LeftButton) {
      // translate
      this->setXTranslation(xTrans+float(dx)/QWidget::size().width()*8.0);
      this->setYTranslation(yTrans-float(dy)/QWidget::size().height()*8.0);
      this->update();
    } else if (event->buttons() & Qt::RightButton) {
      // scale
      float fact = 1000;
      if (dx < 0 || dy > 0) fact *= -1;
      float sq = float(sqrt(pow(dx, 2) + pow(dy, 2)));
      float gd = QWidget::size().width() * QWidget::size().height();
      float ds = sq / gd * fact;
      this->setScale(this->scale*(1 + ds));
      this->update();
    }
    // Do we need both these methods?
  } else if (dimension && (event->buttons() & Qt::LeftButton)) {
    // rotate
    this->setXRotation(xRot+8*dy);
    this->setYRotation(yRot+8*dx);
    this->update();
  } else if (dimension && (event->buttons() & Qt::RightButton)) {
    this->setXRotation(xRot+8*dy);
    this->setZRotation(zRot+8*dx);
    this->update();
  }
}

void GLView::resetView() {
  if (dimension) {
    this->setXRotation(45*16);
    this->setYRotation(45*16);
    this->setZRotation(0);
  }
  this->setXTranslation(0);
  this->setYTranslation(0);
  this->setScale(1.0);
  this->update();
}

void GLView::initializeGL() {
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  static GLfloat lightPosition[4] = {0,5,5,1.0};
  glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void GLView::paintGL() {
  // Need to handle culling for 2D/3D?
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
  f->glClearColor(1.0, 1.0, 1.0, 1.0);
  glLoadIdentity();
  glTranslatef(xTrans, yTrans, -10.0);
  glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
  glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
  glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);
  glScalef(this->scale, this->scale, this->scale);
  emit draw();
}

void GLView::resizeGL(int width, int height) {
  // reset gl view port
  glViewport(0, 0, width, height);
  // calculate aspect ratio for proper resizing
  this->aspectRatio = float(width) / float(height);
  // need to reset the projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
#ifdef QT_OPENGL_ES_1
  glOrthof(-2*this->aspectRatio, +2*this->aspectRatio, -2, +2, 1.0, 15.0);
#else
  glOrtho(-2*this->aspectRatio, +2*this->aspectRatio, -2, +2, 1.0, 15.0);
#endif
  glMatrixMode(GL_MODELVIEW);
}

