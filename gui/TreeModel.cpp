#include "TreeModel.h"

#include "Shape.h"

#include <iostream>

TreeModel::TreeModel(QObject* parent)
  : QStandardItemModel(parent) {
  qRegisterMetaType<CADObject*>("CADObject");
}

TreeModel::~TreeModel() {

}

void TreeModel::addObject(CADObject* cad_object) {
  if (!cad_object) return;
  QString name = QString::fromStdString(cad_object->getName());
  if (name.isEmpty()) return;
  QStandardItem* item = new QStandardItem(name);
  QVariant data; data.setValue(cad_object);
  item->setData(data);
  this->appendRow(item);
  emit updated();
}

CADObject* TreeModel::getObject(const QModelIndex& index) {
  return index.data(Qt::UserRole+1).value<CADObject*>();
}

void TreeModel::drawShapes() {
  for (int i = 0; i < this->rowCount(); ++i) {
    QModelIndex index = this->index(i, 0);
    CADObject* obj = this->getObject(index);
    if (dynamic_cast<Shape*>(obj)) {
      dynamic_cast<Shape*>(obj)->draw();
    }
  }
}
