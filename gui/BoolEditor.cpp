#include "BoolEditor.h"

BoolEditor::BoolEditor(QWidget* parent)
  : QWidget(parent),
    layout(new QHBoxLayout(this)),
    checkBox(new QCheckBox(this)),
    editor(new QLineEdit(this)) {
  checkBox->setCheckState(Qt::Unchecked);
  layout->addWidget(checkBox);
  layout->addWidget(editor);
  this->setLayout(layout);
}

BoolEditor::~BoolEditor() {

}

bool BoolEditor::getState() {
  Qt::CheckState state = checkBox->checkState();
  if (state == Qt::Checked) return true;
  else return false;
}

void BoolEditor::setText(const QString& text) {
  editor->setText(text);
}

void BoolEditor::setValidator(QValidator* v) {
  editor->setValidator(v);
}

QString BoolEditor::getText() {
  return editor->text();
}
