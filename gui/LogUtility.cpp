#include "LogUtility.h"

void LogUtility::info(const std::string& msg) {
  if (!log) return;
  QString message = QString::fromStdString(msg);
  message = "<font color=\"Blue\">" + message + "</font><br>";
  log->insertHtml(message);
}

void LogUtility::warning(const std::string& msg) {
  if (!log) return;
  QString message = QString::fromStdString(msg);
  message = "<font color=\"Orange\">" + message + "</font><br>";
  log->insertHtml(message);
}

void LogUtility::error(const std::string& msg) {
  if (!log) return;
  QString message = QString::fromStdString(msg);
  message = "<font color=\"Red\">" + message + "</font><br>";
  log->insertHtml(message);
}

void LogUtility::setLog(QTextEdit* logPtr) {
  log = logPtr;
}

QTextEdit* LogUtility::log = 0;
