#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:

  void focusChanged(QWidget* previous, QWidget* current);

private:
  Ui::MainWindow *ui;
  void setDefaultTools();
  QVector<QAction*> actions3D;
  QVector<QAction*> actions2D;

private slots:

  void processMisc(QAction* action);
  void on_actionHideLog_toggled(bool checked);
  void on_actionAbout_triggered();
  void on_actionQuit_triggered();
};

#endif // MAINWINDOW_H
