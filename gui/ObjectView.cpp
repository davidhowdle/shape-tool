#include "ObjectView.h"

#include "TreeModel.h"
#include "Shape.h"
#include "CADObject.h"
#include "ShapeFactory.h"
#include "OperatorFactory.h"

#include <QAction>

#include <iostream>

ObjectView::ObjectView(QWidget* parent)
  : QTreeView(parent),
    model(new TreeModel(this)),
    mode(ObjectView::NORMAL),
    internal("objectTree"),
    interactive(false) {
  this->setModel(model);
  this->header()->hide();
  this->setContextMenuPolicy(Qt::CustomContextMenu);
  this->setAccessibleName("objectTree");
  connect(this->selectionModel(),
	  SIGNAL(selectionChanged(const QItemSelection&,
				  const QItemSelection&)),
	  this,
	  SLOT(selectionChanged(const QItemSelection&,
				const QItemSelection&)));
  connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
	  this, SLOT(contextClick(const QPoint&)));
}

ObjectView::~ObjectView() {

}

void ObjectView::processPrimitive(QAction* action) {
  QString name = action->data().toString();
  if (name.isEmpty()) return;
  CADObject* obj = ShapeFactory::create(name.toStdString());
  if (!obj) return;
  model->addObject(obj);
}

void ObjectView::processOperator(QAction* action) {
  QString name = action->data().toString();
  if (name.isEmpty()) return;
  CADObject* shapeOp = OperatorFactory::create(name.toStdString());
  if (!shapeOp) return;
  model->addObject(shapeOp);
}

void ObjectView::processBoolOp(QAction* action) {
  std::cout << action->data().toString().toStdString() << std::endl;
}

QModelIndexList ObjectView::getSelection() {
  QItemSelectionModel* selectionModel = this->selectionModel();
  return selectionModel->selectedIndexes();
}

QModelIndex ObjectView::getCurrent() {
  QItemSelectionModel* selectionModel = this->selectionModel();
  return selectionModel->currentIndex();
}

CADObject* ObjectView::getObject(const QModelIndex& index) {
  return model->getObject(index);
}

void ObjectView::removeRow(int row) {
  model->removeRow(row);
}

void ObjectView::addObject(CADObject* object) {
  model->addObject(object);
}

void ObjectView::setSelect(const QModelIndex& index) {
  QItemSelectionModel* selectionModel = this->selectionModel();
  if (!index.isValid()) selectionModel->clearSelection();
  else selectionModel->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect);
}

void ObjectView::selectionChanged(const QItemSelection& previous,
				  const QItemSelection& current) {
  switch (mode) {
    case SELECTION: {
      QModelIndex index = this->getCurrent();
      bool success = false;
      if (index.isValid()) emit selection(index, internal, success);
      if (previous.indexes().size() == 1)
	this->setSelect(current.indexes()[0]);
      if (success) break;
    }
    case NORMAL: {
      QModelIndexList list = this->getSelection();
      emit clear();
      if (list.size() != 1) {
	emit none();
	break;
      }
      QModelIndex index = this->getCurrent();
      if (!index.isValid()) break;
      CADObject* object = this->getObject(index);
      if (!object) break;
      emit set(object);
    }
  }
}

void ObjectView::contextClick(const QPoint& pt) {
  QModelIndex index = this->indexAt(pt);
  if (!index.isValid()) return;
  if (interactive) {
    QString title = "Delete Item?";
    QString msg = "Are you sure you wish to delete " + index.data().toString();
    QMessageBox::StandardButton rm
      = QMessageBox::question(this, title, msg,
			      QMessageBox::Yes|QMessageBox::No,
			      QMessageBox::Yes);
    if (rm == QMessageBox::No) return;
  }
  this->removeRow(index.row());
}
