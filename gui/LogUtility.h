#ifndef LOGUTILITY_H
#define LOGUTILITY_H

#include <QTextEdit>

class LogUtility {
public:

  static void info(const std::string& msg);
  static void warning(const std::string& msg);
  static void error(const std::string& msg);

  static void setLog(QTextEdit* logPtr);

private:

  static QTextEdit* log;
};

#endif
