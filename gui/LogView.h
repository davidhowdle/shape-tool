#ifndef LOG_VIEW_H
#define LOG_VIEW_H

#include <QTextEdit>

class LogView : public QTextEdit {

public:

  LogView(QWidget* parent = 0);
  ~LogView();
};

#endif
