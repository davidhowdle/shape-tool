#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QStandardItemModel>
#include "CADObject.h"

class TreeModel : public QStandardItemModel {
  Q_OBJECT

public:

  TreeModel(QObject* parent = 0);
  ~TreeModel();

  void addObject(CADObject* cad_object);
  CADObject* getObject(const QModelIndex& index);

public slots:

  void drawShapes();

signals:

  void updated();

};

#endif
