#include "ListEditor.h"

ListEditor::ListEditor(QWidget* parent)
  : QListWidget(parent),
    shapes(nullptr) {
  this->setContextMenuPolicy(Qt::CustomContextMenu);
  connect (this, SIGNAL(customContextMenuRequested(const QPoint&)),
	   this, SLOT(contextClick(const QPoint&)));
    }

ListEditor::~ListEditor() {

}

void ListEditor::pushShape(CADObject* shape) {
  shapes->push_back(shape);
}

CADObject* ListEditor::popShape(CADObject* shape) {
  auto itr = std::find(shapes->begin(), shapes->end(), shape);
  if (itr == shapes->end()) return nullptr;
  CADObject* tmp = *itr;
  shapes->erase(itr);
  return tmp;
}

std::deque<CADObject*>* ListEditor::getShapes() {
  return shapes;
}

void ListEditor::setShapes(std::deque<CADObject*>* q) {
  shapes = q;
  this->clear();
  auto itr = shapes->begin();
  for (; itr != shapes->end(); ++itr) {
    CADObject* tmp = *itr;
    this->addItem(QString::fromStdString(tmp->getName()));
  }
}

void ListEditor::contextClick(const QPoint& pt) {
  QListWidgetItem* item = this->itemAt(pt);
  if (!item) return;
  this->takeItem(this->row(item));
  delete item;
}
