#include "IntersectionMap.h"

bool    IntersectionMap::hasValue(size_t i, size_t j) {
  IntersectionMap::swap(i,j);
  return this->my_map.find(i) != this->my_map.end() &&
         this->my_map[i].find(j) != this->my_map[i].end();
}

bool    IntersectionMap::hasValue(const IntersectionPair &p) {
  return this->hasValue(p.first, p.second);
}

void    IntersectionMap::insert(size_t i, size_t j, size_t v) {
    IntersectionMap::swap(i,j);
    if(this->my_map.find(i) != this->my_map.end()) 
      this->my_map[i].insert(std::make_pair(j,v));
    else {
      std::map<size_t, size_t> tmp0;
      tmp0.insert(std::make_pair(j,v));
      this->my_map.insert(std::make_pair(i, tmp0));
    }
}

void    IntersectionMap::insert(const IntersectionPair &p, size_t v) {
  this->insert(p.first, p.second, v);
}

size_t  IntersectionMap::getValue(size_t i, size_t j) {
  IntersectionMap::swap(i,j);
  if(this->hasValue(i,j)) {
    return this->my_map[i][j];
  }
  return 0;
}

size_t  IntersectionMap::getValue(const IntersectionPair &p) {
  return this->getValue(p.first, p.second);
}

void    IntersectionMap::setValue(size_t i, size_t j, size_t v) {
  IntersectionMap::swap(i,j);
  if(this->hasValue(i,j)) this->my_map[i][j] = v;
}

void    IntersectionMap::setValue(const IntersectionPair &p, size_t v) {
  this->setValue(p.first, p.second, v);
}

void IntersectionMap::swap(size_t &a, size_t &b) {
  if (b < a) std::swap(a,b);
}
