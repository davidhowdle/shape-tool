#include <iostream>
#include <vector>
#include <limits>
#include <math.h>
#include <stdio.h>

#include "IntersectionMap.h"
#include "Algorithms.h"

namespace CSG {
  enum CLASSIFICATION {BEHIND, INFRONT, COINCIDENT, COINCIDENT_FLIPPED, INTERSECTED, CLASSIFICATION_ERROR};
  enum  CLIPPING_OPERATION{A_AUB, A_ADB, A_AIB, B_AUB, B_ADB, B_AIB}; 
  static float epsilon = std::numeric_limits<float>::epsilon();
  
  
  float dot3(const float * a, const float * b) {
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  }
  
  void cross3(const float * a, const float * b, float * c) {
    c[0] = a[1]*b[2]-a[2]*b[1];
    c[1] = a[2]*b[0]-a[0]*b[2];
    c[2] = a[0]*b[1]-a[1]*b[0];
  }
  
  void norm2(float* V) {
    float L = sqrt(dot3(V,V));
    if (fabs(L) < epsilon ) return;
    for (size_t i = 0; i < 3; i++) V[i] /= L;
  }
  
  struct point {
    
    struct point operator-(const struct point &b) const {
      return { {this->X[0] - b.X[0],
        this->X[1] - b.X[1],
        this->X[2] - b.X[2]} };
    }
    
    struct point operator+(const point &b) const {
      return { {this->X[0] + b.X[0],
        this->X[1] + b.X[1],
        this->X[2] + b.X[2]} };
    }
    
    struct point operator *(float s) const {
      return {{this->X[0]*s, this->X[1]*s, this->X[2]*s}};
    }
    
    bool operator==(const struct point &b) const {
      return  fabs(this->X[0] - b.X[0]) < epsilon &&
              fabs(this->X[1] - b.X[1]) < epsilon &&
              fabs(this->X[2] - b.X[2]) < epsilon;
      
    }
    
    bool operator!=(const struct point &b) {
      return !(this->operator==(b));
    }
    
    bool operator <(const struct point &b) const {
      if(this->X[0] < b.X[0]) return true;
      else if (fabs(this->X[0] - b.X[0]) < epsilon) {
        if(this->X[1] < b.X[1]) return true;
        else if (fabs(this->X[1] - b.X[1]) < epsilon) {
          return this->X[2] < b.X[2];
        }
      }
      return false;
    }
    
    bool operator >(const struct point &b) const {
      if(this->X[0] > b.X[0]) return true;
      else if (fabs(this->X[0] - b.X[0]) < epsilon) {
        if(this->X[1] > b.X[1]) return true;
        else if (fabs(this->X[1] - b.X[1]) < epsilon) {
          return this->X[2] > b.X[2];
        }
      }
      return false;
    }
    
    float operator[](size_t i) const { return this->X[i];}
    float x() const { return this->X[0]; }
    float y() const { return this->X[1]; }
    float z() const { return this->X[2]; }

    float X[3];
    
  };
  
  struct plane {
    
    float operator[](size_t i) const { return this->N[i];}
    float nx() const { return this->N[0]; }
    float ny() const { return this->N[1]; }
    float nz() const { return this->N[2]; }
    
    void flip() {
      for(size_t i = 0; i < 3; i++) this->N[i] *= -1;
      this->d = -this->d;
    }
    
    float N[3];
    float d;
    
  };
  
  float distanceFromPlane(size_t pt,
                          const struct plane &p,
                          const std::vector<point> &verts) {
    
    return dot3(verts[pt].X, p.N) + p.d; // equation of a plane
  }
  
  struct edge {
    edge(size_t a, size_t b) : idxs{a,b} {};
    
    static struct plane planeForEdge(const edge &e,
                                     const std::vector<point> &verts,
                                     const float * N0) {
      
      struct plane p;
      point v = verts[e[1]] - verts[e[0]];
      cross3(v.X, N0, p.N);
      norm2(p.N);
      point p1 = verts[e[0]];
      p.d = -1.0*dot3(p.N,p1.X);
      
      return p;
    }
    
    size_t operator[](size_t i) const { return this->idxs[i]; }
    bool operator==(const struct edge &b) const {
      return (this->idxs[0] == b.idxs[0] && this->idxs[1] == b.idxs[1]);
    }
    
    bool operator!=(const struct edge &b) {
      return !(this->operator==(b));
    }
    
    bool lt(const struct plane &p, const std::vector<point> &verts) const {
      float d1 = distanceFromPlane(this->idxs[0], p, verts);
      float d2 = distanceFromPlane(this->idxs[1], p, verts);
      return d1 < 0.0 && d2 < 0.0;
      
    }
    
    bool le(const struct plane &p, const std::vector<point> &verts) const {
      float d1 = distanceFromPlane(this->idxs[0], p, verts);
      float d2 = distanceFromPlane(this->idxs[1], p, verts);
      return d1 < epsilon && d2 < epsilon;
      
    }
    
    bool gt(const struct plane &p, const std::vector<point> &verts) const {
      float d1 = distanceFromPlane(this->idxs[0], p, verts);
      float d2 = distanceFromPlane(this->idxs[1], p, verts);
      return d1 > 0.0 && d2 > 0.0;
    }
    
    bool ge(const struct plane &p, const std::vector<point> &verts) const {
      float d1 = distanceFromPlane(this->idxs[0], p, verts);
      float d2 = distanceFromPlane(this->idxs[1], p, verts);
      return (d1 > epsilon || fabs(d1) <= epsilon) &&
      (d2 > epsilon  || fabs(d2) <= epsilon);
    }
    
    friend std::ostream& operator<<(std::ostream & os, const struct edge &e) {
      os << "[" << e[0] << ", " << e[1] << "]";
      return os;
    }
    
    void swap() {
      // for flipping orientation
      size_t tmp = idxs[0];
      idxs[0] = idxs[1];
      idxs[1] = tmp;
    }

    point tangent(const std::vector<point> &verts) const {
      point v = verts[this->idxs[1]] - verts[this->idxs[0]];
      norm2(v.X); // get normalized directional vector
      return v;
    }
    
    point intersection(const plane &p,
                       const std::vector<point> &verts) const {
      
      float d1 = distanceFromPlane(this->idxs[0], p, verts);
      float d2 = distanceFromPlane(this->idxs[1], p, verts);
      float s = -d1/(d2-d1);
      point tau = verts[this->idxs[1]] - verts[this->idxs[0]];
      return verts[this->idxs[0]] + tau*s;
    }


    void projectedDistance(const float * tau,
                           const std::vector<point> &verts,
                           std::vector<float> &distance) const {
      distance.push_back(dot3(verts[this->idxs[0]].X,tau));
      distance.push_back(dot3(verts[this->idxs[1]].X,tau));
    }

    size_t idxs[2];
  };
  
  
  bool coincidentWithPlane( size_t pt,
                           const struct plane &p,
                           const std::vector<point> &verts) {
    float d = distanceFromPlane(pt,p,verts);
    return fabs(d) <= epsilon;
  }
  
  bool parallelToPlane( size_t e,
                       const struct plane &p,
                       const std::vector<edge> &edge_map,
                       const std::vector<point> &verts) {
    struct point x = verts[edge_map[e][1]] - verts[edge_map[e][0]];
    float d = dot3(x.X, p.N);
    return fabs(d) <= epsilon;
  }
  
  bool coincidentWithPlane( size_t e,
                           const struct plane &p,
                           const std::vector<edge> &edge_map,
                           const std::vector<point> &verts) {
    return parallelToPlane(e,p,edge_map,verts) &&
    coincidentWithPlane(edge_map[e][0], p, verts) &&
    coincidentWithPlane(edge_map[e][1], p, verts);
  }

  bool sameOrientation(const struct plane &a,
                           const struct plane &b) {
    return fabs(acos(dot3(a.N, b.N))) <= epsilon;
  }
  
  bool planeEdgeIntersection( const struct plane &p,
                             size_t e,
                             const std::vector<edge> &edge_map,
                             const std::vector<point> &verts) {
    
    float d1 = distanceFromPlane(edge_map[e][0], p, verts);
    float d2 = distanceFromPlane(edge_map[e][1], p, verts);
    float sign = d1*d2;
    return sign < 0.0; // may cause problems for small distances
  }
  
  CLASSIFICATION classify(size_t e,
                          const struct plane &p,
                          const std::vector<point> &verts,
                          const std::vector<plane> &normals,
                          const std::vector<edge> &edge_map) {
    
    if (planeEdgeIntersection(p,e,edge_map,verts) ) return INTERSECTED;
    if (coincidentWithPlane(e,p,edge_map,verts) ) {
      return sameOrientation(normals[e],p) ? COINCIDENT : COINCIDENT_FLIPPED;
    }  
    if (edge_map[e].le(p, verts)) return BEHIND;
    if (edge_map[e].ge( p, verts)) return INFRONT;
    
    std::cerr << "ERROR UNABLE TO CLASSIFY EDGE!!!" << std::endl;
    return CLASSIFICATION_ERROR;
  }
  
  bool planeElementIntersection(const struct plane &p,
                                size_t e,
                                const std::vector<edge> &element_map,
                                const std::vector<point> &verts) {
    // assume triangle facets
    float d1 = distanceFromPlane(element_map[e][0], p, verts);
    float d2 = distanceFromPlane(element_map[e][1], p, verts);
    float d3 = distanceFromPlane(element_map[e][2], p, verts);
    
    if (d1 * d2 < epsilon) return true;
    if (d2 * d3 < epsilon) return true;
    return d3 * d1 < epsilon;
  }
  
  void computeEdgeEdgeIntersection(size_t ea, size_t eb,
                                   std::vector<point> &verts,
                                   std::vector<plane> &normals,
                                   std::vector<edge> &edge_map,
                                   IntersectionMap &intersection_map,
                                   size_t &behind, size_t &infront) {
    
    size_t int_idx;
    if (intersection_map.hasValue(ea, eb)) {
      int_idx = intersection_map.getValue(ea, eb);
    } else {
      int_idx = verts.size();
      intersection_map.insert(ea, eb, int_idx);
      point ei = edge_map[eb].intersection(normals[ea], verts);
      verts.push_back(ei);
    }
    
    size_t no_edges = edge_map.size();
    edge_map.push_back(CSG::edge(edge_map[eb][0], int_idx));
    edge_map.push_back(CSG::edge(int_idx, edge_map[eb][1]));
    normals.push_back(normals[eb]);
    normals.push_back(normals[eb]);
    
    if(distanceFromPlane(edge_map[eb][0], normals[ea], verts) > epsilon) {
      behind = no_edges + 1;   infront = no_edges;
    } else {
      behind = no_edges;       infront = no_edges + 1;
    }
  } 

  std::vector<size_t> edgeSort(const std::vector<size_t> &edges,
                const std::vector<point>  & verts,
                const std::vector<edge>   & edge_map,
                      std::vector<float>  & distance
                ) {
    std::vector<size_t> ids;
    if (!edges.empty()) {

      size_t no_verts = edges.size()*2;
      point tau = edge_map[edges[0]].tangent(verts);
      distance.reserve(no_verts);
      ids.reserve(no_verts);
     
     size_t k = 0;
      for(auto &e :edges) {
        edge_map[e].projectedDistance(tau.X, verts, distance);
        ids.push_back(k);
        ids.push_back(k++);
      }

      // sort distances with carry
      SparseLA::Algorithms::MergeSort::sort(&distance[0],&ids[0],no_verts);
    }

    return ids;
  }
  
  std::vector<size_t> pointSort(const std::vector<size_t> &edges,
                               const std::vector<point>  & verts,
                               const std::vector<edge>   & edge_map,
                               std::vector<point>  & ordered
                               ) {
    std::vector<size_t> ids;
    if (!edges.empty()) {
      
      size_t no_verts = edges.size()*2;
      ordered.reserve(no_verts);
      ids.reserve(no_verts);
      
      size_t k = 0;
      for(auto &e :edges) {
        ordered.push_back(verts[edge_map[e][0]]);
        ordered.push_back(verts[edge_map[e][1]]);
        ids.push_back(k++);
        ids.push_back(k++);
      }
      
      // sort distances with carry
      SparseLA::Algorithms::MergeSort::sort<point>(&ordered[0],&ids[0],no_verts);
    }
    
    return ids;
  }

  // set operations for two segments
  void colinearUnion( const size_t &a,
                      const size_t &b,
                      const std::vector<point> &verts,
                      std::vector<plane> &normals,
                      std::vector<edge> &edge_map,
                      std::vector<size_t> &result) {

    std::vector<size_t> edges{a,b};
    std::vector<float> distance; distance.reserve(2);
    auto ids = edgeSort(edges, verts, edge_map, distance);

     if( ids[0] == ids[2] ||
         ids[1] == ids[2] ||
         fabs(distance[1] - distance[2]) < epsilon) {
       edge_map.push_back(CSG::edge(edge_map[edges[ids[0]]][0],edge_map[edges[ids[3]]][1] ));
       result.push_back(edge_map.size()-1);
       normals.push_back(normals[a]);
    }else {
      result.push_back(a);
      result.push_back(b);
    }
  }

  void colinearDifference(  const size_t &a,
                            const size_t &b,
                            const std::vector<point> &verts,
                            std::vector<plane> &normals,
                            std::vector<edge> &edge_map,
                            std::vector<size_t> &result) {

    std::vector<size_t> edges{a,b};
    std::vector<float> distance; distance.reserve(2);
    auto ids = edgeSort(edges, verts, edge_map, distance);

    if ( ids[0] == ids[2] ) {
      if (ids[0] == 0)
        edge_map.push_back(CSG::edge(edge_map[edges[ids[0]]][0], edge_map[edges[ids[1]]][0] ));
      else
        edge_map.push_back(CSG::edge(edge_map[edges[ids[2]]][1], edge_map[edges[ids[3]]][1] ));
       result.push_back(edge_map.size()-1);
       normals.push_back(normals[a]);
    } else if( ids[1] == ids[2] ) {
      
      if ( ids[1] == 1) {
        size_t edge_count = edge_map.size();
        edge_map.push_back(CSG::edge(edge_map[edges[ids[0]]][0], edge_map[edges[ids[1]]][0] ));
        edge_map.push_back(CSG::edge(edge_map[edges[ids[2]]][1], edge_map[edges[ids[3]]][1] ));
        for(size_t i = 0; i < 2; i++) {
          result.push_back(edge_count++);
          normals.push_back(normals[a]);
        }
      }
    }else {
      result.push_back(a);
    }
  }

  void colinearIntersection(  const size_t &a,
                              const size_t &b,
                              const std::vector<point> &verts,
                              std::vector<plane> &normals,
                              std::vector<edge> &edge_map,
                              std::vector<size_t> &result) {

    std::vector<size_t> edges{a,b};
    std::vector<float> distance; distance.reserve(2);
    auto ids = edgeSort(edges, verts, edge_map, distance);

     if( ids[0] == ids[2] ||
         ids[1] == ids[2] ) {
       edge_map.push_back(CSG::edge(edge_map[edges[ids[1]]][0],edge_map[edges[ids[2]]][1] ));
       result.push_back(edge_map.size()-1);
       normals.push_back(normals[a]);
     }
  }
  
  bool computeEdgeEdgeOperation(size_t eA, size_t eB, CLIPPING_OPERATION operation,
                                   const std::vector<point> &verts,
                                   std::vector<plane> &normals,
                                   std::vector<edge> &edge_map,
                                   IntersectionMap &segment_operation_map,
                                   std::vector<size_t> &result) {

    if (segment_operation_map.hasValue(eA, eB)) return false; // we already computed these segments
    segment_operation_map.insert(eA,eB,1); // add segment operation
    if (operation == A_AUB || operation == B_AUB) colinearUnion(eA,eB,verts,normals,edge_map,result);
    else if (operation == A_ADB || operation == B_ADB) colinearDifference(eA,eB,verts,normals,edge_map,result);
    else if (operation == A_AIB || operation == B_AIB) colinearIntersection(eA,eB,verts,normals,edge_map,result);
    return true;
  }

// MERGE A COLINEAR SET
  bool segmentsClosed(const std::vector<int> &visited,const std::vector<size_t> &sorted, int a, int b) {
    for(; b > -1; b--) {
      if (visited[sorted[b]] != 2) return false;
    }
    return  true;
  }

  bool mergeEndPoints(size_t i, const std::vector<size_t> &sorted,
                        const std::vector<float> &distance ) {
    if(i + 1 < sorted.size()) {
      return fabs(distance[i] - distance[i+1]) < CSG::epsilon;
    }
    return  false;
  }

  std::vector<CSG::edge> merge(const std::vector<size_t> &sorted,
                               const std::vector<float> &distance,
                               const std::vector<size_t> &edges,
                               const std::vector<CSG::edge> &edge_map) {
    
    size_t n = edges.size();
    std::vector<int> visited(n,0);
    visited[sorted[0]] = 1;
    size_t a = 0;
    std::vector<CSG::edge> merged;
    merged.reserve(10);
    
    for(size_t i = 0; i < sorted.size() - 1; i++) {
      auto vi = sorted[i+1];
      visited[vi]++;
      if (visited[vi] == 2) {
        if(segmentsClosed(visited, sorted, a, i+1)) {
          if(!mergeEndPoints(i+1,sorted, distance)) {
             merged.push_back(CSG::edge(edge_map[edges[sorted[a]]][0],
                                        edge_map[edges[sorted[i+1]]][1]));
             a = i+2;
          }
        }
      }
    }
    return merged;
  }

  void mergeColinear(const std::vector<size_t> colinear_edges,
                    const std::vector<point> &verts,
                    std::vector<plane> &normals,
                    std::vector<edge> &edge_map,
                    std::vector<size_t> &result) {

    if( !colinear_edges.empty()) {
      std::vector<float> distance;
      auto sorted_ids = CSG::edgeSort(colinear_edges, verts, edge_map, distance);
      auto merged = merge(sorted_ids, distance, colinear_edges, edge_map);
      
      // update data structure
      // get representative normal
      size_t edge_count = edge_map.size();
      edge_map.insert(edge_map.end(), merged.begin(), merged.end());
      for(size_t i = 0; i < merged.size(); i++) {
        result.push_back(edge_count+i);
        normals.push_back(normals[colinear_edges[0]]);
      }
    }
  }

  void mergePoints(const std::vector<size_t> &edges,
                   const std::vector<point> &verts,
                   std::vector<edge> &edge_map) {
    if( !edges.empty()) {
      std::vector<point> distance;
      auto sorted_ids = CSG::pointSort(edges, verts, edge_map, distance);

      // merge the fuckin edges
      size_t i = 0;
      size_t j = 1;
      size_t ei, ej;
      size_t ci, cj;
      size_t vi, vj;
      for(; j < distance.size(); j++) {
        if(!(ci = sorted_ids[i] % 2)) {
          ei = edges[sorted_ids[i]/2];
        } else {
          ei = edges[(sorted_ids[i] - 1)/2];
        }
        
        while (distance[i] == distance[j]) {
          if(!(cj = sorted_ids[j] % 2)) {
            ej = edges[sorted_ids[j]/2];
          } else {
            ej = edges[(sorted_ids[j] - 1)/2];
          }
          vi = edge_map[ei].idxs[ci];
          vj = edge_map[ej].idxs[cj];
          if(vi != vj)
            edge_map[ej].idxs[cj] = edge_map[ei].idxs[ci];
          
          j++;
        }
        i = j;
      }
    }
  }
}
