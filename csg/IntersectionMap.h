#include <map>
#include "IntersectionPair.h"

class IntersectionMap {

public:
  IntersectionMap() {};
  ~IntersectionMap() {};

  bool hasValue(size_t i, size_t j);
  bool hasValue(const IntersectionPair &p);

  void insert(size_t i, size_t j, size_t v);
  void insert(const IntersectionPair &p, size_t v);

  size_t getValue(size_t i, size_t j);
  size_t getValue(const IntersectionPair &p);

  void setValue(size_t i, size_t j, size_t v);
  void setValue(const IntersectionPair &p, size_t v);

private:

  std::map<size_t, std::map<size_t, size_t>> my_map;
  static void swap(size_t &i, size_t &j);
};
