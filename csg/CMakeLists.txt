SET(HEADERS
  IntersectionPair.h
  BijectiveMap.h
  IntersectionMap.h
  CSG_Utilities.h
  BSP2D.h
)

SET(SOURCES 
  IntersectionMap.cpp
  BijectiveMap.cpp
)

include_directories()

SET(EXTERNAL_LIBS
)

add_library(csg_utils ${HEADERS} ${SOURCES})
target_link_libraries(csg_utils ${EXTERNAL_LIBS})
