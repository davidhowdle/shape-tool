#include <map>
#include <set>

#include "IntersectionPair.h"

class BijectiveMap {

public:
  BijectiveMap() {};
  ~BijectiveMap() {};

  bool hasValue(size_t v) const;
  bool hasValue(size_t a, size_t b) const;
  bool hasValue(const IntersectionPair &p) const; 

  void insert(size_t a, size_t b, size_t v);
  void insert(const IntersectionPair &p, size_t v);

  size_t getValue(size_t a, size_t b);
  size_t getValue(const IntersectionPair &p);

  void setValue(size_t a, size_t b, size_t v);
  void setValue(const IntersectionPair &p, size_t v);

private:
  std::map<IntersectionPair, size_t> my_map;
  std::set<size_t> values;
};
