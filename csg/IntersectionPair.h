#include <stdio.h>

class IntersectionPair {
public:
  IntersectionPair(size_t f, size_t s) : first(f), second(s) {}
  IntersectionPair(const IntersectionPair &p2) : first(p2.first), second(p2.second) {}
  ~IntersectionPair() {}

  bool operator==(const IntersectionPair &p2) const {
    if(this->first == p2.first && this->second == p2.second) return true;
    if(this->first == p2.second && this->second == p2.first) return true;
    return false;
  }
  
  bool operator!=(const IntersectionPair &p2) const {
    return !this->operator==(p2);
  }

  bool operator<(const IntersectionPair & p2) const {
    if (this->operator==(p2)) return false;

    if(this->first == p2.first) {
      return this->second < p2.second;
    }
    return this->first < p2.first;
  }

  bool operator>(const IntersectionPair &p2) const {
    return this->operator!=(p2) && !(this->operator<(p2));
  }

  bool operator<=(const IntersectionPair &p2) const {
    return this->operator==(p2) || this->operator<(p2);
  }

  bool operator>=(const IntersectionPair &p2) const {
    return this->operator==(p2) || !(this->operator<(p2));
  }

  void swap() {
    size_t tmp = this->second;
    this->second = this->first;
    this->first = tmp;
  }

  size_t first, second;
};
