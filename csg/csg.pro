! include( ../common.pri) {
  error("meshing dir could not find common config file!")
}

TEMPLATE = lib
CONFIG += staticlib
HEADERS = BSP2D.h BijectiveMap.h CSG_Utilities.h IntersectionMap.h IntersectionPair.h
SOURCES = BijectiveMap.cpp IntersectionMap.cpp


