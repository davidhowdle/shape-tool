#include "BijectiveMap.h"

bool BijectiveMap::hasValue(size_t v) const {
  return this->values.find(v) != this->values.end();
}

bool BijectiveMap::hasValue(size_t a, size_t b) const {
  return this->my_map.find(IntersectionPair(a,b)) != this->my_map.end();
}

bool BijectiveMap::hasValue(const IntersectionPair &p) const {
  return this->my_map.find(p) != this->my_map.end();
}

void BijectiveMap::insert(size_t a, size_t b, size_t v) {
  this->my_map.insert(std::make_pair(IntersectionPair(a,b), v));
}

void BijectiveMap::insert(const IntersectionPair &p, size_t v) {
  this->my_map.insert(std::make_pair(p,v));
  this->values.insert(v);
}

size_t BijectiveMap::getValue(size_t a, size_t b) {
  return this->getValue(IntersectionPair(a,b));
}

size_t BijectiveMap::getValue(const IntersectionPair &p) {
  return this->my_map[p];
}

void BijectiveMap::setValue(size_t a, size_t b, size_t v) {
  this->setValue(IntersectionPair(a,b),v);
}

void BijectiveMap::setValue(const IntersectionPair &p, size_t v) {
  if( this->hasValue(p)) {
    this->values.erase(this->my_map[p]);
    this->my_map[p] = v;
  } else {
    this->insert(p,v);
  }
}

