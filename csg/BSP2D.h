#include "CSG_Utilities.h"
#include <string>
#include <fstream>
#include <stack>

namespace CSG {

  enum  BSPT_OPERATION{NONE, PUSH_LEFT, PUSH_RIGHT};

  struct bsp2d_node {
    
    bsp2d_node(size_t e)
      : element(e), left(nullptr), right(nullptr) {}

// member variables
    size_t element;
    bsp2d_node * left;
    bsp2d_node * right;
    std::vector<size_t> coincident;
    std::vector<size_t> coincident_flipped;
  };

  struct bsp2d_data {
    std::vector<point>  verts;
    std::vector<plane>  normals;
    std::vector<edge>   element_map;
    IntersectionMap     intersection_map;
    IntersectionMap     segment_operation_map;
  };

  void insert(bsp2d_node * &T, size_t e, bsp2d_data * data) {
    
    if(T) {
      auto classification = CSG::classify(e, data->normals[T->element], data->verts, data->normals, data->element_map);
      switch (classification) {
        case BEHIND:      insert(T->left,   e, data); break;
        case INFRONT:     insert(T->right,  e, data); break;
        case COINCIDENT:  T->coincident.push_back(e); break;
        case COINCIDENT_FLIPPED:  T->coincident_flipped.push_back(e); break;
        case INTERSECTED: {
           size_t behind, infront;
           computeEdgeEdgeIntersection(T->element, e,
                                      data->verts, data->normals, data->element_map, data->intersection_map,
                                      behind, infront);
           insert(T->left, behind,  data); 
           insert(T->left, infront, data); 
        } break;
        default: break;
      }
    } else {
      T = new bsp2d_node(e);
    }
  }
    
  void clip(  const bsp2d_node *T,
              size_t e,
              bsp2d_data * data,
              BSPT_OPERATION bsp_operation,
              CLIPPING_OPERATION operation,
              std::vector<size_t> &clipped) {

      if(T) {
        auto classification = CSG::classify(e, data->normals[T->element], data->verts, data->normals, data->element_map);
        switch (classification) {
          case BEHIND:      clip(T->left,   e, data, PUSH_LEFT,  operation, clipped); break;
          case INFRONT:     clip(T->right,  e, data, PUSH_RIGHT, operation, clipped); break;
          case COINCIDENT:  {
            // need to perform the operation on this segment e and this nodes defining element
            std::vector<size_t> resultant; // the res
            size_t a = T->element;
            size_t b = e;
            if(operation == B_AUB || operation == B_ADB || operation == B_AIB) std::swap(a,b);
            if (CSG::computeEdgeEdgeOperation(a, b, operation,
                                              data->verts,
                                              data->normals,
                                              data->element_map,
                                              data->segment_operation_map,
                                              resultant)) {
              clipped.insert(clipped.end(), resultant.begin(), resultant.end());
            }
          } break;
          case COINCIDENT_FLIPPED: break;
          case INTERSECTED: {
             size_t behind, infront;
             computeEdgeEdgeIntersection(T->element, e,
                                        data->verts,
                                        data->normals,
                                        data->element_map,
                                        data->intersection_map,
                                        behind, infront);
             clip(T->left, behind , data, PUSH_LEFT,  operation, clipped); 
             clip(T->right, infront, data, PUSH_RIGHT, operation, clipped);
          } break;
          default: break;
        }
      } else {
        if (bsp_operation == PUSH_LEFT      &&
            (operation == A_AIB             ||
             operation == B_AIB             ||
             operation == A_ADB))   { clipped.push_back(e); }
        else if(bsp_operation == PUSH_RIGHT &&
            (operation == A_AUB ||
             operation == B_AUB ||
             operation == B_ADB))   { clipped.push_back(e); }
      } 
  }

  void merge(bsp2d_node *T, bsp2d_data *data) {
    if(T) {
      merge(T->left,  data);
      // if we have a coplanar set we need to merge the set 
      // and update our value/s
      if (!T->coincident.empty()) {
        std::vector<size_t> result;
        T->coincident.push_back(T->element);
        CSG::mergeColinear(T->coincident, data->verts, data->normals, data->element_map, result);
        if( !result.empty()) {
          T->element = result[0];
          T->coincident.clear();
          T->coincident.insert(T->coincident.begin(), result.begin()+1, result.end());
        } else std::cerr << "OH SHIT, OUR MERGE RETURNED NOTHING! SHOULD NOT BE POSSIBLE..." << std::endl;

      }
      merge(T->right, data);
    }
  }
  
  void reverse(bsp2d_node *T, bsp2d_data * data) {
    if(T) {
      bsp2d_node * tmp = T->left;
      T->left = T->right;
      T->right = tmp;
      data->element_map[T->element].swap();
      data->normals[T->element].flip();
      reverse(T->left, data);
      reverse(T->right, data); 
    }
  }

  void print(const bsp2d_node *T, std::vector<size_t> &elements) {
    if(T) {
      print(T->right, elements);
      elements.push_back(T->element);
      for(auto &e : T->coincident) elements.push_back(e);
      print(T->left, elements);
    }  
  }
  
  void mergePoints(const bsp2d_node * T, bsp2d_data *data) {
    std::vector<size_t> edges;
    CSG::print(T, edges);
    CSG::mergePoints(edges, data->verts, data->element_map);
  }

  void destroy(bsp2d_node * T) {
    if(T) {
      destroy(T->left);
      destroy(T->right);
      delete T;
    }
  }

  void writeToFile(const bsp2d_node *T,
      const std::string & file_path,
      const std::string &file_name) {

    std::ofstream ofs(file_path + "/" + file_name);
    std::stack<const bsp2d_node *> stk;
    stk.push(T);
    while(!stk.empty()) {
      auto node = stk.top();
      stk.pop();
      ofs << node->element;

      if(node->left) ofs << "\t" << node->left->element;
      else ofs << "\t" << "X";

      if(node->right) ofs << "\t" << node->right->element;
      else ofs << "\t" << "X";

      for(auto & e : node->coincident) ofs << "\t" << e;
      ofs << std::endl;

      if(node->left) stk.push(node->left);
      if(node->right) stk.push(node->right);
    }

    ofs.close();
  }
  
  void writeShapeToFile(const bsp2d_node *T,
      const bsp2d_data * data,
      const std::string & file_path,
      const std::string &file_name) {

    // write verts
    std::ofstream ofs_verts(file_path + "/" + file_name + "_verts.txt");
    for(auto &v : data->verts) ofs_verts << v[0] <<"\t" << v[1] << "\t" << v[2] << std::endl;
    ofs_verts.close();
    
    // write edges
    std::vector<size_t> edges;
    print(T,edges);
    std::ofstream ofs_mesh(file_path + "/" + file_name + "_mesh.txt");
    for(auto &e : edges) ofs_mesh << e << "\t" << data->element_map[e][0] <<" \t" << data->element_map[e][1] << std::endl;
    ofs_mesh.close();
  }

  void writeShapeToFile(const std::vector<size_t> &edges,
      const bsp2d_data * data,
      const std::string & file_path,
      const std::string &file_name) {

    // write verts
    std::ofstream ofs_verts(file_path + "/" + file_name + "_verts.txt");
    for(auto &v : data->verts) ofs_verts << v[0] <<"\t" << v[1] << "\t" << v[2] << std::endl;
    ofs_verts.close();
    
    // write edges
    std::ofstream ofs_mesh(file_path + "/" + file_name + "_mesh.txt");
    for(auto &e : edges) ofs_mesh << e << "\t" << data->element_map[e][0] <<" \t" << data->element_map[e][1] << std::endl;
    ofs_mesh.close();
  }

}
