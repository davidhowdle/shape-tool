#include "ConvexDelaunay2D.h"
#include "LiteList.h"

ConvexDelaunay2D::ConvexDelaunay2D(const float * coordinates, size_t nc)
  : MeshGenerator(coordinates, nc){};

void ConvexDelaunay2D::convexInsertVertex(const size_t &u, const meshing_utils::edge &vw) {
  int idx_t = -1;
  size_t x;
  meshing_utils::edge wv = {vw.b, vw.a};
  if(this->edge_adjacencies.find(wv) != this->edge_adjacencies.end()) {
    // get adjacent vert
    idx_t = this->edge_adjacencies[wv];
    x = this->triangles[idx_t].adjacent(wv);
  }
  
  if( idx_t >= 0 && 
      meshing_utils::inCircle(this->vertices[u], this->vertices[vw.a], this->vertices[vw.b], this->vertices[x]) > 0.0) {
    this->mesh.remove(this->triangles[idx_t].id);
    meshing_utils::edge vx{vw.a,x};
    meshing_utils::edge xw{x,vw.b};
    convexInsertVertex(u, vx);
    convexInsertVertex(u, xw);
  } else {
    meshing_utils::addTriangle(  u, vw.a, vw.b,
                            this->edge_adjacencies, this->triangles, this->mesh); // utility function
  }
}

void ConvexDelaunay2D::setMesh() {
  // make lite list to track insertion order
  size_t nV = this->vertices.size();
  lite_list insertion_order (nV);

  // get random permutation
  std::vector<size_t> pi = meshing_utils::randomPermutation(0, nV, nV);

  // begin algorithm
  for (size_t i = nV-1; i > 2; i--) insertion_order.soft_remove(pi[i]);
  
  // make first triangle
  auto pi0 = pi[0];
  meshing_utils::addTriangle( pi0, insertion_order.getNext(pi0), insertion_order.getPred(pi0),
                this->edge_adjacencies, this->triangles, this->mesh);
  
  // insert vertices in reverse order
  for (size_t i = 3; i < nV; i++) {
    auto idx = pi[i];
    auto v = insertion_order.getNext(idx);
    auto w = insertion_order.getPred(idx);
    convexInsertVertex(idx, {v,w});
  }
}
  
void ConvexDelaunay2D::printToFile(const std::string &path, const std::string &file_name) const {
  meshing_utils::printToFile(this->mesh, this->triangles, this->vertices, path, file_name);
}
