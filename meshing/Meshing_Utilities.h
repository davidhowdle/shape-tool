#ifndef MESHING_UTILITIES_H
#define MESHING_UTILITIES_H

#include <string>
#include <vector>
#include <stack>
#include <list>
#include <set>
#include <map>
#include <random>
#include <limits>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdlib.h> // size_t

namespace meshing_utils {

  struct edge {
    size_t a, b;
    
    bool operator < (const struct edge &e) const {
      if (a < e.a) return true;
      else if (a == e.a) return b < e.b;
      return false;
    }

    bool operator == (const struct edge &e) const {
      return a == e.a && b == e.b;
    }

    bool operator != (const struct edge &e) const {
      return !this->operator==(e);
    }

    void reverse() {
      std::swap(this->a, this->b);
    }

  };

  struct point3 {
    float x, y, z;
  };

  struct triangle {
    size_t a, b, c, id;

    size_t operator[](size_t idx) const {
      switch (idx) {
        case 0: return this->a; break;
        case 1: return this->b; break;
        case 2: return this->c; break;
        default: return this->id; break;
      }
    }

    size_t adjacent(const struct edge & e) const {
      if (e.a == this->b) return this->a;
      if (e.b == this->b) return this->c;
      return this->b;
    }

    struct edge adjacent(const size_t &p) const {
      if    (p == this->a) return {this->b, this->c};
      if    (p == this->b) return {this->c, this->a};
      return {this->a, this->b};
    }

    int adjacent( const size_t &p, 
                  std::map<struct edge, size_t> &edge_map, 
                  size_t &adjacent_triangle) {
      auto e = this->adjacent(p);
      e.reverse();
      if (edge_map.find(e) != edge_map.end()) {
        adjacent_triangle = edge_map[e];
        return 1;
      }
      return 0;
    }
    
    bool operator ==(const struct triangle & t) {
      if(t.id == this->id) return true;
      return a == t.a && b == t.b && c == t.c;
    }
  };

  void addTriangle(size_t a, size_t b, size_t c,
                    std::map<struct edge, size_t> &edge_map,
                    std::vector<struct triangle> &triangles,
                    std::list<size_t> &mesh);

  std::vector<size_t> randomPermutation(size_t lower_bound,
                                        size_t upper_bound,
                                        size_t N);

  float inCircle(const struct point3 &a,
                 const struct point3 &b,
                 const struct point3 &c,
                 const struct point3 &d);
    

  float orient2d(const struct point3 &a,
                 const struct point3 &b,
                 const struct point3 &c);

  void digCavity( const point3 &p,
                  const size_t & t,
                  const std::vector<struct point3> &points,
                  const std::vector<struct triangle> &faces,
                  std::list<size_t> &mesh,
                  std::map<struct edge, size_t> &edge_map, 
                  std::vector<int> &visited,
                  std::stack<struct edge> &cavity);

  void makeHole(  size_t &t,
                  const std::set<edge> &hole,
                  std::map<struct edge, size_t> &edge_map, 
                  const std::vector<struct triangle> &faces,
                  std::list<size_t> &mesh,
                  std::vector<int> &visited);

  void printToFile(const std::list<size_t> &face_list,
                   const std::vector<struct triangle> &faces,
                   const std::vector<struct point3> &points,
                   const std::string &path,
                   const std::string &file_name);
    
}
#endif
