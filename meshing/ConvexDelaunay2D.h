#ifndef CONVEX_DELAUNAY_H
#define CONVEX_DELAUNAY_H

#include "MeshGenerator.h"

class ConvexDelaunay2D : public MeshGenerator {

public:
  ConvexDelaunay2D(const float * coordinates, size_t nc);
  ~ConvexDelaunay2D(){};
  virtual void setMesh();
  virtual void printToFile(const std::string &path, const std::string &file_name) const;
  const std::vector<meshing_utils::triangle> & getTriangles() const { return this->triangles; }
  
  friend std::ostream& operator<<(std::ostream &os, const ConvexDelaunay2D & dt2d) {
    // prints mesh facets to ostream
    os << dt2d.getMesh().size() << " triangles in constrained delaunay mesh: " << std::endl;
    for(auto &ti : dt2d.getMesh()) {
      os << "triangle " << ti << ": ";
      os << dt2d.getTriangles()[ti].a << " ";
      os << dt2d.getTriangles()[ti].b << " ";
      os << dt2d.getTriangles()[ti].c << std::endl;
    }

    return os;
  }

private:
  std::map<meshing_utils::edge, size_t> edge_adjacencies;
  std::vector<meshing_utils::triangle> triangles;

  void convexInsertVertex(const size_t &u, const meshing_utils::edge &vw);
};

#endif
