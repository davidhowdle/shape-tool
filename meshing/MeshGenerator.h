#ifndef MESH_GENERATOR_H
#define MESH_GENERATOR_H
#include "Meshing_Utilities.h"

class MeshGenerator {
public:
  MeshGenerator(const float * coordinates, const size_t &nc);
  virtual ~MeshGenerator(){};
  virtual void setMesh() = 0;
  virtual void printToFile(const std::string &path, const std::string &file_name) const = 0;
  const std::list<size_t> & getMesh() const { return this->mesh; }

  static void convertToPoints(const float * coordinates,
                              const size_t &nc,
                              std::vector<meshing_utils::point3> &points);

protected:
  std::list<size_t> mesh;
  std::vector<meshing_utils::point3> vertices;
};

#endif
