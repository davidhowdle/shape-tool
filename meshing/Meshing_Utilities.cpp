#include "Meshing_Utilities.h"


void meshing_utils::addTriangle(size_t a, size_t b, size_t c,
                  std::map<struct edge,size_t> &edge_map,
                  std::vector<struct triangle> &triangles,
                  std::list<size_t> &mesh) {
  size_t tn = triangles.size();
  struct triangle t{a,b,c,tn};
  edge_map[{a,b}] = tn;
  edge_map[{b,c}] = tn;
  edge_map[{c,a}] = tn;
  triangles.push_back(t);
  mesh.push_back(tn);
}

std::vector<size_t> meshing_utils::randomPermutation(size_t lower_bound, size_t upper_bound, size_t N) {
  std::vector<size_t> pi(N,0);
  size_t d;
  std::default_random_engine generator;
  std::uniform_int_distribution<size_t> distribution(lower_bound, upper_bound);
  
  for(size_t i = lower_bound, k = 0; i <= upper_bound; i++, k++) pi[k] = i;
  for(size_t i = N-1; i > 0; i--) {
    d = distribution(generator) % i;
    std::swap(pi[d], pi[i]);
  }
  return pi;
}

float meshing_utils::inCircle(const struct point3 &a,
               const struct point3 &b,
               const struct point3 &c,
               const struct point3 &d) {
  
  float A11 = a.x - d.x;
  float A12 = a.y - d.y;
  float A13 = powf(A11, 2) + powf(A12, 2);
  
  float A21 = b.x - d.x;
  float A22 = b.y - d.y;
  float A23 = powf(A21, 2) + powf(A22, 2);
  
  float A31 = c.x - d.x;
  float A32 = c.y - d.y;
  float A33 = powf(A31, 2) + powf(A32, 2);
  
  float det = A11 * (A22*A33 - A23*A32) -
  A12 * (A21*A33 - A23*A31) +
  A13 * (A21*A32 - A22*A31);
  det = fabsf(det) < std::numeric_limits<float>::epsilon() ? 0.0 : det;
  return det;
  
}

float meshing_utils::orient2d(const struct point3 &a,
               const struct point3 &b,
               const struct point3 &c) {
  float A11 = a.x - c.x;
  float A12 = a.y - c.y;
  
  float A21 = b.x - c.x;
  float A22 = b.y - c.y;
  
  float det = A11*A22 - A12*A21;
  det = fabsf(det) < std::numeric_limits<float>::epsilon() ? 0.0 : det;
  return det;
}

void meshing_utils::digCavity(const point3 &p,
                              const size_t & t,
                              const std::vector<struct point3> &points,
                              const std::vector<struct triangle> &faces,
                              std::list<size_t> &mesh,
                              std::map<struct edge, size_t> &edge_map,
                              std::vector<int> &visited,
                              std::stack<struct edge> &cavity) {
  // t should be the index of a triangle that contains the point p.
  // the purpose of this method is to dig around the containing triangle.
  
  if (visited[t] == 0 ) {
    visited[t] = 1;
    mesh.remove(t);
    auto tri = faces[t];
    size_t adjacent_triangle = 0;
    for (size_t i = 0; i < 3; i++) {
      if (tri.adjacent(tri[i], edge_map, adjacent_triangle) &&
          inCircle(points[faces[adjacent_triangle].a],
                   points[faces[adjacent_triangle].b],
                   points[faces[adjacent_triangle].c],
                   p ) >= 0.0) {
            digCavity(p, adjacent_triangle, points, faces, mesh, edge_map, visited, cavity);
          } else {
            cavity.push(tri.adjacent(tri[i]));
          }
    }
  }
}
  
void meshing_utils::makeHole( size_t &t,
                              const std::set<edge> &hole,
                              std::map<struct edge, size_t> &edge_map, 
                              const std::vector<struct triangle> &faces,
                              std::list<size_t> &mesh,
                              std::vector<int> &visited) {
  // t must be a valid facet in the hole
  if (visited[t] == 0) {
    visited[t] = 1;
    mesh.remove(t);
    auto facet = faces[t];
    for (size_t i = 0; i < 3; i++) {
      auto ei = facet.adjacent(facet[i]);
      if ( (hole.find(ei) == hole.end()) &&
          facet.adjacent(facet[i], edge_map, t))  {
          meshing_utils::makeHole(t, hole, edge_map, faces, mesh, visited);
      }
    }
  }
}

void meshing_utils::printToFile(const std::list<size_t> &face_list,
                 const std::vector<struct triangle> &faces,
                 const std::vector<struct point3> &points,
                 const std::string &path,
                 const std::string &file_name) {
  
  // print verts
  std::ofstream coordinates(path + "/" + file_name + "_coordinates.txt");
  for (auto &pt : points) coordinates << pt.x << "\t" << pt.y << "\t" << pt.z << std::endl;
  coordinates.close();
  
  std::ofstream mesh(path + "/" + file_name + "_mesh.txt");
  for (auto &t : face_list) mesh << faces[t].a << "\t" << faces[t].b << "\t" << faces[t].c << std::endl;
  mesh.close();
}

