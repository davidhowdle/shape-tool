#include <stdlib.h> // size_t
#include <vector>

class lite_list {
public:
  lite_list(size_t n) : next(n,0), pred(n,0), N(n) {
    for(size_t i = 0; i < n; i++) {
      this->next[i] = (i+1)%n;
      this->pred[i] = (n+i-1)%n;
    }
  }
  
  void soft_remove(size_t i) {
    // does not change size of elements
    // just modifies next and preds
    if(i < this->N) {
      auto nxt = this->next[i];
      auto prd = this->pred[i];
      this->next[prd] = nxt;
      this->pred[nxt] = prd;
    }
  }
  
  size_t getNext(size_t idx) const {
    return this->next[idx];
  }
  
  size_t getPred(size_t idx) const {
    return this->pred[idx];
  }
  
private:
  std::vector<size_t> next;
  std::vector<size_t> pred;
  size_t N;
};
