#include "ConstrainedDelaunay2D.h"
#include "LiteList.h"
#include "Meshing_Utilities.h"

ConstrainedDelaunay2D::ConstrainedDelaunay2D(const float * coordinates, size_t nc)
  : MeshGenerator(coordinates, nc){};

void ConstrainedDelaunay2D::cavityInsertVertex(const size_t &u, const meshing_utils::edge &vw) {
  int idx_t = -1;
  size_t x;
  meshing_utils::edge wv{vw.b, vw.a};
  if(this->edge_adjacencies.find(wv) != this->edge_adjacencies.end()) {
    // get adjacent vert
    idx_t = this->edge_adjacencies[wv];
    x = this->triangles[idx_t].adjacent(wv);
  }
  
  if(idx_t < 0 ||
     (meshing_utils::orient2d(this->vertices[u], this->vertices[vw.a], this->vertices[vw.b]) > 0.0 &&
      meshing_utils::inCircle(this->vertices[u], this->vertices[vw.a], this->vertices[vw.b], this->vertices[x]) <= 0.0)) {
       // constrained delaunay
      meshing_utils::addTriangle(u, vw.a, vw.b, this->edge_adjacencies, this->triangles, this->mesh);
     } else {
       this->mesh.remove(this->triangles[idx_t].id);
       meshing_utils::edge vx{vw.a,x};
       meshing_utils::edge xw{x,vw.b};
       cavityInsertVertex(u, vx);
       cavityInsertVertex(u, xw);
     }
}

void ConstrainedDelaunay2D::sortedPermutation(std::vector<size_t> &pi, lite_list &insertion_order) {
  // precompute distances
  size_t nV = this->vertices.size();
  std::vector<float> distance(nV,0);
  for(size_t i = 0; i < nV; i++) {
    distance[i] = meshing_utils::orient2d(this->vertices[0], this->vertices[i], this->vertices[nV-1]);
  }

  size_t pi0;
  std::default_random_engine generator;
  std::uniform_int_distribution<size_t> distribution(1,nV-2);
  for (int i = nV-3; i >= 0; i--) {
    pi0 = pi[i];
    auto di = distance[pi0];
    auto dn = distance[insertion_order.getNext(pi0)];
    auto dp = distance[insertion_order.getPred(pi0)];
    while (di <= dn && di <= dp) {
      size_t j = distribution(generator)%(i-1);
      std::swap(pi[i], pi[j]);
      pi0 = pi[i];
      di = distance[pi0];
      dn = distance[insertion_order.getNext(pi0)];
      dp = distance[insertion_order.getPred(pi0)];
    }
    insertion_order.soft_remove(pi[i]);
  }
}

void ConstrainedDelaunay2D::setMesh() {
  // make lite list to track insertion order
  size_t nV = this->vertices.size(); 
  lite_list insertion_order (nV);
  // set permutation
  std::vector<size_t> pi = meshing_utils::randomPermutation(1, nV-2, nV-2);

  // sort the random permutation 
  this->sortedPermutation(pi, insertion_order); 
  
  // make first triangle
  meshing_utils::addTriangle(0,pi[0], nV-1, this->edge_adjacencies, this->triangles, this->mesh);
  
  size_t pi0;  
  for (size_t i = 1; i < nV-2; i++) {
    pi0= pi[i];
    auto v = insertion_order.getNext(pi0);
    auto w = insertion_order.getPred(pi0);
    cavityInsertVertex(pi0, {v,w});
  }
}

void ConstrainedDelaunay2D::insertPoint(const meshing_utils::point3 &p) {
  
  std::stack<meshing_utils::edge> cavity;
  std::vector<int> visited(this->triangles.size(),0);
  auto it = this->mesh.begin();
  while ( cavity.empty() && it != this->mesh.end()) {
    auto t = this->triangles[*it];
    if(meshing_utils::inCircle(this->vertices[t.a],
                               this->vertices[t.b],
                               this->vertices[t.c],
                               p) >= 0.0) {
      meshing_utils::digCavity(p, *it,
                               this->vertices,
                               this->triangles,
                               this->mesh,
                               this->edge_adjacencies,
                               visited,
                               
                               cavity);
    }
    ++it;
  }
  
  if (cavity.empty()) return;
  size_t pno = this->vertices.size();
  this->vertices.push_back(p);
  while( !cavity.empty()) {
    auto e = cavity.top();
    cavity.pop();
    meshing_utils::addTriangle(pno, e.a, e.b, this->edge_adjacencies, this->triangles, this->mesh);
  }
}
void ConstrainedDelaunay2D::insertPLC(const float * coordinates, size_t nc) {
  // 0. convert to a vector of points
  std::vector<meshing_utils::point3> new_points; new_points.reserve(nc/3);
  MeshGenerator::convertToPoints(coordinates, nc, new_points);

  // 1. insert all points
  for (auto & p : new_points) this->insertPoint(p);
}

void ConstrainedDelaunay2D::insertHole(const float * coordinates, size_t nc) {

  // 0. get current size of points
  size_t v0 = this->vertices.size();

  // 1. create plc edge set
  std::set<meshing_utils::edge> plc_edges;
  for (size_t i = 0; i < nc; i++) plc_edges.insert({v0+i, v0 + (i+1)%nc});

  // 2. insert plc
  this->insertPLC(coordinates, nc);
  
  // 3. make hole
  std::vector<int> visited(this->triangles.size(), 0);
  auto e0 = plc_edges.begin();
  auto t0 = this->edge_adjacencies[*e0];
  meshing_utils::makeHole(t0,
                          plc_edges,
                          this->edge_adjacencies,
                          this->triangles,
                          this->mesh,
                          visited);
}
  
void ConstrainedDelaunay2D::printToFile(const std::string &path, const std::string &file_name) const {
  meshing_utils::printToFile(this->mesh, this->triangles, this->vertices, path, file_name);
}
