#include "MeshGenerator.h"

MeshGenerator::MeshGenerator(const float * coordinates, const size_t &nc) {
  this->vertices.reserve(nc);
  MeshGenerator::convertToPoints(coordinates, nc, this->vertices);
}

void MeshGenerator::convertToPoints(const float * coordinates,
                                    const size_t &nc,
                                    std::vector<meshing_utils::point3> &points) {
  points.clear();
  for (size_t i = 0; i < nc; i++) {
    points.push_back({coordinates[3*i], coordinates[3*i+1], coordinates[3*i + 2]});
  }
}
