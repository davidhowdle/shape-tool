! include( ../common.pri) {
  error("meshing dir could not find common config file!")
}

TEMPLATE = lib
CONFIG += staticlib
HEADERS = ConstrainedDelaunay2D.h    ConvexDelaunay2D.h    LiteList.h      MeshGenerator.h     Meshing_Utilities.h
SOURCES = ConstrainedDelaunay2D.cpp ConvexDelaunay2D.cpp    MeshGenerator.cpp   Meshing_Utilities.cpp
