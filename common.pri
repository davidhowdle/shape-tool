QT       += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXX = clang++
CONFIG += qt debug c++14
INCLUDEPATH += . ..

macx {
  message("BUILDING FOR MAC OSX")
  QMAKE_MAC_SDK = macosx10.11
  QMAKE_CXXFLAGS += -stdlib=libc++
  INCLUDEPATH += /opt/X11/include/ \
                 /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/Headers/
  LIBS += /opt/X11/lib/ /usr/lib/
}

linux {
  INCLUDEPATH += /usr/include/openblas/
  LIBS += -lopenblas
}

WARNINGS += -Wall
LIBS += -lGLU -lblas

release:DESTDIR = release
release:OBJECTS_DIR = release/.obj
release:MOC_DIR = release/.moc
release:RCC_DIR = release/.rcc
release:UI_DIR = release/.ui

debug:DESTDIR = debug
debug:OBJECTS_DIR = debug/.obj
debug:MOC_DIR = debug/.moc
debug:RCC_DIR = debug/.rcc
debug:UI_DIR = debug/.ui
