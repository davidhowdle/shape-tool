namespace topology {

  struct he_edge;
  struct he_vert {
    size_t id;
    float x, y, z;
    he_edge * edge;
  };

  struct he_face;
  struct he_edge {
    
    ~he_edge() {
      
      auto nxt = this->next;
      auto pred = this->pred;
      this->next = nullptr;
      this->pred = nullptr;
      this->target = nullptr;

      if(nxt) delete this->next;
      if(pred) delete this->pred;
    }
    
    void insertNext(he_edge * nxt) {
      if(this->next) this->next->insertNext(nxt);
      else {
        nxt->pred = this;
        this->next = nxt;
      }
    }
    
    size_t id;
    he_vert * target;
    he_edge * pred;
    he_edge * next;
    he_edge * pair;
    he_face * face;
  };
  
  struct he_face {
    size_t id;
    he_edge * edge;
    
    ~he_face() {
      delete this->edge;
      this->edge = nullptr;
    }
    
    void insert(he_edge * edge) {
      edge->face = this;
      if(this->edge)
        this->edge->insertNext(edge);
      else
        this->edge = edge;
    }

    void close() {

      if(!this->edge) return;
      
      he_edge * ei = this->edge;
      while(ei->next) {
        ei = ei->next;
      }
      this->edge->pred = ei;
      ei->next = this->edge;
      ei = this->edge;
      do {
        if (!ei->target->edge) {
          ei->target->edge = ei->next;
        } else if (ei->target->edge->target == ei->pred->target) {
          ei->pair = ei->target->edge;
          ei->target->edge->pair = ei;
        }
        ei = ei->next;
      } while(ei != this->edge);
    }

    static he_face * makeTriangle(he_vert * a, he_vert * b, he_vert *c) {
      
      he_face * t = new he_face();
      he_edge * e0 = new he_edge();
      e0->target = b;
      t->insert(e0);
      
      he_edge * e1 = new he_edge();
      e1->target = c;
      t->insert(e1);
      
      he_edge * e2 = new he_edge();
      e2->target = a;
      t->insert(e2);
      
      t->close();
      return t;
    }
    
    void remove() {
      // unlink all
      auto e = this->edge;
      do {
        e->target->edge = nullptr;
        e->face = nullptr;
        if(e->pair) e->pair->pair = nullptr;
        e = e->next;
      } while (e != this->edge);
      
    }
      
  };

}
